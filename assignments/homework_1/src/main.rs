/*! Textual, multi-player version of Conway's game of life.

Based on an idea by Yohann Dudouit.

I render the grids in the terminal, which means it does not work very well on
windows. The reason is that windows' terminal is very primitive and using
colors is difficult, and moving the cursor next to impossible.

As explained below however you don't need to actually run the game to complete
the assignment, just run the tests which do not use any rendering.


# Warning

**Submissions that does not compile with the project provided will not be
graded.**


# Intro

The homework only deals with the frontend, *i.e.* the part that handles user
input to create the initial configuration.

More precisely, the tasks only impact the [`parser`][parse mod] module that's
in the [`frontend`][frontend mod] module. The actual file is
`src/frontend/parser.rs`.



All the tasks deal with functions

- [`mk_parser`][parse fun]
    which creates the parser using the [piston_meta][piston] parser generator
    library

- [`ast_to_sub_grid`][grid gen fun]
    which transforms the AST your parser returns into a [`SubGrid`][sub grid]

A `SubGrid` is just the player name and a grid. A grid is a
`Vec< Vec<Status> >` where [`Status`][status] is an enum that's either `Dead`
or `Live(id)`, where `id` is the identifier of the player (`usize`). This
identifier is given to you as a parameter of `ast_to_sub_grid`.


# Submissions

While you can work as a group as usual, this time submissions are individual.

**Each student must submit the file `src/frontend/parser.rs`** with his/her
name in the module documentation (top of the file, where *Adrien Champion* is).
Right below that you will write the names of your teammates (where *nobody*
is).

The `# Report` section right below this must contain a few lines describing
what you did in the group. Or anything else you want to say.


# Grammar

The grammar you must write a parser for follows. It is given in
[BNF][bnf]-like syntax with terminals in monospace font (`terminal`) and
non-terminals in italic (*NonTerminal*). The starting non-terminal symbol is
*Conf*.

Last, I write

- \"**nothing**\" for the empty sentence.
- \"**not(** `char` **)**\" for any sequence of characters without character
    `char`
- \"**w**\" for any whitespace character (space, tab, newline, *etc.*)
- \"**nl**\" for *newline*: `\r` on windows and `\n` on everything else

|          |     |    |
|---------:|:---:|:---|
|   *Conf* | ::= | *Player* *Grid* |
| *Player* | ::= | `player` `:` `"` *String* `"` |
|   *Grid* | ::= | `grid` `(` *Int* `,` *Int* `)` `{` *Rows* `}`
|   *Rows* | ::= | *Cols* `,` *Rows*
|          |     | **nothing**
|   *Cols* | ::= | *Cell* *Cols*
|          |     | **nothing**
|   *Cell* | ::= | *Live*
|          |     | *Dead*
|   *Live* | ::= | `1`
|          |     | `x`
|          |     | `*`
|   *Dead* | ::= | `0`
|          |     | `_`
|          |     | `.`
|*Comment* | ::= | `//` **not(** **nl** **)** **nl**



## Repetitions

It is possible to rewrite the rules for *Rows* and *Cols* as non-empty
repetitions **+** in [EBNF][ebnf]. Doing so brings the grammar closer to the
format the parser generator library takes.

The equivalent rules are

|          |     |    |
|---------:|:---:|:---|
|   *Rows* | ::= | **(** *Cols* `,` **)+** |
|   *Cols* | ::= | *Cell* **+** |


## Whitespaces and Comments

Whitespaces and comments are ignored and can appear anywhere. You don't need
to support this right away, see the tasks for details.



# Testing your code

I wrote a bunch of testcases in `src/frontend/parser.rs`. Running `cargo test`
will execute these tests and let you know if some fail and why. They are
compartmented in modules corresponding to the tasks.

- to run only the tests for task 1 do `cargo test --features task_1`

- to run only the tests for task 1 and 2 do `cargo test --features task_2`

- to run all the tests do `cargo test` (`cargo test --features task_3` also
    works)


At first you'll probably want to run your code on a simple sample file and see
what happens. I provide 4 files in `rsc/`. You can pass them as argument by
doing `cargo run -- rsc/yako.gol` for instance.

To actually run the game, the binary will require you to provide 4 files:

```[bash]
cargo run -- rsc/yako.gol rsc/rika.gol rsc/agata.gol rsc/adrien.gol
```




# Tasks

The biggest challenge here is to understand how to use the parser library.
Writing the parser can prove challenging, but do not underestimate the
difficulty of going through the AST generated to produce the actual (sub-)grid.

**If you're struggling to generate the grid from the AST, it's probably because
you should change something at parsing-level to make the structure output by
the parser easier to work with.**


## Task 0 (2 points)

Document everything you write. 1 point for documenting the two functions,
explaining the choices and difficulties encountered in a few line. 1 point
for explaining what you're doing **at least every 3 lines of code**.

## Task 1 (10 points)

Write the necessary functions in the [`parser`][parse mod] module to pass all
the tests `cargo test --features task_1` runs.

In practice, this means

- you don't need to support comments (third rule for `_` in the grammar)

- you won't have to worry about whitespaces too much as all testcases have the
    same form, so an ad hoc parser will get through.

Between *i)* getting familiar with `piston_meta` *ii)* writing the parser and
*iii)* writing the code to go from AST to sub-grid, completing task 1 should
take you a whole afternoon, maybe a bit more.


## Task 2 (2 points)

Have your parser / grid construction pass all the tests `cargo test --features
task_2` runs.

This means you will need to support whitespaces between pretty much every
token.

Once you have completed task 1 this task is rather simple should be fast (less
than 30 minutes).



## Task 3 (3 points)

Have your parser / grid construction pass all the tests `cargo test` runs.

This means you will need to support comments between pretty much every token.

This task has a few pitfalls so it's difficult to give an estimate on the time
it'll take. I would guess between 10 minutes and an hour, maybe a bit more.


## Extra credit (4 points)

This one has nothing to do with parsing, it's about the adaptation of the game
of life to multiplayer.

[`RealGrid`][real grid] (in `src/grid.rs`) is the structure that stores the
current grid. Function `next` computes the next one.



### Extra task 1 (1 points)

In the documentation of module `parser` (top of the file
`src/frontend/parser.rs`), explain the rules followed by the `next` function
to update the grid. When does a dead cell becomes live? When does a live cell
become dead / stays live?

You are encouraged to use markdown to make your explanation readable. Don't
refer to bits of code, just put the lines you want to talk about

```
printl!("in a block like this.")
```

We will look directly at the documentation generated, not the comments in the
source file. If it's unreadable you'll get nothing and will have wasted your
time. Rust lets you make your documentation very sexy, use it.

### Extra task 2 (3 points)

Propose changes to the rules and discuss their impact, also as documentation
of the `parser` module. You don't need to implement the changes, but you are
expected to make your explanation understandable.

It would be wise to have a few lines of code in your explanation to make what
you mean clear.



[parse mod]: frontend/parser/index.html
[frontend mod]: frontend/index.html
[bnf]: https://en.wikipedia.org/wiki/Backus%E2%80%93Naur_Form
[ebnf]: https://en.wikipedia.org/wiki/Backus%E2%80%93Naur_Form#Variants
[grid gen fun]: frontend/parser/fn.ast_to_sub_grid.html
[parse fun]: frontend/parser/fn.mk_parser.html
[piston]: https://crates.io/crates/piston_meta
[status]: grid/enum.Status.html
[sub grid]: frontend/struct.SubGrid.html
[real grid]: grid/struct.RealGrid.html
*/

#![allow(non_upper_case_globals)]

extern crate ansi_term as ansi ;
extern crate ansi_control ;
extern crate piston_meta as cfg ;
extern crate range ;

use std::thread::sleep ;
use std::time::Duration ;
use std::process::exit ;

pub mod cursor ;
pub mod common ;
pub mod grid ;
pub mod frontend ;
pub mod patterns ;

use common::* ;
use grid::* ;

const fps_default: u8 = 50 ;
const max_fps:     u8 = 60 ;

/// Result of parsing CLAs.
///
/// Contains the files to parse and the configuration from the flags.
struct Settings {
  /// The files to parse.
  pub files: Vec<String>,
  /// Number of frames per seconds.
  pub fps: u8,
}
impl Settings {
  /// Parses the CLAs.
  pub fn mk() -> Self {
    use std::cmp::{ min, max } ;
    let mut args = std::env::args() ;
    // First argument is the name of the program.
    let _ = args.next() ;
    // Stores the files to parse.
    let mut files = vec![] ;
    // Stores the FPS value.
    let mut fps = fps_default ;

    // Retrieve flags
    'flag_loop: loop {
      // Retrieve next CLA.
      if let Some(arg) = args.next() {
        match & arg as & str {
          "-h" => {
            Self::help() ;
            exit(0)
          },
          "-fps" => match args.next() {
            Some(val) => match u8::from_str_radix(& val, 10) {
              Ok(val) => fps = max(1, min(val, max_fps)),
              Err(err) => common::error(
                format!(
                  "expected integer (u8) after \"-fps\"\ngot \"{}\"\n{}", val, err
                )
              ),
            },
            None => {
              Self::help() ;
              common::error(
                "expected integer after \"-fps\"\ngot nothing".to_string()
              )
            },
          },
          // Unknown option.
          string if string[0..1].to_string() == "-" => {
            Self::help() ;
            common::error(
              format!("unknown option \"{}\"", string)
            )
          },
          // Found first file, breaking out of loop after pushing it.
          first_file => {
            files.push(first_file.to_string()) ;
            break 'flag_loop
          },
        }
      } else {
        Self::help() ;
        common::error(
          "expected files to parse\ngot nothing".to_string()
        )
      }
    } ;

    // Appending rest of the files to parse.
    files.extend(args) ;

    Settings { files: files, fps: fps }
  }

  /// Frame duration from FPS.
  fn frame_duration(& self) -> Duration {
    Duration::from_millis(
      1000 / (self.fps as u64)
    )
  }

  /// Prints a usage message.
  fn help() {
    let stl = ansi::Style::new().bold() ;
    println!(
      "\
{}: `gol [option]* <file_1> ... <file_n>`
Loads the initial configuration for <file_1> ... <file_n> and runs a
multiplayer version of Conway's game of life on the union of all the grids.
   ______________________________________________
  /!\\                                          /!\\
 /!!!\\ All sub-grids must have the same size. /!!!\\
/!!!!!\\______________________________________/!!!!!\\
{}:
  {}          prints this message
  {} <int>  sets the number of frame per second (default {}, clamped to [1,{}])
      ",
      stl.paint("Usage"),
      stl.paint("Options"),
      stl.paint("-h"),
      stl.paint("-fps"),
      fps_default,
      max_fps,
    )
  }
}

fn print<G: Grid>(grid: & G) {
  let conf = grid.conf() ;
  println!("  {1:->0$}", (conf.wdt() + 1) * 2, "") ;
  for line in grid.grid().iter() {
    print!("  |") ;
    for status in line.iter() {
      conf.praint(status)
    } ;
    println!("|")
  } ;
  println!("  {1:->0$}", (conf.wdt() + 1) * 2, "") ;
  print!("  ") ;
  let score = grid.score() ;
  let mut cnt = 0 ;
  let bold = ansi::Style::new().bold() ;
  for & (ref name, ref style) in conf.players().iter() {
    let name: & str = & name ;
    print!(
      "|{} {} {} {}",
      style.paint(">"), style.paint(name),
      bold.paint(& format!("{:^5}", score[cnt]) as & str),
      style.paint("<")
    ) ;
    cnt += 1
  } ;
  println!("|") ;
  println!("")
}

fn main() {
  use std::io::{ stdin, stdout, Write } ;
  use frontend::* ;

  println!("") ;

  let mut parser = match GridParser::mk() {
    Ok(parser) => parser,
    Err(e) => error(e),
  } ;

  let clas = Settings::mk() ;

  println!("|===| Parsing player files.") ;
  for arg in clas.files.iter() {
    print!("| {:<25} ", arg) ;
    match parser.parse_file(& arg) {
      Ok(_) => println!("success"),
      Err(e) => common::error(e),
    }
  } ;

  println!("|===| Success, current players:") ;
  for sub in parser.sub_grids().iter() {
    println!("| - {}", sub.player())
  } ;

  println!("|===| Creating actual grid.") ;
  let mut grid = match parser.to_grid() {
    Ok(grid) => grid,
    Err(e) => common::error(e),
  } ;
  println!("| Success.") ;
  println!("|===|") ;

  println!("") ;
  println!("Press enter to launch.") ;
  let _ = {
    let mut buf = String::new() ;
    std::io::stdin().read_line(& mut buf)
  } ;

  let duration = clas.frame_duration() ;

  println!("Initial:") ;
  print(& grid) ;

  let stdin = stdin() ;
  let mut stdout = stdout() ;
  let mut buffer = String::with_capacity(100) ;

  // Set to true when done but user asks to continue.
  let mut keep_going = false ;

  let mut cnt = 0 ;
  'rendering: loop {
    match grid.update_seq() {
      Ok(done) => {
        cursor::goto_top(& grid) ;
        println!("Generation ({: ^7}):", cnt) ;
        print(& grid) ;
        if done {
          if ! keep_going {
            'question: loop {
              cursor::goto_question_line() ;
              print!(
                "Done, grid has reached a fixpoint or someone has won. \
                Keep going? (yes/No) > "
              ) ;
              match stdout.flush() {
                Ok(_) => (),
                Err(e) => common::error(
                  format!("could not flush stdout:\n{}", e)
                ),
              } ;
              match stdin.read_line(& mut buffer) {
                Ok(_) => {
                  match & buffer.trim() as & str {
                    "Yes" | "yes" | "ye" | "yeah" => {
                      keep_going = true ;
                      break 'question
                    },
                    "No" | "no" | "nah" | "na" | "" => break 'rendering,
                    blah => print!("Sorry, did not get that ({}).", blah),
                  }
                },
                Err(e) => panic!("could not read line: {}", e)
              } ;
              buffer.clear()
            }
          }
        }
      },
      Err(e) => common::error(
        format!("could not update grid:\n{}", e)
      ),
    } ;
    cnt += 1 ;
    sleep(duration)
  } ;

  println!("") ;
  ()
}