//! Dumpable game of life patterns.

use std::io ;

use grid::Status ;

pub type Pattern = Vec< Vec<Status> > ;

pub fn dump(_pattern: Pattern, _file: & str) -> io::Result<()> {
  unimplemented!()
}

fn parse(s: & str) -> Result<
  Vec< Vec<Status> >, String
> {
  use frontend::GridParser ;
  // Building a parser.
  let mut parser = match GridParser::mk() {
    Ok(parser) => parser,
    Err(e) => return Err(e),
  } ;
  // Parsing.
  match parser.parse_str(s) {
    Ok(()) => Ok( parser.into_sub_grids().pop().unwrap().into_grid() ),
    Err(e) => Err(e)
  }
}

macro_rules! parse_pattern {
  ($module:expr, $name:expr, $s:expr) => (
    match parse(
      & format!(
        $s, $name
      ) 
    ) {
      Ok(grid) => grid,
      Err(e) => $crate::common::error(
        format!(
          "could not create pattern \"{}::{}\"\n{}", $module, $name, e
        )
      ),
    }
  )
}


/// Patterns that just stand still.
pub mod still_life {
  use super::{ parse, Pattern } ;

  /// Block.
  ///
  /// ```
  /// **
  /// **
  /// ```
  pub fn block() -> Pattern {
    parse_pattern!(
      "still_life", "block", "
        player: \"{}\"
        grid (2,2) {{
          **,
          **,
        }}
      "
    )
  }

  /// Beehive.
  ///
  /// ```
  /// _**_
  /// *__*
  /// _**_
  /// ```
  pub fn beehive() -> Pattern {
    parse_pattern!(
      "still_life", "beehive", "
        player: \"{}\"
        grid (3,4) {{
          _**_,
          *__*,
          _**_,
        }}
      "
    )
  }

  /// Loaf.
  ///
  /// ```
  /// _**_
  /// *__*
  /// _*_*
  /// __*_
  /// ```
  pub fn loaf() -> Pattern {
    parse_pattern!(
      "still_life", "loaf", "
        player: \"{}\"
        grid (4,4) {{
          _**_,
          *__*,
          _*_*,
          __*_,
        }}
      "
    )
  }

  /// Loaf.
  ///
  /// ```
  /// **_
  /// *_*
  /// _*_
  /// ```
  pub fn boat() -> Pattern {
    parse_pattern!(
      "still_life", "boat", "
        player: \"{}\"
        grid (3,3) {{
          **_,
          *_*,
          _*_,
        }}
      "
    )
  }

  /// All still life patterns.
  pub fn all() -> Vec<Pattern> {
    vec![
      block(), beehive(), loaf(), boat()
    ]
  }

}