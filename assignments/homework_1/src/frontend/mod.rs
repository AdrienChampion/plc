//! Frontend handling player's initial configurations.

use std::io ;
use std::io::Read ;
use std::iter::Iterator ;

use range::Range ;
use cfg::{ Syntax, MetaData } ;

use common::Conf ;
use grid::{ Status, RealGrid } ;

pub mod parser ;

/// A sub-grid.
#[derive(PartialEq)]
pub struct SubGrid {
  /// Player name.
  player: String,
  /// Grid.
  grid: Vec< Vec< Status > >,
}
impl SubGrid {
  /// Creates a new sub-grid.
  pub fn mk(player: String, grid: Vec< Vec< Status > >) -> Self {
    SubGrid {
      player: player,
      grid: grid,
    }
  }
  /// The player the grid is for.
  pub fn player(& self) -> & str {
    & self.player
  }
  /// The sub-grid.
  pub fn grid(& self) -> & Vec< Vec< Status > > {
    & self.grid
  }
  /// The sub-grid.
  pub fn into_grid(self) -> Vec< Vec< Status > > {
    self.grid
  }

  /// Dumps a sub-grid in a file.
  pub fn dump(& self, file: & str) -> io::Result<()> {
    use std::fs::OpenOptions ;
    use std::io::Write ;
    match OpenOptions::new().write(true).create(true).open(file) {
      Ok(mut file) => {
        try!(
          write!(
            file, "player: \"{}\"\n\ngrid ({}, {}) {{\n",
            self.player, self.grid.len(), self.grid[0].len()
          )
        ) ;
        for row in self.grid.iter() {
          try!( write!(file, "  ") ) ;
          for cell in row.iter() {
            match * cell {
              Status::Dead => try!( write!(file, "_") ),
              Status::Live(_) => try!( write!(file, "*") ),
            }
          }
          try!( write!(file, "\n") )
        } ;
        write!(file, "}}\n")
      },
      Err(e) => Err(e),
    }
  }
}

/// A grid parser.
///
/// Stores the sub-grid parser and the grids parsed this far.
pub struct GridParser {
  /// Parser for sub-grid files.
  syntax: Syntax,
  /// Sub-grids parsed this far.
  sub_grids: Vec< SubGrid >,
  /// Size of the sub-grids parsed this far.
  size: Option<(usize, usize)>,
  /// Temporary string buffer to read stuff, files typically.
  str_buffer: String,
  /// Temporary token buffer.
  tkn_buffer: Vec< Range<MetaData> >,
}
impl GridParser {
  /// Creates a new grid parser.
  pub fn mk() -> Result<Self, String> {
    Ok(
      GridParser {
        syntax: try!( parser::mk_parser() ),
        sub_grids: vec![],
        size: None,
        str_buffer: String::with_capacity(10_000),
        tkn_buffer: Vec::with_capacity(1_000),
      }
    )
  }
  /// The sub-grids.
  pub fn sub_grids(& self) -> & [ SubGrid ] {
    & self.sub_grids
  }
  /// The sub-grids.
  pub fn into_sub_grids(self) -> Vec<SubGrid> {
    self.sub_grids
  }


  /// Creates the grid corresponding to the union of the sub-grids.
  ///
  /// Only works for 4 players.
  pub fn to_grid(self) -> Result<RealGrid, String> {
    use std::iter::Extend ;

    // Retrieve size of the actual grid.
    let (rows, cols) = match self.size {
      Some( (rows, cols) ) => (rows * 2, cols * 2),
      None => return Err(
        format!("sub-grid parser is empty, cannot construct grid.")
      ),
    } ;

    // Only works for 4 players for now.
    if self.sub_grids.len() != 4 {
      return Err(
        format!("expected 4 players\ngot {}", self.sub_grids.len())
      )
    } ;

    let mut subs = self.sub_grids.into_iter() ;

    // Player names.
    let mut players = vec![] ;

    // Retrieve first grid.
    let mut top_grid = match subs.next() {
      Some(sub) => {
        players.push(sub.player) ;
        sub.grid
      },
      None => unreachable!(),
    } ;

    // Extend rows with rows of second grid.
    match subs.next() {
      Some(sub) => {
        players.push(sub.player) ;
        let mut cnt = 0 ;
        for row in sub.grid.into_iter() {
          top_grid[cnt].extend(row) ;
          cnt += 1
        }
      },
      None => unreachable!(),
    } ;

    // Retrieve third grid.
    let mut bottom_grid = match subs.next() {
      Some(sub) => {
        players.push(sub.player) ;
        sub.grid
      },
      None => unreachable!(),
    } ;

    // Extend rows of bottom grid with rows of fourth grid.
    match subs.next() {
      Some(sub) => {
        players.push(sub.player) ;
        let mut cnt = 0 ;
        for row in sub.grid.into_iter() {
          bottom_grid[cnt].extend(row) ;
          cnt += 1
        }
      },
      None => unreachable!(),
    } ;

    // Join top and bottom.
    top_grid.extend( bottom_grid ) ;

    Ok(
      RealGrid::mk(
        top_grid, Conf::mk(rows, cols, players.into_iter().collect())
      )
    )
  }


  /// Parses a sub-grid from a file.
  pub fn parse_file(& mut self, file: & str) -> Result<(), String> {
    use std::fs::OpenOptions ;
    match OpenOptions::new().read(true).open(file) {
      Err(err) => Err(
        format!("could not open file \"{}\":\n{}", file, err)
      ),
      Ok(mut file_reader) => {
        // Read all of the file into the buffer.
        match file_reader.read_to_string(& mut self.str_buffer) {
          Ok(_) => self.parse_buffer(),
          Err(e) => Err(
            format!(
              "could not read from file \"{}\":\n{}", file, e
            )
          ),
        }
      }
    }
  }

  /// Parses a sub-grid from the internal (string) buffer.
  ///
  /// Private to prevent n00bs from parsing empty buffers.
  fn parse_buffer(& mut self) -> Result<(), String> {
    use cfg::parse_errstr ;
    match parse_errstr(
      & self.syntax,
      & self.str_buffer,
      & mut self.tkn_buffer,
    ) {
      Ok(_) => {
        let res = self.build_sub_grid() ; // No problem, building grid.
        self.str_buffer.clear() ;         // Clean string buffer.
        res                               // Return result.
      },
      Err(e) => Err(e),
    }
  }

  /// Parses a sub-grid from a string.
  pub fn parse_str(& mut self, s: & str) -> Result<(), String> {
    use cfg::parse_errstr ;
    match parse_errstr(
      & self.syntax,
      s,
      & mut self.tkn_buffer,
    ) {
      Ok(_) => self.build_sub_grid(), // No problem, building grid.
      Err(e) => Err(e),
    }
  }

  /// Builds a sub-grid from the internal (token) buffer.
  fn build_sub_grid(& mut self) -> Result<(), String> {
    let sub_grid = try!(
      parser::ast_to_sub_grid(self.sub_grids.len(), & self.tkn_buffer)
    ) ;

    let rows = sub_grid.grid().len() ;
    let cols = sub_grid.grid()[0].len() ;

    // Check for dimension mismatch with other previous sub-grids.
    match self.size {
      Some( (r0ws, c0ls) ) => if rows != r0ws || cols != c0ls {
        return Err(
          format!(
            "grid for \"{}\" has size ({}, {})\n\
            but previous grids had size ({}, {})",
            sub_grid.player(), rows, cols, r0ws, c0ls
          )
        )
      },
      // First sub-grid we parse, update size info.
      None => self.size = Some( (rows, cols) ),
    }

    // Pushing grid to internal list of grids.
    self.sub_grids.push(sub_grid) ;

    // Clear token buffer.
    self.tkn_buffer.clear() ;

    Ok(())

  }
}