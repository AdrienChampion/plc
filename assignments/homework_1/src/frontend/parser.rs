/*! Parser and AST to `SubGrid` functions.

Used to parse user initial sub-grids.

# Submission

> Adrien Champion

# Extra task 1

**This explanation below is fairly detailed, you were not expected to provide
that much information.**

The `next` function looks at each cell `c` and works as follows.

## If `c` is dead

The algorithm uses a [HashMap][map] `map` to count the number of live cells
belonging to each player around `c`. The new status of `c` is decided by [the
following piece of code][dead update]

```[no_use]
let mut res = (0, Dead) ;
for (id, cnt) in map {
  if cnt >= 3 {
    match cnt.cmp(& res.0) {
      Greater => res = (cnt, Live(id)),
      Equal => match res.1 {
        Live(_) => res.1 = Dead,
        Dead => (),
      },
      Less => (),
    }
  }
} ;
```

We see that the cell becomes alive iff **exactly** one player has three of his
/ her cells or more alive next to `c`.

In particular, if two players have three or more of their cells next to `c`,
the cell stays dead. The idea is that the two players are *fighting* over `c`
and it does not belong to anyone yet.

Because of the way the algorithm is written, if there was three players with
three or more cells next to `c` then it would actually become alive. This is
impossible however, since `c` only has 8 neightbors.

It is worth noting that the code could be made more efficient. [A new HashMap
is created for each cell][inefficient]:

```[no_use]
match self.old[r][c] {
  Dead => {
    let mut map = HashMap::with_capacity(self.conf().players().len()) ;
```

The hash map could be created once and for all when the grid is created, and
stored inside the grid itself. `next` could thus use this map instead of
creating (allocating) a new one each time, potentially triggering memory
reorganization of the heap.

## If `c` is alive and belongs to player `p`

The cell stays alive iff there is 2 or 3 cells belonging to player `p` around
it. Otherwise it dies.

In particular, a cell cannot change directly from a player to another. It has
to die first.



# Extra task 2

Any of the aspects of the algorithm described above could be changed to alter
the dynamics of the game.

For instance, by allowing cells to change from one player to another directly,
making conquering cells more likely and faster.


[map]: https://doc.rust-lang.org/stable/std/collections/struct.HashMap.html (HashMap doc)
[dead update]: ../../../src/gol/grid.rs.html#211-223 (Dead to Alive resolution)
[inefficient]: ../../../src/gol/grid.rs.html#176 (Inefficient code)
*/

use cfg::{ Syntax, MetaData } ;
use range::Range ;

use grid::Status ;
use frontend::SubGrid ;

/// Creates the parser for user initial sub-grids.
pub fn mk_parser() -> Result<Syntax, String> {
  use cfg::* ;
  let rules = r#"
    /* Single line comments using `//`. */
    0 sl_comment = {
      [.w? "//" ..."\n"?]
      [.w? "//" ..."\r"?]
    }

    /* Some comments and / or whitespaces. */
    11 cws = { .s!(.w? sl_comment) .w? }
    1 cmt_or_ws = cws

    /* Cells. */
    21 dead = { "0":!"alive" "_":!"alive" ".":!"alive" }
    22 alive = { "1":"alive" "*":"alive" "x":"alive" }
    2 cell = { dead alive }

    /* Sub-grid. Rows are comma-separated, cells are whitespace-separated. */
    31 row = .s!(cmt_or_ws cell)
    32 rows = [
      cmt_or_ws .s!([cmt_or_ws "," cmt_or_ws] row:"row") cmt_or_ws
    ]
    3 sub_grid = [
      "grid" cmt_or_ws "("
        cmt_or_ws .$_:"n_rows" cmt_or_ws "," cmt_or_ws .$_:"n_cols" cmt_or_ws
      ")" cmt_or_ws "{" rows "}"
    ]

    /* Player info. */
    4 player_info = ["player" cmt_or_ws ":" cmt_or_ws .t!:"player"]

    /* Entry point of the parser. */
    5 entry_point = [
      cmt_or_ws player_info
      cmt_or_ws sub_grid
      cmt_or_ws
    ]
  "# ;
  
  match syntax_errstr(rules) {
    Err(err) => return Err(
      format!("could not create parser:\n{}", err)
    ),
    Ok(parser) => Ok(parser),
  }
}


/** Unwraps an option of a result.

Returns `something` if the input is `Some(Ok(something))`, exits the current
function with an error otherwise.

The second parameter is the token expected, used to document the error if there
is no more tokens. */
macro_rules! try_next_token {
  ($to_try:expr, expecting $s:expr) => (
    match $to_try {
      Some( Ok(something) ) => something,
      Some( Err(e) ) => return Err(e),
      None => return Err(
        format!("expected {}, got nothing", $s)
      ),
    } ;
  ) ;
}

/** Applies a function to the next token in a token iterator, if any. */
fn next_token_apply<
  'a,
  TknIter: Iterator<Item = & 'a Range<MetaData>>,
  Output,
  Fun: Fn(& 'a Range<MetaData>) -> Output,
>(
  tokens: & mut TknIter, f: Fun
) -> Option<Output> {
  match tokens.next() {
    Some(tkn) => Some( f(tkn) ),
    None => None,
  }
}

/// Turns an AST produced by parsing using the parser from `mk_parser` into a
/// `SubGrid`.
pub fn ast_to_sub_grid(
  id: usize, ast: & [ Range<MetaData> ]
) -> Result<SubGrid, String> {
  use cfg::MetaData::* ;

  let mut tokens = ast.iter() ;

  // First token should be the player.
  let player = try_next_token!(
    next_token_apply(
      & mut tokens,
      |tkn| match tkn.data {
        String(ref key, ref val) => if ** key == "player" {
          Ok((** val).clone())
        } else {
          Err(
            format!(
              "expected player\ngot key: {}\n    value: {}", key, val
            )
          )
        },
        _ => Err(
          format!("expected player\ngot {:?}", tkn)
        ),
      }
    ), expecting "player"
  ) ;

  // Second token should be the number of rows.
  let rows = try_next_token!(
    next_token_apply(
      & mut tokens,
      |tkn| match tkn.data {
        F64(ref key, ref val) => if ** key == "n_rows" {
          Ok(* val as usize)
        } else {
          Err(
            format!(
              "expected rows count\ngot key: {}\n    value: {}", key, val
            )
          )
        },
        _ => Err(
          format!("expected rows count\ngot {:?}", tkn)
        ),
      }
    ), expecting "n_rows"
  ) ;

  // Second token should be the number of cols.
  let cols = try_next_token!(
    next_token_apply(
      & mut tokens,
      |tkn| match tkn.data {
        F64(ref key, ref val) => if ** key == "n_cols" {
          Ok(* val as usize)
        } else {
          Err(
            format!(
              "expected cols count\ngot key: {}\n    value: {}", key, val
            )
          )
        },
        _ => Err(
          format!("expected cols count\ngot {:?}", tkn)
        ),
      }
    ), expecting "n_cols"
  ) ;

  // Preparing an empty list of rows with the right capacity.
  let mut grid = Vec::with_capacity(rows) ;
  // Preparing an empty list of cells with the right capacity.
  let mut row = Vec::with_capacity(cols) ;

  // Tokens left should only be the grid.
  for token in tokens {
    match token.data {
      // End of `row` node.
      EndNode(ref id) if ** id == "row" => {
        // Push current row to grid.
        grid.push(row) ;
        // Create new row.
        row = Vec::with_capacity(cols)
      },

      // Start of `row` node.
      StartNode(ref id) if ** id == "row" => (),

      // Cell data.
      Bool(ref key, ref val) => if ** key == "alive" {
        // Pushing new cell to current row.
        row.push(
          if * val { Status::Live(id) } else { Status::Dead }
        )
      } else {
        return Err(
          format!("expected cell\ngot{:?}", token)
        )
      },

      _ => panic!("aaahhh: {:?}", token),
    }
  } ;

  // Check for dimension mismatch (rows).
  if grid.len() != rows {
    return Err(
      format!(
        "expected sub-grid with {} rows\ngot {} rows", rows, grid.len()
      )
    )
  } ;
  // Check for dimension mismatch (columns).
  for i in 0..grid.len() {
    if grid[i].len() != cols {
      return Err(
        format!(
          "expected sub-grid with {} columns\nbut row {} has {} columns",
          cols, i, grid[i].len()
        )
      )
    }
  } ;

  // Add sub-grid to internal list of subgrids.
  Ok( SubGrid::mk(player, grid) )
}








/* |=====| TEST STUFF. |=====| */






/// Parses a string and creates the sub-grid.
#[cfg(test)]
pub fn get_grid(id: usize, to_parse: & str) -> Result<SubGrid, String> {
  use cfg::parse_errstr ;
  // Token buffer.
  let mut ast = vec![] ;
  // Creating parser.
  let parser = try!( mk_parser() ) ;
  // Parsing input string.
  try!(
    parse_errstr(& parser, to_parse, & mut ast)
  ) ;
  // Returning subgrid.
  ast_to_sub_grid(id, & ast)
}

/// Prints a grid in the input format.
///
/// For debugging in tests.
#[cfg(test)]
pub fn grid_to_str(grid: & SubGrid) -> String {
  let player = grid.player() ;
  let grid = grid.grid() ;
  let (rows, cols) = (grid.len(), grid[0].len()) ;
  let header = format!(
    "\
      player: \"{}\"\n\n\
      grid ({}, {}) {{\
    ", player, rows, cols
  ) ;
  let just_misses_footer = grid.iter().fold(
    header,
    |s, row| {
      let no_comma = row.iter().fold(
        format!("{}\n  ", s),
        |s, cell| {
          format!(
            "{}{}", s, match * cell {
              Status::Dead => "_",
              Status::Live(_) => "x",
            }
          )
        }
      ) ;
      format!("{},", no_comma)
    }
  ) ;
  format!("{}\n}}", just_misses_footer)
}

/// Checks two grids are equal, prints an error and panics otherwise.
#[cfg(test)]
pub fn check_same(
  input: & str, expected: SubGrid, to_check: Result<SubGrid, String>
) {
  match to_check {
    Ok(grid) => if expected == grid { () } else {
      println!("") ;
      println!("|===| Error on input") ;
      for line in input.lines() {
        println!("| {}", line)
      } ;
      println!("|===| expected grid") ;
      for line in grid_to_str(& expected).lines() {
        println!("| {}", line)
      } ;
      println!("|===| but got grid:") ;
      for line in grid_to_str(& grid).lines() {
        println!("| {}", line)
      } ;
      println!("|===|") ;
      println!("") ;
      panic!("test failed")
    },
    Err(e) => {
      println!("") ;
      println!("|===| Error on input") ;
      for line in input.lines() {
        println!("| {}", line)
      } ;
      println!("|===| expected grid") ;
      for line in grid_to_str(& expected).lines() {
        println!("| {}", line)
      } ;
      println!("|===| but got an error:") ;
      for line in e.lines() {
        println!("| {}", line)
      } ;
      println!("|===|") ;
      println!("") ;
      panic!("test failed")
    },
  }
}

/// Checks that parsing has failed on some input.
#[cfg(test)]
pub fn check_fails(
  input: & str, error: & str, to_check: Result<SubGrid, String>
) {
  match to_check {
    Err(_) => (),
    Ok(grid) => {
      println!("") ;
      println!("|===| Error on input") ;
      for line in input.lines() {
        println!("| {}", line)
      } ;
      println!("|===| expected parse error") ;
      for line in error.lines() {
        println!("| {}", line)
      } ;
      println!("|===| but parsing was successful:") ;
      for line in grid_to_str(& grid).lines() {
        println!("| {}", line)
      } ;
      println!("|===|") ;
      println!("") ;
      panic!("test failed")
    },
  }
}

/// DSL to create a grid.
#[cfg(test)]
macro_rules! mk_grid {
  (
    size = ( $rows:expr, $cols:expr ),
    id = ( $name:expr, $id:expr),
    grid = {
      $( $row:expr, { $( $col:expr ),+ } ),*
    }
  ) => (
    {
      let mut vec = vec![
        vec![ $crate::grid::Status::Dead ; $cols ] ; $rows
      ] ;
      for (row,col) in vec![
        $( $( ($row - 1, $col - 1) ),+ ),*
      ].into_iter() {
        vec[row][col] = $crate::grid::Status::Live($id)
      } ;
      $crate::frontend::SubGrid::mk($name.to_string(), vec)
    }
  ) ;
}




#[cfg(test)]
/// Simple testcases that should work.
///
/// No comments at all.
mod task_1_correct {
  use super::* ;

  #[test]
  fn test_1() {
    let input = r#"
player: "yako"

grid (20,20) {
  ____________________,
  ____________________,
  ______xxx___________,
  _______x____________,
  _______x____________,
  ______xxx___________,
  ____________________,
  ______xxx___________,
  ______xxx___________,
  ____________________,
  ______xxx___________,
  _______x____________,
  _______x____________,
  ______xxx___________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 42 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("yako", id),
      grid = {
        3,  { 7, 8, 9 },
        4,  {    8    },
        5,  {    8    },
        6,  { 7, 8, 9 },
        8,  { 7, 8, 9 },
        9,  { 7, 8, 9 },
        11, { 7, 8, 9 },
        12, {    8    },
        13, {    8    },
        14, { 7, 8, 9 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

  #[test]
  fn test_2() {
    let input = r#"
player: "agata"

grid (20,20) {
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ________x___________,
  __________x_________,
  _______xx__xxx______,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 0 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("agata", id),
      grid = {
        9, { 9 },
        10, { 11 },
        11, { 8, 9, 12, 13, 14 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

  #[test]
  fn test_3() {
    let input = r#"
player: "rika"

grid (
  20,     20
) {
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ______x_x_x_________,
  ______x___x_________,
  ______x___x_________,

  ______x___x_________,
  ______x_x_x_________,
  ____________________,

  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,

  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 7 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("rika", id),
      grid = {
        7,  { 7, 9, 11 },
        8,  { 7,    11 },
        9,  { 7,    11 },
        10, { 7,    11 },
        11, { 7, 9, 11 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

  #[test]
  fn test_4() {
    let input = r#"
player: "adrien"

grid (20,20) {
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____xxx_xxx_________,
  ____x_____x_________,
  ______x_x___________,
  _____xx_xx__________,
  ____x_____x_________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 11 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("adrien", id),
      grid = {
        8,  { 5, 6, 7,   9, 10, 11 },
        9,  { 5,                11 },
        10, {       7,   9         },
        11, {    6, 7,   9, 10     },
        12, { 5,                11 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

}





#[cfg(test)]
/// Simple testcases that should not work because of dimension mismatch.
///
/// No comments at all.
mod task_1_incorrect {
  use super::* ;

  #[test]
  fn test_1() {
    let input = r#"
player: "yako"

grid (17,20) {
  ____________________,
  ____________________,
  ______xxx___________,
  _______x____________,
  _______x____________,
  ______xxx___________,
  ____________________,
  ______xxx___________,
  ______xxx___________,
  ____________________,
  ______xxx___________,
  _______x____________,
  _______x____________,
  ______xxx___________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 420 ;

    check_fails(
      input, "\
        size declared is (17,20)\n\
        but actual size is (20,20)\
      ", get_grid(id, input)
    )

  }

  #[test]
  fn test_2() {
    let input = r#"
player: "yako"

grid (20,20) {
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ________x___________,
  _________x_________,
  _______xx__xxx______,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 44_442 ;

    check_fails(
      input, "\
        size declared is (20,20)\n\
        but line 10 only has 19 columns\
      ", get_grid(id, input)
    )

  }

  #[test]
  fn test_3() {
    let input = r#"
player "yako"

grid (20,20) {
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ________x___________,
  _________x__________,
  _______xx__xxx______,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 77 ;

    check_fails(
      input, "\
        missing `:` after `player` keyword\
      ", get_grid(id, input)
    )

  }

  #[test]
  fn test_4() {
    let input = r#"
player: "yako

grid (20,20) {
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ________x___________,
  _________x__________,
  _______xx__xxx______,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 5 ;

    check_fails(
      input, "\
        missing `\"` around player name keyword\
      ", get_grid(id, input)
    )

  }

}





#[cfg(
  all(
    test,
    not( feature="task_1" ),
  )
)]
/// Simple testcases that should work.
///
/// No comments at all but some extra spaces.
mod task_2 {
  use super::* ;

  #[test]
  fn test_1() {
    let input = r#"
player: "yako"

grid (20,20) {
  ____________________,
  ____________________,
  ______xxx___________,
  _______x____________,
  _______x____________,
  ______xxx___________,
  ____________________,
  ______xxx___________,
  ______xxx___________,
  ____________
  ________,
  ______xxx___________,
  _______x____________,
  _______x________



  ____,
  ______xxx___________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,

  ____________________,
}
    "# ;

    let id = 42 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("yako", id),
      grid = {
        3,  { 7, 8, 9 },
        4,  {    8    },
        5,  {    8    },
        6,  { 7, 8, 9 },
        8,  { 7, 8, 9 },
        9,  { 7, 8, 9 },
        11, { 7, 8, 9 },
        12, {    8    },
        13, {    8    },
        14, { 7, 8, 9 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

  #[test]
  fn test_2() {
    let input = r#"
player: "agata"

    grid (
  20,     20


  )

 {
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ________x___________,
  __________x_________,
  _______xx__xxx______,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 1_000 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("agata", id),
      grid = {
        9, { 9 },
        10, { 11 },
        11, { 8, 9, 12, 13, 14 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

  #[test]
  fn test_3() {
    let input = r#"
player




:    "rika"         
  
grid (
  20,     


  20
) {
  ____________________,
  __________ __________,
  ____________________,
  _ ___________________,
  _________________ ___,
  ____________________,
  ______x_x_x_________,
  ______x___x_________,
  ______x___x_________,

  ______x___x_________,
  ______x_x_x_________,
  ____________________,

  ____________________,
  ____________________,
  ____________________,
  _______ _____________,
  ____________________,

  ____________________,
  __________ __________,
  ____________________,
}
    "# ;

    let id = 7 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("rika", id),
      grid = {
        7,  { 7, 9, 11 },
        8,  { 7,    11 },
        9,  { 7,    11 },
        10, { 7,    11 },
        11, { 7, 9, 11 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

  #[test]
  fn test_4() {
    let input = r#"
player
:
"adrien"

grid
(
20
,
20
)
{
  ____________________
  ,
  ____________________
  ,
  ____________________
  ,
  _________
  ___________
  ,
  ____________________
  ,
  ____________________
  ,
  ____________________
  ,
  ____xxx_xxx_________
  ,   
  ____x_____x _________
  ,
  ______x_x___________
  , 
  _____  xx_xx__________
  ,
  ____x_____x_________
  ,
  ____________________
  ,
  ____________________
  ,
  ____________________
  ,
  ____________________
  ,
  ____________________
  ,
  ____________________
  ,
  ___________   _________
  ,
  ____________________
  ,



}



    "# ;

    let id = 30 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("adrien", id),
      grid = {
        8,  { 5, 6, 7,   9, 10, 11 },
        9,  { 5,                11 },
        10, {       7,   9         },
        11, {    6, 7,   9, 10     },
        12, { 5,                11 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

}





#[cfg(
  all(
    test,
    not( feature="task_1" ),
    not( feature="task_2" ),
  )
)]
/// Simple testcases that should work.
///
/// No comments at all.
mod task_3 {
  use super::* ;

  #[test]
  fn test_1() {
    let input = r#"
// Some comment.
player: "yako"

grid (20,20) {
  ____________________,
  ____________________,
  ______xxx___________,
  _______x____________,
  _______x____________,
  ______xxx___________,
  ____________________,
  ______xxx___________,
  ______xxx___________,
  ____________________,
  ______xxx___________,
  _______x____________,
  _______x____________,
  ______xxx___________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 17 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("yako", id),
      grid = {
        3,  { 7, 8, 9 },
        4,  {    8    },
        5,  {    8    },
        6,  { 7, 8, 9 },
        8,  { 7, 8, 9 },
        9,  { 7, 8, 9 },
        11, { 7, 8, 9 },
        12, {    8    },
        13, {    8    },
        14, { 7, 8, 9 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

  #[test]
  fn test_2() {
    let input = r#"
player: "agata"
// Comment.
// Another comment.
grid (20,20) {
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ________x___________,
  __________x_________,
  _______xx__xxx______,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 2 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("agata", id),
      grid = {
        9, { 9 },
        10, { 11 },
        11, { 8, 9, 12, 13, 14 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

  #[test]
  fn test_3() {
    let input = r#"
player: "rika"

grid (
  20,     20
) {
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ______x_x_x_________,
  ______x___x_________,
  ______x___x_________,
  // Comment.
  ______x___x_________,
  ______x_x_x_________,
  ____________________,
  // Comment.
  // Comment.
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,

  ____________________,
  ____________________,
  ____________________,
}
    "# ;

    let id = 0 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("rika", id),
      grid = {
        7,  { 7, 9, 11 },
        8,  { 7,    11 },
        9,  { 7,    11 },
        10, { 7,    11 },
        11, { 7, 9, 11 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

  #[test]
  fn test_4() {
    let input = r#"
player:
  // My name.
  "adrien"

grid (
  // Number of rows.
  20,
  // Number of cols.
  20
) {
  ____________________,
  ____________________,
  __________ // Half an empty line.
  __________ // Half an empty line.
  , // That's just a comma.
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____xxx_xxx_________,
  ____x_____x_________,
  ______x_x___________,
  _____xx_xx__________,
  ____x_____x_________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  ____________________,
  // End of grid def.
}
// At this point the file ends.
    "# ;

    let id = 7 ;

    let expected = mk_grid!(
      size = (20,20),
      id = ("adrien", id),
      grid = {
        8,  { 5, 6, 7,   9, 10, 11 },
        9,  { 5,                11 },
        10, {       7,   9         },
        11, {    6, 7,   9, 10     },
        12, { 5,                11 }
      }
    ) ;

    check_same(
      input, expected, get_grid(id, input)
    )

  }

}








