/*! Library providing wrapping values with units.

The actual wrapper is the `U` struct. Trait [`ToUnits`][to units] provides
helper function to turn primitive types into values with a unit.

# Solution

See the [`solution` module][solution].

Note that I turned the *binary project* (runnable) into a library (not
runnable) by having a `lib.rs` top level file instead of a `main.rs` one. I did
this for several technical reasons that do not matter that much here, if you
want to know you can ask me on Piazza.

To run the test from the `eval` module **and the tests in the documentation**,
run `cargo test`.

# Tasks

See the [`eval` module][eval]. Updated with the answer to the extra credit
question.

[to units]: solution/helpers/trait.ToUnits.html (ToUnits trait)
[eval]: eval/index.html (Eval module)
[solution]: solution/index.html (Solution module)
*/

#![allow(dead_code)]
#![forbid(missing_docs)]

extern crate rand ;

pub mod provided ;
pub mod solution ;

pub mod eval ;
