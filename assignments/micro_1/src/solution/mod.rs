/*! Solution for the unit module.

As discussed in class, the benefit of writing polymorphic code it can be used
with basically anything. For instance, the following code is all we need to
wrap 3D values instead of (1D) scalars values

Note that this code is **compiled and run** when you run `cargo test`.

Note also the `extern crate units ;`. In this code we are using the `units`
library as an **external library**.

```
extern crate units ;

use std::ops::{ Div, Neg } ;
use std::fmt::Display ;
use std::fmt ;

/// Structure for 3D.
///
/// Typically represents a distance.
#[derive(Clone, Copy)]
pub struct Vec3 {
  pub x: f64,
  pub y: f64,
  pub z: f64,
}
impl Display for Vec3 {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    write!(fmt, "[{}, {}, {}]", self.x, self.y, self.z)
  }
}

/// Macro to create `Vec3` easily.
macro_rules! vec3 {
  ($x:expr, $y:expr, $z:expr) => (
    Vec3 { x: $x as f64, y: $y as f64, z: $z as f64 }
  ) ;
}

// Division by scalars. By time value typically, to construct a speed.
impl Div<f64> for Vec3 {
  type Output = Vec3 ;
  fn div(mut self, scalar: f64) -> Vec3 {
    self.x = self.x / scalar ;
    self.y = self.y / scalar ;
    self.z = self.z / scalar ;
    self
  }
}

// |===| The code above is (one take on) what one would write regardless of the
//     | `units` module. The implementation below is **all that's needed** to
//     | wrap `Vec3`s in `U` wrappers and use them.

// Implementing `Val` for `Vec3`.
impl units::solution::Val for Vec3 {
  // In a real units library this function would not be there. I wrote it in
  // the code I gave you to illustrate abstract functions, but here it doesn't
  // make sense.
  fn from_f64(_: f64) -> Self {
    panic!("this function does not make sense for `Vec3`")
  }
}

fn main() {
  use units::solution::* ;
  use units::solution::helpers::* ;
  let vec3 = vec3![ 7, 5, 11 ] ;
  let len = Length::meters(vec3) ;
  let time = 17.seconds() ;
  // Dividing to get a 3D speed.
  let speed = len / time ;
  println!("speed: {}", speed)
}
```
*/

use std::cmp::PartialEq ;

mod wrap ;
pub use self::wrap::* ;
pub mod helpers ;

/// Trait for units.
pub trait Unit: Clone + Copy + PartialEq {
  /// String representation of a unit.
  fn to_str(& self) -> String ;
}

/// Length units.
#[derive(Clone, Copy, PartialEq)]
pub enum Length {
  /// Meters.
  M,
  /// Feet.
  Ft,
  /// Miles.
  Mi,
  /// Kilometers.
  Km,
}
impl Length {
  /// Wraps a value in meters.
  pub fn meters<V: Val>(v: V) -> U<V, Length> {
    U::mk(v, Length::M)
  }
  /// Wraps a value in feet.
  pub fn feet<V: Val>(v: V) -> U<V, Length> {
    U::mk(v, Length::Ft)
  }
  /// Wraps a value in feet.
  pub fn miles<V: Val>(v: V) -> U<V, Length> {
    U::mk(v, Length::Mi)
  }
  /// Wraps a value in kilometers.
  pub fn kilometers<V: Val>(v: V) -> U<V, Length> {
    U::mk(v, Length::Km)
  }

  /// Factor to convert to meters.
  pub fn to_meters(& self) -> f64 {
    match * self {
      Length::M => 1.0,
      Length::Ft => 0.3048,
      Length::Mi => 1_609.344,
      Length::Km => 1_000.0,
    }
  }
  /// Factor to convert to feet.
  pub fn to_feet(& self) -> f64 {
    match * self {
      Length::M => 3.281,
      Length::Ft => 1.0,
      Length::Mi => 5_280.0,
      Length::Km => 3_281.0,
    }
  }
  /// Factor to convert to miles.
  pub fn to_miles(& self) -> f64 {
    match * self {
      Length::M => 0.00062137,
      Length::Ft => 0.000189,
      Length::Mi => 1.0,
      Length::Km => 0.62137,
    }
  }
  /// Factor to convert to kilometers.
  pub fn to_kilometers(& self) -> f64 {
    match * self {
      Length::M => 0.001,
      Length::Ft => 0.0003048,
      Length::Mi => 1.609344,
      Length::Km => 1.0,
    }
  }
}
impl Unit for Length {
  fn to_str(& self) -> String {
    match * self {
      Length::M => "m".to_string(),
      Length::Ft => "ft".to_string(),
      Length::Mi => "mi".to_string(),
      Length::Km => "km".to_string(),
    }
  }
}

/// Time units.
#[derive(Clone, Copy, PartialEq)]
pub enum Time {
  /// Seconds.
  S,
  /// Minutes.
  M,
  /// Hours.
  H,
}
impl Time {
  /// Wraps a value in seconds.
  pub fn seconds<V: Val>(v: V) -> U<V, Time> {
    U::mk(v, Time::S)
  }
  /// Wraps a value in minutes.
  pub fn minutes<V: Val>(v: V) -> U<V, Time> {
    U::mk(v, Time::M)
  }
  /// Wraps a value in hours.
  pub fn hours<V: Val>(v: V) -> U<V, Time> {
    U::mk(v, Time::H)
  }

  /// Factor to convert to seconds.
  pub fn to_seconds(& self) -> f64 {
    match * self {
      Time::S => 1.0,
      Time::M => 60.0,
      Time::H => 3_600.0,
    }
  }
  /// Factor to convert to minutes.
  pub fn to_minutes(& self) -> f64 {
    match * self {
      Time::S => 1.0 / 60.0,
      Time::M => 1.0,
      Time::H => 60.0,
    }
  }
  /// Factor to convert to hours.
  pub fn to_hours(& self) -> f64 {
    match * self {
      Time::S => 1.0 / 3_600.0,
      Time::M => 1.0 / 60.0,
      Time::H => 1.0,
    }
  }
}
impl Unit for Time {
  fn to_str(& self) -> String {
    match * self {
      Time::S => "s".to_string(),
      Time::M => "mn".to_string(),
      Time::H => "h".to_string(),
    }
  }
}

/// Fraction of units.
#[derive(Clone, Copy, PartialEq)]
pub struct Frac<Num, Den> {
  /// Numerator.
  num: Num,
  /// Denominator.
  den: Den,
}
impl<Num, Den> Frac<Num, Den> {
  /// Creates a fraction.
  pub fn mk(num: Num, den: Den) -> Self {
    Frac { num: num, den: den }
  }
  /// Numerator.
  #[inline]
  pub fn num(& self) -> & Num { & self.num }
  /// Denominator.
  #[inline]
  pub fn den(& self) -> & Den { & self.den }
}
impl Frac<Length, Time> {
  /// Factor to convert to miles per hour.
  pub fn to_miph(& self) -> f64 {
    self.num.to_miles() / self.den.to_hours()
  }
  /// Factor to convert to meter per hour.
  pub fn to_mph(& self) -> f64 {
    self.num.to_meters() / self.den.to_hours()
  }
  /// Factor to convert to meter per second.
  pub fn to_mps(& self) -> f64 {
    self.num.to_meters() / self.den.to_seconds()
  }
}
impl<Num: Unit, Den: Unit> Unit for Frac<Num, Den> {
  fn to_str(& self) -> String {
    format!("({}/{})", self.num.to_str(), self.den.to_str())
  }
}



/// Product of units.
#[derive(Clone, Copy, PartialEq)]
pub struct Prod<Lhs, Rhs> {
  /// Left-hand side.
  lhs: Lhs,
  /// Right-hand side.
  rhs: Rhs,
}
impl<Lhs, Rhs> Prod<Lhs, Rhs> {
  /// Creates a product.
  pub fn mk(lhs: Lhs, rhs: Rhs) -> Self {
    Prod { lhs: lhs, rhs: rhs }
  }
  /// Left-hand side.
  #[inline]
  pub fn lhs(& self) -> & Lhs { & self.lhs }
  /// Right-hand side.
  #[inline]
  pub fn rhs(& self) -> & Rhs { & self.rhs }
}
impl<Lhs: Unit, Rhs: Unit> Unit for Prod<Lhs, Rhs> {
  fn to_str(& self) -> String {
    format!("{}.{}", self.lhs.to_str(), self.rhs.to_str())
  }
}





