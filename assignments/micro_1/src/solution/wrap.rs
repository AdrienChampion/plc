//! Wrapper around a value and a unit.

use std::ops::{ Div, Mul, Neg } ;
use std::fmt ;
use std::fmt::Display ;

use solution::* ;

/// Trait for values.
#[allow(dead_code)]
pub trait Val: Clone + Copy {
  /// Conversion from an f64.
  fn from_f64(f64) -> Self ;
}
impl Val for f64 {
  fn from_f64(x: f64) -> Self { x as f64 }
}
impl Val for f32 {
  fn from_f64(x: f64) -> Self { x as f32 }
}
impl Val for usize {
  fn from_f64(x: f64) -> Self { x as usize }
}
impl Val for isize {
  fn from_f64(x: f64) -> Self { x as isize }
}

/// Wraps a value and a unit.
#[derive(Clone, Copy)]
pub struct U<V: Val, Uni: Unit> {
  /// Value.
  val: V,
  /// Unit.
  uni: Uni,
}
impl<V: Val, Uni: Unit> U<V, Uni> {
  /// Creates a wrapped value.
  #[inline]
  pub fn mk(val: V, uni: Uni) -> Self {
    U { val: val, uni: uni }
  }
  /// The value stored.
  pub fn val(& self) -> V { self.val }
  /// The unit of the value.
  pub fn uni(& self) -> Uni { self.uni }
}
impl <V: Val + Display, Uni: Unit> U<V, Uni> {
  /// Prints with prefix.
  pub fn print(& self, pref: & 'static str) {
    println!("{} {}", pref, self)
  }
}

impl<
  // The functions below multiply `self.val` of type|
  // `V` by an `f64` (conversion) and we put the    |
  // result in `self.val`.                          |
  // We thus want to multiply `V` by `f64` to get a |
  // `V`.                                           |
  V: Val + Mul<f64, Output = V> //                  |
  //       ^^^^^^^^^^^^^^^^^^^^~~~~~~~~~~~~~~~~~~~~~|
> U<V, Length> { //                                 |
  /// Converts to meters.                           |
  pub fn to_meters(mut self) -> Self { //           |
    self.val = self.val * self.uni.to_meters() ; // |
    // Multiplication has type `(V, f64) -> V`      |
    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^~~~~~~|
    self.uni = Length::M ;
    self
  }
  /// Converts to feet.
  pub fn to_feet(mut self) -> Self {
    self.val = self.val * self.uni.to_feet() ;
    self.uni = Length::Ft ;
    self
  }
  /// Converts to miles.
  pub fn to_miles(mut self) -> Self {
    self.val = self.val * self.uni.to_miles() ;
    self.uni = Length::Mi ;
    self
  }
  /// Converts to kilometers.
  pub fn to_kilometers(mut self) -> Self {
    self.val = self.val * self.uni.to_kilometers() ;
    self.uni = Length::Km ;
    self
  }
}

impl<
  // Same thing as above.
  V: Val + Mul<f64, Output = V>
> U<V, Time> {
  /// Converts to seconds.
  pub fn to_seconds(mut self) -> Self {
    self.val = self.val * self.uni.to_seconds() ;
    self.uni = Time::S ;
    self
  }
  /// Converts to minutes.
  pub fn to_minutes(mut self) -> Self {
    self.val = self.val * self.uni.to_minutes() ;
    self.uni = Time::M ;
    self
  }
  /// Converts to hours.
  pub fn to_hours(mut self) -> Self {
    self.val = self.val * self.uni.to_hours() ;
    self.uni = Time::H ;
    self
  }
}

impl<
  // Still the same thing.
  V: Val + Mul<f64, Output = V>
> U< V, Frac<Length, Time> > {
  /// Converts to miles per hour.
  pub fn to_miph(mut self) -> Self {
    self.val = self.val * self.uni.to_miph() ;
    self.uni = Frac::mk(Length::Mi, Time::H) ;
    self
  }
  /// Converts to meter per hour.
  pub fn to_mph(mut self) -> Self {
    self.val = self.val * self.uni.to_mph() ;
    self.uni = Frac::mk(Length::M, Time::H) ;
    self
  }
  /// Converts to meter per second.
  pub fn to_mps(mut self) -> Self {
    self.val = self.val * self.uni.to_mps() ;
    self.uni = Frac::mk(Length::M, Time::S) ;
    self
  }
}


impl<
  // Most generic version.
  // First we have the type for the values of the numerator.
  Val1: Val + Div<Val2, Output = Val3>,
  //          ^^^^^^^^^^^^^^^^^^^^^^^^
  // In the body of `div` we have `val: self.val / den.val`.
  // That is, we want a division with type `(Val1, Val2) -> Val3`.

  // Unit of numerator.
  Uni1: Unit,
  // Then the type for the values of the denominator.
  Val2: Val,
  // Unit of denominator.
  Uni2: Unit,
  // Then the type for the values of the result of the division.
  Val3: Val,
  // Note that the less generic case, where `Val1 = Val2 = Val3` is
  // included.
> Div<
  // Type we want to divide by.
  U<Val2, Uni2>
> for U<Val1, Uni1> {
  //  ^^^^^^^^^^^^^~~~ Type we want to divide.
  type Output = U< Val3, Frac<Uni1, Uni2> > ;
  //               ^^^^~~~ Division produces a value of type `Val3`.
  fn div(self, den: U<Val2, Uni2>) -> Self::Output {
    U {
      val: self.val / den.val,
      uni: Frac::mk(self.uni, den.uni),
    }
  }
}

// Same thing for `Mul`.
impl<
  // Most generic version.
  // First we have the type for the values of the lhs.
  Val1: Val + Mul<Val2, Output = Val3>,
  //          ^^^^^^^^^^^^^^^^^^^^^^^^
  // In the body of `mul` we have `val: self.val * den.val`.
  // That is, we want a multiplication with type `(Val1, Val2) -> Val3`.

  // Unit of lhs.
  Uni1: Unit,
  // Then the type for the values of the rhs.
  Val2: Val,
  // Unit of rhs.
  Uni2: Unit,
  // Then the type for the values of the result of the multiplication.
  Val3: Val,
  // Note that the less generic case, where `Val1 = Val2 = Val3` is
  // included.
> Mul<
  // Type we want to multiply by.
  U<Val2, Uni2>
> for U<Val1, Uni1> {
  //  ^^^^^^^^^^^^^~~~ Type we want to multiply.
  type Output = U< Val3, Prod<Uni1, Uni2> > ;
  //               ^^^^~~~ Division produces a value of type `Val3`.
  fn mul(self, den: U<Val2, Uni2>) -> Self::Output {
    U {
      val: self.val * den.val,
      uni: Prod::mk(self.uni, den.uni),
    }
  }
}

// Negation is easy.
// It's not very intuitive to allow negation to have a return type different
// from it's input type but why not.
//
// The unit however does not change. The negation of `10.seconds()` should
// have unit seconds.
impl<
  // Type of the values of `Self`.
  Val1: Val + Neg<Output = Val2>,
  // Unit of `Self` and result.
  Uni: Unit,
  // Type of the result of the negation.
  Val2: Val,
> Neg for U<Val1, Uni> {
  type Output = U<Val2, Uni> ;
  fn neg(self) -> Self::Output {
    U {
      val: - self.val,
      uni: self.uni,
    }
  }
}


impl<
  // Nothing special here, just forcing `V` to implement `Display` so that we
  // can print it.
  V: Val + Display, Uni: Unit
> fmt::Display for U<V, Uni> {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    write!(fmt, "{}{}", self.val(), self.uni().to_str())
  }
}