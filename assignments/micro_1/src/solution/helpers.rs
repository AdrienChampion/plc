/*! Helper traits.

There's several ways to provide helper functions depending on what
functionalities you want to provide and how. I did not give you any constraints
as to what these functionalities were so as long as you adapted this file to
work with `f64` it's fine.

The version given here is designed in the same spirit as the one you were
given. Integers have helper functions to make it convenient to generate `U`
wrappers around `f64` values.

```
extern crate units ;
fn main() {
  use units::solution::* ;
  use units::solution::helpers::{ ToUnits, ToVal } ;
  // Can use helper functions with `f64`.
  let seven = 7f64.meters() ;
  // Can also use a positive integer...
  let eleven = 11.seconds() ;
  // ...and a negative one.
  let minus_five = -5.miles() ;
  let speed = seven / eleven ;
  println!("speed: {}", speed)
}
```
*/

use solution::{ U, Val, Length, Time, Frac } ;

/// Can be converted to something implementing `Val`.
pub trait ToVal {
  /// Output value type.
  type Output: Val ;
  /// Converts to a value.
  fn to_val(& self) -> Self::Output ;
}

/// Convenience trait for units over primitive types.
pub trait ToUnits<V: Val> {
  /// Turns into meters.
  fn meters(& self)  -> U<V, Length> ;
  /// Turns into feet.
  fn feet(& self)    -> U<V, Length> ;
  /// Turns into miles.
  fn miles(& self)   -> U<V, Length> ;
  /// Turns into seconds.
  fn seconds(& self) -> U<V, Time> ;
  /// Turns into minutes.
  fn minutes(& self) -> U<V, Time> ;
  /// Turns into hours.
  fn hours(& self)   -> U<V, Time> ;
  /// Turns into miles per hour.
  fn miph(& self)    -> U<V, Frac<Length, Time>> ;
  /// Turns into miles per second.
  fn mips(& self)    -> U<V, Frac<Length, Time>> ;
  /// Turns into meter per hour.
  fn mph(& self)     -> U<V, Frac<Length, Time>> ;
  /// Turns into meter per second.
  fn mps(& self)     -> U<V, Frac<Length, Time>> ;
}

impl<T: ToVal> ToUnits<T::Output> for T {
  fn meters(& self)  -> U<T::Output, Length> {
    Length::meters( self.to_val() )
  }
  fn feet(& self)    -> U<T::Output, Length> {
    Length::feet( self.to_val() )
  }
  fn miles(& self)   -> U<T::Output, Length> {
    Length::miles( self.to_val() )
  }
  fn seconds(& self) -> U<T::Output, Time>   {
    Time::seconds( self.to_val() )
  }
  fn minutes(& self) -> U<T::Output, Time>   {
    Time::minutes( self.to_val() )
  }
  fn hours(& self)   -> U<T::Output, Time>   {
    Time::hours( self.to_val() )
  }
  fn miph(& self)    -> U<T::Output, Frac<Length, Time>> {
    U::mk( self.to_val(), Frac::mk(Length::Mi, Time::H) )
  }
  fn mips(& self)    -> U<T::Output, Frac<Length, Time>> {
    U::mk( self.to_val(), Frac::mk(Length::Mi, Time::S) )
  }
  fn mph(& self)     -> U<T::Output, Frac<Length, Time>> {
    U::mk( self.to_val(), Frac::mk(Length::M, Time::H) )
  }
  fn mps(& self)     -> U<T::Output, Frac<Length, Time>> {
    U::mk( self.to_val(), Frac::mk(Length::M, Time::S) )
  }
}


impl ToVal for f64 {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self }
}
impl ToVal for usize {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}
impl ToVal for isize {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}
impl ToVal for u8 {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}
impl ToVal for i8 {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}
impl ToVal for u16 {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}
impl ToVal for i16 {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}
impl ToVal for u32 {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}
impl ToVal for i32 {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}
impl ToVal for u64 {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}
impl ToVal for i64 {
  type Output = f64 ;
  fn to_val(& self) -> f64 { * self as f64 }
}

