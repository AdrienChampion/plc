//! Lazy bool stuff.

use std::fmt ;

use lazy_fun::{ LazyFun, AsStr, Apply, Eval } ;
use lazy_fun::Res::* ;

use lazy_usize::{ LazyUsize, UsizeCmp } ;

/// Lazy bool computation.
pub enum LazyBool {
  /// An actual bool value.
  Val(bool),
  /// A branching (ite).
  Branch(Box<LazyBool>, Box<LazyBool>, Box<LazyBool>),
  /// A lazy bool operator application.
  Laz(LazyFun<bool, LazyBool, bool>, Box<LazyBool>, Vec<LazyBool>),
  /// A comparison between lazy `usize`s.
  Cmp(UsizeCmp, Box<LazyUsize>, Box<LazyUsize>),
}
impl Eval<bool> for LazyBool {
  fn cost(& self) -> usize {
    use std::cmp::max ;
    use self::LazyBool::* ;
    match * self {
      Val(_) => 0,
      Branch(ref cond, ref thn, ref els) =>
        (* cond).cost() + max( (* thn).cost(), (* els).cost() ),
      Laz(_, ref head, ref tail) => {
        tail.iter().fold(
          tail.len() + (* head).cost(),
          |acc, arg| acc + arg.cost()
        )
      },
      Cmp(_, ref lhs, ref rhs) => 1 + (* lhs).cost() + (* rhs).cost(),
    }
  }
  fn eval(self) -> (bool, usize) {
    use self::LazyBool::* ;

    match self {
      Val(val) => (val, 0),

      Branch(cond, lft, rgt) => {
        let (cond, count_1) = cond.eval() ;
        if cond {
          let (res, count_2) = lft.eval() ;
          (res, count_1 + count_2)
        } else {
          let (res, count_2) = rgt.eval() ;
          (res, count_1 + count_2)
        }
      },

      Laz(fun, lhs, args) => {
        let (mut lhs, mut count) = lhs.eval() ;

        for arg in args.into_iter() {
          match fun.apply(lhs, arg) {
            Done(res, res_count) => {
              count += res_count ;
              lhs = res ;
              break
            },
            NYet(res, res_count) => {
              count += res_count ;
              lhs = res
            },
          }
        } ;

        (lhs, count)
      },

      Cmp(cmp, lhs, rhs) => {
        let (lhs, lhs_count) = lhs.eval() ;
        let (rhs, rhs_count) = rhs.eval() ;
        (cmp.cmp(lhs, rhs), lhs_count + rhs_count + 1)
      },
    }
  }
}
impl fmt::Display for LazyBool {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    use self::LazyBool::* ;
    match * self {
      Val(ref val) => write!(fmt, "{}", val),
      Branch(ref cond, ref lft, ref rgt) => write!(
        fmt, "(ite {} {} {})", cond, lft, rgt
      ),
      Laz(ref fun, ref lhs, ref args) => {
        try!( write!(fmt, "({} {}", fun.as_str(), lhs) ) ;
        for arg in args {
          try!( write!(fmt, " {}", arg) )
        } ;
        write!(fmt, ")")
      },
      Cmp(ref cmp, ref lhs, ref rhs) => write!(
        fmt, "({} {} {})", cmp, lhs, rhs
      ),
    }
  }
}


// /// Can be turned in a lazy bool.
// pub trait ToLazyBool {
//   /// Turns `self` into a lazy bool.
//   #[inline(always)]
//   fn to_lazy(self) -> LazyBool ;
// }
// impl ToLazyBool for bool {
//   fn to_lazy(self) -> LazyBool { LazyBool::Val(self) }
// }
// impl ToLazyBool for LazyBool {
//   fn to_lazy(self) -> LazyBool { self }
// }


/// Can be turned in a vector of lazy bools.
pub trait ToLazyBoolVec {
  /// Turns `self` into a vector of lazy bools.
  #[inline(always)]
  fn to_vec(self) -> Vec<LazyBool> ;
}
impl ToLazyBoolVec for bool {
  fn to_vec(self) -> Vec<LazyBool> { vec![ LazyBool::Val(self) ] }
}
impl ToLazyBoolVec for LazyBool {
  fn to_vec(self) -> Vec<LazyBool> { vec![ self ] }
}
impl ToLazyBoolVec for Vec<bool> {
  fn to_vec(self) -> Vec<LazyBool> {
    self.into_iter().map(|val| LazyBool::Val(val)).collect()
  }
}
impl ToLazyBoolVec for Vec<LazyBool> {
  fn to_vec(self) -> Vec<LazyBool> { self }
}

/// Can be used to create bool lazy operator applications.
pub trait LazyBoolOps<Args: ToLazyBoolVec>: Sized {
  /// Turns self in a lazy bool.
  #[inline(always)]
  fn to_lazy(self) -> LazyBool ;
  /// Creates a lazy `and` application.
  fn and(self, args: Args) -> LazyBool {
    LazyBool::Laz(
      LazyFun::and(), Box::new( self.to_lazy() ), args.to_vec()
    )
  }
  /// Creates a lazy `or` application.
  fn or(self, args: Args) -> LazyBool {
    LazyBool::Laz(
      LazyFun::or(), Box::new( self.to_lazy() ), args.to_vec()
    )
  }
}
impl<Args: ToLazyBoolVec> LazyBoolOps<Args> for bool {
  fn to_lazy(self) -> LazyBool { LazyBool::Val(self) }
}
impl<Args: ToLazyBoolVec> LazyBoolOps<Args> for LazyBool {
  fn to_lazy(self) -> LazyBool { self }
}

/// Can be used to create an if-then-else.
pub trait LazyIte<T> {
  /// Creates an if-then-else.
  fn ite(self, T, T) -> T ;
}
impl LazyIte<LazyBool> for bool {
  fn ite(self, lft: LazyBool, rgt: LazyBool) -> LazyBool {
    LazyBool::Branch(
      Box::new(LazyBool::Val(self)), Box::new(lft), Box::new(rgt)
    )
  }
}
impl LazyIte<LazyUsize> for bool {
  fn ite(self, lft: LazyUsize, rgt: LazyUsize) -> LazyUsize {
    LazyUsize::Branch(
      LazyBool::Val(self), Box::new(lft), Box::new(rgt)
    )
  }
}
impl LazyIte<LazyBool> for LazyBool {
  fn ite(self, lft: LazyBool, rgt: LazyBool) -> LazyBool {
    LazyBool::Branch(
      Box::new(self), Box::new(lft), Box::new(rgt)
    )
  }
}
impl LazyIte<LazyUsize> for LazyBool {
  fn ite(self, lft: LazyUsize, rgt: LazyUsize) -> LazyUsize {
    LazyUsize::Branch(
      self, Box::new(lft), Box::new(rgt)
    )
  }
}

/// Can be used to create lazy usize comparisons.
pub trait LazyUsizeCmp<Rhs> {
  /// Creates a `<=`.
  fn le(self, Rhs) -> LazyBool ;
  /// Creates a `<`.
  fn lt(self, Rhs) -> LazyBool ;
  /// Creates a `>`.
  fn gt(self, Rhs) -> LazyBool ;
  /// Creates a `>=`.
  fn ge(self, Rhs) -> LazyBool ;
}

impl LazyUsizeCmp<usize> for usize {
  fn le(self, rhs: usize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Le,
      Box::new( LazyUsize::Val(self) ),
      Box::new( LazyUsize::Val(rhs) )
    )
  }
  fn lt(self, rhs: usize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Lt,
      Box::new( LazyUsize::Val(self) ),
      Box::new( LazyUsize::Val(rhs) )
    )
  }
  fn gt(self, rhs: usize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Gt,
      Box::new( LazyUsize::Val(self) ),
      Box::new( LazyUsize::Val(rhs) )
    )
  }
  fn ge(self, rhs: usize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Ge,
      Box::new( LazyUsize::Val(self) ),
      Box::new( LazyUsize::Val(rhs) )
    )
  }
}
impl LazyUsizeCmp<usize> for LazyUsize {
  fn le(self, rhs: usize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Le,
      Box::new( self ),
      Box::new( LazyUsize::Val(rhs) )
    )
  }
  fn lt(self, rhs: usize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Lt,
      Box::new( self ),
      Box::new( LazyUsize::Val(rhs) )
    )
  }
  fn gt(self, rhs: usize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Gt,
      Box::new( self ),
      Box::new( LazyUsize::Val(rhs) )
    )
  }
  fn ge(self, rhs: usize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Ge,
      Box::new( self ),
      Box::new( LazyUsize::Val(rhs) )
    )
  }
}
impl LazyUsizeCmp<LazyUsize> for usize {
  fn le(self, rhs: LazyUsize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Le,
      Box::new( LazyUsize::Val(self) ),
      Box::new(rhs)
    )
  }
  fn lt(self, rhs: LazyUsize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Lt,
      Box::new( LazyUsize::Val(self) ),
      Box::new(rhs)
    )
  }
  fn gt(self, rhs: LazyUsize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Gt,
      Box::new( LazyUsize::Val(self) ),
      Box::new(rhs)
    )
  }
  fn ge(self, rhs: LazyUsize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Ge,
      Box::new( LazyUsize::Val(self) ),
      Box::new(rhs)
    )
  }
}
impl LazyUsizeCmp<LazyUsize> for LazyUsize {
  fn le(self, rhs: LazyUsize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Le,
      Box::new(self),
      Box::new(rhs)
    )
  }
  fn lt(self, rhs: LazyUsize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Lt,
      Box::new(self),
      Box::new(rhs)
    )
  }
  fn gt(self, rhs: LazyUsize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Gt,
      Box::new(self),
      Box::new(rhs)
    )
  }
  fn ge(self, rhs: LazyUsize) -> LazyBool {
    LazyBool::Cmp(
      UsizeCmp::Ge,
      Box::new(self),
      Box::new(rhs)
    )
  }
}