//! Lazy function module.

use lazy_funs ;

use lazy_bool::LazyBool ;
use lazy_usize::LazyUsize ;

/// Result of a lazy computation.
///
/// The `usize` is number of operations performed to get this result.
pub enum Res<Out> {
  /// Not done yet: computation so far is not final.
  NYet(Out, usize),
  /// Done, no need to look at the remaining operands.
  Done(Out, usize),
}

/// Has a string representation.
pub trait AsStr {
  /// String representation of something.
  #[inline(always)]
  fn as_str(& self) -> & 'static str ;
}
/// Can be applied to two arguments.
pub trait Apply<Lhs, Rhs, Out> {
  /// Applies to two arguments.
  #[inline(always)]
  fn apply(& self, Lhs, Rhs) -> Res<Out> ;
}
/// Can be evaluated.
pub trait Eval<Out> {
  /// Evaluation. The second element of the return pair is the number of
  /// operations performed to get the result.
  fn eval(self) -> (Out, usize) ;
  /// Number of operations needed for eager evaluation.
  fn cost(& self) -> usize ;
}

/// A lazy function with a string representation, and the function itself.
pub struct LazyFun<Lhs, Rhs, Out> {
  /// String representation.
  str_rep: & 'static str,
  /// Lazy function.
  fun: Box< Fn(Lhs, Rhs) -> Res<Out> >,
}
impl<Lhs, Rhs, Out> AsStr for LazyFun<Lhs, Rhs, Out> {
  fn as_str(& self) -> & 'static str { self.str_rep }
}
impl Apply<usize, LazyUsize, usize> for LazyFun<usize, LazyUsize, usize> {
  fn apply(& self, lhs: usize, rhs: LazyUsize) -> Res<usize> {
    (* self.fun)(lhs, rhs)
  }
}
impl Apply<bool, LazyBool, bool> for LazyFun<bool, LazyBool, bool> {
  fn apply(& self, lhs: bool, rhs: LazyBool) -> Res<bool> {
    (* self.fun)(lhs, rhs)
  }
}
impl LazyFun<bool, LazyBool, bool> {
  pub fn and() -> Self {
    LazyFun { str_rep: "and", fun: Box::new(lazy_funs::boo::lazy_and) }
  }
  pub fn or() -> Self {
    LazyFun { str_rep: "or", fun: Box::new(lazy_funs::boo::lazy_or) }
  }
}
impl LazyFun<usize, LazyUsize, usize> {
  pub fn add() -> Self {
    LazyFun { str_rep: "+", fun: Box::new(lazy_funs::int::lazy_add) }
  }
  pub fn sub() -> Self {
    LazyFun { str_rep: "-", fun: Box::new(lazy_funs::int::lazy_sub) }
  }
  pub fn mul() -> Self {
    LazyFun { str_rep: "*", fun: Box::new(lazy_funs::int::lazy_mul) }
  }
  pub fn div() -> Self {
    LazyFun { str_rep: "/", fun: Box::new(lazy_funs::int::lazy_div) }
  }
  pub fn m0d() -> Self {
    LazyFun { str_rep: "%", fun: Box::new(lazy_funs::int::lazy_mod) }
  }
}