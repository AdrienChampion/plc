//! Lazy usize stuff.

use std::fmt ;

use lazy_fun::{ LazyFun, AsStr, Apply, Eval } ;
use lazy_fun::Res::* ;

use lazy_bool::LazyBool ;

/// Comparison over usizes.
pub enum UsizeCmp {
  Le, Lt, Gt, Ge
}
impl UsizeCmp {
  /// Compares two `usize`s.
  pub fn cmp(self, lhs: usize, rhs: usize) -> bool {
    use self::UsizeCmp::* ;
    match self {
      Le => lhs <= rhs,
      Lt => lhs < rhs,
      Gt => lhs > rhs,
      Ge => lhs >= rhs,
    }
  }
}
impl fmt::Display for UsizeCmp {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    use self::UsizeCmp::* ;
    match * self {
      Le => write!(fmt, "<="),
      Lt => write!(fmt, "<"),
      Gt => write!(fmt, ">"),
      Ge => write!(fmt, ">="),
    }
  }
}

/// Lazy usize computation.
pub enum LazyUsize {
  /// An actual usize value.
  Val(usize),
  /// A branching (ite).
  Branch(LazyBool, Box<LazyUsize>, Box<LazyUsize>),
  /// A lazy usize operator application.
  Laz(LazyFun<usize, LazyUsize, usize>, Box<LazyUsize>, Vec<LazyUsize>),
}
impl Eval<usize> for LazyUsize {
  fn cost(& self) -> usize {
    use std::cmp::max ;
    use self::LazyUsize::* ;
    match * self {
      Val(_) => 0,
      Branch(ref cond, ref thn, ref els) =>
        (* cond).cost() + max( (* thn).cost(), (* els).cost() ),
      Laz(_, ref head, ref tail) => {
        tail.iter().fold(
          tail.len() + (* head).cost(),
          |acc, arg| acc + arg.cost()
        )
      },
    }
  }
  fn eval(self) -> (usize, usize) {
    use self::LazyUsize::* ;

    match self {
      Val(val) => (val, 0),

      Branch(cond, lft, rgt) => {
        let (cond, count_1) = cond.eval() ;
        if cond {
          let (res, count_2) = lft.eval() ;
          (res, count_1 + count_2)
        } else {
          let (res, count_2) = rgt.eval() ;
          (res, count_1 + count_2)
        }
      },

      Laz(fun, lhs, args) => {
        let (mut lhs, mut count) = lhs.eval() ;

        for arg in args.into_iter() {
          match fun.apply(lhs, arg) {
            Done(res, res_count) => {
              count += res_count ;
              lhs = res ;
              break
            },
            NYet(res, res_count) => {
              count += res_count ;
              lhs = res
            },
          }
        } ;

        (lhs, count)
      },
    }
  }
}
impl fmt::Display for LazyUsize {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    use self::LazyUsize::* ;
    match * self {
      Val(ref val) => write!(fmt, "{}", val),
      Branch(ref cond, ref lft, ref rgt) => write!(
        fmt, "(ite {} {} {})", cond, lft, rgt
      ),
      Laz(ref fun, ref lhs, ref args) => {
        try!( write!(fmt, "({} {}", fun.as_str(), lhs) ) ;
        for arg in args {
          try!( write!(fmt, " {}", arg) )
        } ;
        write!(fmt, ")")
      },
    }
  }
}


// /// Can be turned in a lazy usize.
// pub trait ToLazyUsize {
//   /// Turns `self` into a lazy usize.
//   #[inline(always)]
//   fn to_lazy(self) -> LazyUsize ;
// }
// impl ToLazyUsize for usize {
//   fn to_lazy(self) -> LazyUsize { LazyUsize::Val(self) }
// }
// impl ToLazyUsize for LazyUsize {
//   fn to_lazy(self) -> LazyUsize { self }
// }


/// Can be turned in a vector of lazy usizes.
pub trait ToLazyUsizeVec {
  /// Turns `self` into a vector of lazy usizes.
  #[inline(always)]
  fn to_vec(self) -> Vec<LazyUsize> ;
}
impl ToLazyUsizeVec for usize {
  fn to_vec(self) -> Vec<LazyUsize> { vec![ LazyUsize::Val(self) ] }
}
impl ToLazyUsizeVec for LazyUsize {
  fn to_vec(self) -> Vec<LazyUsize> { vec![ self ] }
}
impl ToLazyUsizeVec for Vec<usize> {
  fn to_vec(self) -> Vec<LazyUsize> {
    self.into_iter().map(|val| LazyUsize::Val(val)).collect()
  }
}
impl ToLazyUsizeVec for Vec<LazyUsize> {
  fn to_vec(self) -> Vec<LazyUsize> { self }
}

/// Can be used to create usize lazy operator applications.
pub trait LazyUsizeOps<Args: ToLazyUsizeVec>: Sized {
  /// Turns self in a lazy usize.
  #[inline(always)]
  fn to_lazy(self) -> LazyUsize ;
  /// Creates a lazy `+` application.
  fn add(self, args: Args) -> LazyUsize {
    LazyUsize::Laz(
      LazyFun::add(), Box::new( self.to_lazy() ), args.to_vec()
    )
  }
  /// Creates a lazy `-` application.
  fn sub(self, args: Args) -> LazyUsize {
    LazyUsize::Laz(
      LazyFun::sub(), Box::new( self.to_lazy() ), args.to_vec()
    )
  }
  /// Creates a lazy `*` application.
  fn mul(self, args: Args) -> LazyUsize {
    LazyUsize::Laz(
      LazyFun::mul(), Box::new( self.to_lazy() ), args.to_vec()
    )
  }
  /// Creates a lazy `/` application.
  fn div(self, args: Args) -> LazyUsize {
    LazyUsize::Laz(
      LazyFun::div(), Box::new( self.to_lazy() ), args.to_vec()
    )
  }
  /// Creates a lazy `%` application.
  fn modulo(self, args: Args) -> LazyUsize {
    LazyUsize::Laz(
      LazyFun::m0d(), Box::new( self.to_lazy() ), args.to_vec()
    )
  }
}
impl<Args: ToLazyUsizeVec> LazyUsizeOps<Args> for usize {
  fn to_lazy(self) -> LazyUsize { LazyUsize::Val(self) }
}
impl<Args: ToLazyUsizeVec> LazyUsizeOps<Args> for LazyUsize {
  fn to_lazy(self) -> LazyUsize { self }
}