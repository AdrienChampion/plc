/*! Lazy functions.

# Group

- Adrien Champion
- Richard Blair

# Comments about the homework

Blah

*/

#![allow(unused_imports, unused_variables)]

/// Bool lazy functions.
pub mod boo {
  use Res ;
  use Res::* ;
  use lazy_bool::LazyBool ;
  use lazy_fun::Eval ;

  /// Lazy conjunction.
  pub fn lazy_and(lhs: bool, rhs: LazyBool) -> Res<bool> {
    unimplemented!()
  }
  /// Lazy disjunction.
  pub fn lazy_or(lhs: bool, rhs: LazyBool) -> Res<bool> {
    unimplemented!()
  }
}

/// Usize lazy functions.
pub mod int {
  use Res ;
  use Res::* ;
  use lazy_usize::LazyUsize ;
  use lazy_fun::Eval ;

  /// Lazy usize addition.
  pub fn lazy_add(lhs: usize, rhs: LazyUsize) -> Res<usize> {
    unimplemented!()
  }
  /// Lazy usize substraction.
  pub fn lazy_sub(lhs: usize, rhs: LazyUsize) -> Res<usize> {
    unimplemented!()
  }
  /// Lazy usize multiplication.
  pub fn lazy_mul(lhs: usize, rhs: LazyUsize) -> Res<usize> {
    unimplemented!()
  }
  /// Lazy usize division.
  pub fn lazy_div(lhs: usize, rhs: LazyUsize) -> Res<usize> {
    unimplemented!()
  }
  /// Lazy usize modulo.
  pub fn lazy_mod(lhs: usize, rhs: LazyUsize) -> Res<usize> {
    unimplemented!()
  }
}