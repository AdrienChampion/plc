/*! Lazy bool and int expressions.

This crate provides simple lazy expressions that evaluate to `bool` or
`usize`. An expression is either a value or the application of an operation.

**NB:** (almost) all operations are *n-ary*. They take any number (>= 1) of
arguments. Typically `7 - 1 - 1` is understood as `(- 7 1 1)` in prefix
notation: the application of `-` to the three arguments `7`, `1` and `1`.

Now,
[`LazyUsize` is the tree for int expressions](lazy_usize/enum.LazyUsize.html)
and looks like this:

```[no_run]
pub enum LazyUsize {
    Val(usize),
    Branch(LazyBool, Box<LazyUsize>, Box<LazyUsize>),
    Laz(LazyFun<usize, LazyUsize, usize>, Box<LazyUsize>, Vec<LazyUsize>),
}
```

* `Val` is an actual usize value,
* `Branch` represents an if-then-else expression (containing the condition and
  the two branches),
* `Laz` is a lazy operation, containing
    * the operation to apply,
    * the first argument, and
    * the other arguments.

So for instance, the expression

```
7 + (8 * 0 * 2) + 3 + (7 - 1 - 1)
```

which in prefix notation is

```
( + 7 (* 8 0 2) 3 (- 7 1 1) )
```

would basically be represented as follows:

```
Laz(
  +,
  Val(7),
  vec![
    Laz(*, Val(8), vec![Val(0), Val(2)]),
    Val(3),
    Laz(-, Val(7), vec![Val(1), Val(1)])
  ]
)
```

While the function evaluating such expressions is implemented, you have to
write the code for each particular operation (`+`, `*`, *etc.*). It's not very
hard to do if you understand what's going on. Read the instructions carefully.

# Submissions

[lazy_funs.rs](file:///Users/uchuu/Documents/bitbucket/plc_assignments/lazy/target/doc/lazy/lazy_funs/index.html)
**is the only file you will modify and submit**.

Unlike previous homeworks, this time we ask you to have **one submission per
group**. Do not forget to list all the members of the groups in the comments
of `lazy_funs.rs`.

# Instructions

The functions you have to implement all have the same shape. Let's look at
[`lazy_funs::int::lazy_mul`](lazy_funs/int/fn.lazy_mul.html) for instance:

```
pub fn lazy_mul(lhs: usize, rhs: LazyUsize) -> Res<usize>
```

Note that while `lhs` is an actual `usize` value, `rhs` is a `LazyUsize` (a
lazy evaluation not yet computed). Why? Because maybe it's not necessary to
know the value of `rhs` to know the result of the multiplication. (If `lhs` is
`0` for multiplication for instance.)

Consider the problem of evaluating `7 * (1 - 1) * 2`. Let's rewrite it in
prefix notation: `(* 7 (- 1 1) 2)`. When evaluating the multiplication, we must
evaluate the first argument: it's an actual value here, so there's no real
evaluation going on. It's just `7`.

The evaluator will then call `lazy_mul`, giving it `7` for `lhs` and the lazy
expression `(- 1 1)` for `rhs`. `lazy_mul` must return the result of the
multiplication along with another important information: *"do we care about the
remaining arguments of the operation?"*.
In this example, `lazy_mul` will evaluate `(- 1 1)` to `0`. The result of the
multiplication is 0. *Do we care about the remaining arguments of the
multiplication?* No, whatever they are, the result will be `0`.

This is expressed by [`Res`](enum.Res.html), the return type of the lazy
functions:

```
pub enum Res<Out> {
    NYet(Out, usize),
    Done(Out, usize),
}
```

Both contain the result of the operation of type `Out`. `Out = usize` for
`lazy_mul`. The variants differ in that

* `Done` means that the remaining arguments **won't** change the result,
* `NYet` means that the remaining arguments **can** change the result.

In addition to the result of type `Out`, both variants have a `usize` value.
It is the number of operations performed to produce the result.
The [`eval` function from the `Eval` trait](trait.Eval.html#tymethod.eval) is
what you'll use to evaluate `rhs`.

```
let (rhs_val, rhs_count) = rhs.eval() ;
//   ^^^^^^^| ^^^^^^^^^|- number of operations performed to evaluate `rhs`.
//          |
//          |------------ value `rhs` evaluates to
```

In the example above, `rhs` is `(- 1 1)`. It evaluates to `0` in `1` operation
(`1 - 1`).

We can now compute the result and look at what it is to decide whether we care
about the remaining arguments or not. In the case of multiplication:

```
if rhs_val == 0 {
  // 0 times anything is zero, remaining arguments don't matter.
  Done(0, rhs_count)
} else {
  // Not zero, remaining arguments of multiplication matter.
  NYet(lhs * rhs_val, rhs_count + 1)
  //   ^^^^^^^^^^^^^            ^^^~ we just did one operation to compute the
  //                                 result
}
```

Not that in the case of multiplication, if `lhs` was `0` we would have returned
`Done(0,0)` right away. No computation is needed to multiply `0` by something.

## Tasks

The tests for each task pass iff the evaluation yields the right result, and
reports the right, *minimal* number of operations to obtain this result.

### Task 1 (2 points)

Implement

* [`lazy_funs::int::lazy_add`](lazy_funs/int/fn.lazy_add.html), and
* [`lazy_funs::int::lazy_sub`](lazy_funs/int/fn.lazy_sub.html).

These are rather easy to implement as all the arguments in additions /
substractions matter. These functions just evaluate `rhs`, compute the result,
and return a `Res::NYet` with updated operation count.

To get the points you must past the tests for `cargo test --features="task_1"`.

### Task 2 (4 points)

Implement the remaining functions in `lazy_funs::int`:

* [`lazy_funs::int::lazy_mul`](lazy_funs/int/fn.lazy_mul.html),
* [`lazy_funs::int::lazy_div`](lazy_funs/int/fn.lazy_div.html), and
* [`lazy_funs::int::lazy_mod`](lazy_funs/int/fn.lazy_mod.html).

These are a bit more tricky as depending on the value of `lhs`, `rhs` and the
result the remaining arguments might not matter.

To get the points you must past the tests for `cargo test --features="task_2"`.

### Task 3 (3 points)

Implement the functions in [`lazy_funs::boo`](lazy_funs/boo/index.html):

* [`lazy_funs::boo::lazy_and`](lazy_funs/boo/fn.lazy_and.html), and
* [`lazy_funs::boo::lazy_or`](lazy_funs/boo/fn.lazy_or.html).

These functions are very similar to the ones from task 2: you will have to
look at the value of `lhs` and `rhs` to decide if the remaining arguments
matter or not.

To get the points you must past the tests for `cargo test --features="task_3"`.

*/


/// Macro defining a DSL for lazy expressions.
#[macro_export]
macro_rules! mk_lazy {
  (( ite $cond:tt $thn:tt $els:tt )) => (
    mk_lazy!($cond).ite( mk_lazy!($thn), mk_lazy!($els) )
  ) ;
  (( and $fst:tt $( $e:tt )+ )) => (
    mk_lazy!($fst).and( vec![ $( mk_lazy!($e) ),+ ] )
  ) ;
  (( or $fst:tt $( $e:tt )+ )) => (
    mk_lazy!($fst).or( vec![ $( mk_lazy!($e) ),+ ] )
  ) ;
  (( <= $lhs:tt $rhs:tt )) => (
    mk_lazy!($lhs).le( mk_lazy!($rhs) )
  ) ;
  (( < $lhs:tt $rhs:tt )) => (
    mk_lazy!($lhs).lt( mk_lazy!($rhs) )
  ) ;
  (( > $lhs:tt $rhs:tt )) => (
    mk_lazy!($lhs).gt( mk_lazy!($rhs) )
  ) ;
  (( >= $lhs:tt $rhs:tt )) => (
    mk_lazy!($lhs).ge( mk_lazy!($rhs) )
  ) ;
  (( + $fst:tt $( $e:tt )+ )) => (
    mk_lazy!($fst).add( vec![ $( mk_lazy!($e) ),+ ] )
  ) ;
  (( - $fst:tt $( $e:tt )+ )) => (
    mk_lazy!($fst).sub( vec![ $( mk_lazy!($e) ),+ ] )
  ) ;
  (( * $fst:tt $( $e:tt )+ )) => (
    mk_lazy!($fst).mul( vec![ $( mk_lazy!($e) ),+ ] )
  ) ;
  (( / $fst:tt $( $e:tt )+ )) => (
    mk_lazy!($fst).div( vec![ $( mk_lazy!($e) ),+ ] )
  ) ;
  (( % $fst:tt $( $e:tt )+ )) => (
    mk_lazy!($fst).modulo( vec![ $( mk_lazy!($e) ),+ ] )
  ) ;
  (true) => ( $crate::lazy_bool::LazyBool::Val(true) ) ;
  (false) => ( $crate::lazy_bool::LazyBool::Val(false) ) ;
  ($e:expr) => ( $crate::lazy_usize::LazyUsize::Val($e) ) ;
}


/// Tries to add a result to a stack.
/// 
/// Supports Usize and Bool lazy constructs.
#[macro_use]
macro_rules! try_add_to_stack {
  (usize $stack:expr, $to_add:expr) => (
    match $stack.last_mut() {
      Some( & mut (_, _, ref mut args) ) => args.push(
        $crate::lazy_usize::LazyUsize::Val($to_add)
      ),
      None => return $to_add
    }
  ) ;
  (bool $stack:expr, $to_add:expr) => (
    match $stack.last_mut() {
      Some( & mut (_, _, ref mut args) ) => args.push(
        $crate::lazy_bool::LazyBool::Val($to_add)
      ),
      None => return $to_add
    }
  ) ;
}

pub mod lazy_funs ;
mod lazy_fun ;
pub mod lazy_bool ;
pub mod lazy_usize ;

use lazy_usize::* ;
use lazy_bool::* ;
pub use lazy_fun::* ;

/// Entry point.
fn main() {
  let lazy = mk_lazy!(
    (or
      (<=
        ( + 7 (* 0 3 2 1) ( ite false (* 0 3 2 1) (% 3 7 3 8) ) )
        (* 7 6 (- 3 2))
      )
      (> (* 3 7 3 6 7 35) (+ 2 3 7))
    )
  ) ;

  println!("lazy: {}", lazy) ;

  let eager_cost = lazy.cost() ;
  let (res, ops) = lazy.eval() ;
  println!("") ;
  println!("res: {} in {} operations ({} if eager)", res, ops, eager_cost) ;
}





// |===| Tests.





#[cfg(test)]
mod test {
  use std::cmp::PartialEq ;
  use std::fmt::Display ;
  use lazy_fun::Eval ;
  pub fn test<
    Out: Display + PartialEq, Expr: Eval<Out> + Display
  >(expr: Expr, expected_res: Out, expected_count: usize) {
    use std::cmp::Ordering::* ;

    println!("") ;
    println!("|===|") ;
    println!("| Expression:") ;
    println!("|   {}", expr) ;
    let max_count = expr.cost() ;
    let (res, count) = expr.eval() ;
    
    if res != expected_res {
      println!("| ") ;
      println!("| Error:") ;
      println!("|   expected the expression to evaluate to {}", expected_res) ;
      println!("|   but got {}", res) ;
      println!("|===|") ;
      println!("") ;
      panic!("evaluation result mismatch")
    } else {
      println!("| ") ;
      println!("| Evaluates correctly to {}", res)
    }

    match count.cmp(& expected_count) {
      Less => {
        println!("| ") ;
        println!("| Error:") ;
        println!("|   expected a minimum of {} operations", expected_count) ;
        println!("|   but your evaluation performed {} operations", count) ;
        println!("| Either my implementation is non-optimal,") ;
        println!(
          "| or you do not compute correctly the number of operations."
        ) ;
        println!("| (It's probably the latter.)") ;
        println!("|===|") ;
        println!("") ;
        panic!("suspiciously low number of operations")
      },
      Greater if count > max_count => {
        println!("| ") ;
        println!("| Error:") ;
        println!("|   expected a minimum of {} operations", expected_count) ;
        println!("|   but your evaluation performed {} operations", count) ;
        println!(
          "| That's actually more than eager evaluation ({} operations).",
          max_count
        ) ;
        println!("|===|") ;
        println!("") ;
        panic!("very high number of operations")
      },
      Greater => {
        println!("| ") ;
        println!("| Error:") ;
        println!(
          "|   expected {} operations for this evaluation", expected_count
        ) ;
        println!("|   but your evaluation performed {} operations", count) ;
        println!("|===|") ;
        println!("") ;
        panic!("high number of operations") ;
      },
      Equal => (),
    } ;
  }
}

#[cfg(test)]
macro_rules! mk_test {
  ($name:ident for $e:tt expecting ($res:expr, $count:expr)) => (
    #[test]
    #[allow(unused_imports)]
    fn $name() {
      use lazy_bool::* ;
      use lazy_usize::* ;
      $crate::test::test( mk_lazy!($e), $res, $count )
    }
  ) ;
}

#[cfg(
  all( test, not(feature="task_2"), not(feature="task_3") )
)]
mod task_1 {
  mk_test!{
    test_add_1 for (+ 0 1 2 3 4) expecting (10, 4)
  }
  mk_test!{
    test_add_2 for (+ 7 3 9 4) expecting (23, 3)
  }
  mk_test!{
    test_add_3 for (+ 1 0 0 0) expecting (1, 3)
  }

  mk_test!{
    test_sub_1 for (- 42 1 2 3 4 7) expecting (25, 5)
  }
  mk_test!{
    test_sub_2 for (- 0 0 0 0) expecting (0, 3)
  }
  mk_test!{
    test_sub_3 for (- 1 0 0 0) expecting (1, 3)
  }
}

#[cfg(
  all( test, not(feature="task_1"), not(feature="task_3") )
)]
mod task_2 {
  mk_test!{
    test_mul_1 for (* 7 6 2) expecting (84, 2)
  }
  mk_test!{
    test_mul_2 for (* 2 1 0 1) expecting (0, 1)
  }
  mk_test!{
    test_mul_3 for (* 0 (+ 3 2) (- 7 5)) expecting (0, 0)
  }

  mk_test!{
    test_div_1 for (/ 7 6 2) expecting (0, 2)
  }
  mk_test!{
    test_div_2 for (/ 8 (- 7 6) 3 1) expecting (2, 4)
  }
  mk_test!{
    test_div_3 for (/ (* 7 (- 3 2 1)) (+ 3 2) (- 7 5)) expecting (0, 2)
  }
  mk_test!{
    test_div_4 for (/ (* 7 (- 10000 2 1)) (* 10 7) (- 7 5)) expecting (499, 7)
  }

  mk_test!{
    test_mod_1 for (% 7 6 2) expecting (1, 2)
  }
  mk_test!{
    test_mod_2 for (% 8 (- 199 (* 3 2)) 10000) expecting (8, 4)
  }
  mk_test!{
    test_mod_3 for (% (- 1 (/ 10 8)) 3 7 4) expecting (0, 2)
  }
}

#[cfg(
  all( test, not(feature="task_1"), not(feature="task_2") )
)]
mod task_3 {
  mk_test!{
    test_and_1
    for (and (>= (* 1 7 0 2) (- 7 2)) (> (* 10 2) (% 0 7)))
    expecting (false, 3)
  }
  mk_test!{
    test_and_2
    for (and
      (> (+ 7 3 9 4) (- 2 1))
      (<= (* 2 7 3) 700)
      (>= (- 100 3) (+ 75 7 3 10))
    ) expecting (true, 15)
  }
  mk_test!{
    test_and_3
    for (and
      (> (+ 7 3 9 4) (- 2 1))
      (>= (* 2 7 3) 700)
      (>= (- 100 3) (+ 75 7 3 10))
    ) expecting (false, 8)
  }

  mk_test!{
    test_or_1
    for (or (>= (* 1 7 0 2) (- 7 2)) (> (* 10 2) (% 0 7)))
    expecting (true, 5)
  }
  mk_test!{
    test_or_2
    for (or
      (> (+ 7 4 9 4) (- 2 1))
      (<= (* 9 7 3) 700)
      (>= (- 100 3) (+ 75 7 3 10))
    ) expecting (true, 5)
  }
  mk_test!{
    test_or_3
    for (or
      (< (+ 7 3 9 4) (- 2 1))
      (>= (* 2 7 3) 700)
      (<= (- 100 3 1) (+ 75 7 3 10))
    ) expecting (false, 16)
  }
}