# Micro assignment 2

**NB: you are not allowed to do this assignment in groups.**

Choose **three programming languages NOT in the blacklist below**. Programming
languages come with an implementation, for instance the `rustc` compiler for
Rust, the java compiler and virtual machine for java, the python runtime,
*etc.*

For the languages you chose, rate their typical implementation on an
interpreted VS. compiled scale from 0 to 5. Also, write a few lines explaining
the workflow of these languages to go from sources to runtime. See below for
some examples.

I suggest you simply go to the wikipedia page of the language, read a bit about
it, and then write a few lines answering the question. You should not spend
more than 1 hour on this, I'd say 30 minutes is enough if you're good at
information hunting.

Each (implementation of a) language you discuss is worth 2 points. You can get
1 extra credit point by discussing, in one or two sentences, something
interesting about one of the languages. I'm not asking for any details, see the
Rust example below for instance (note the extra credit part is prefixed with
`>`, *i.e.* markdown *quote*).


## Blacklist

- rust
- java
- javascript
- python
- C
- C++

## Suggested languages

- nim
- brainfuck
- whitespace
- lisp
- haskell
- ocaml
- pascal
- scala
- F#
- C#

# Example

## Rust: 4 / 5

The Rust compiler generates LLVM code. Since LLVM is so close to hardware,
there is relatively little interpretation going on. Therefore, Rust is very
close to being purely compiled.

> Also, it seems Rust relies on an unusual model for memory management called
> *regional memory management* that seems to provide very strong guarantees
> at compile-time.

## Java: 3 / 5

While Java typically has a compilation phase, the (byte) code generated is
still rather hi-level and at runtime there is quite a lot of interpretation
going on. It is thus in the middle between purely compiled and purely
interpreted.

## Bash: 0 / 5

Bash is a script language like Python and Javascript. At runtime, each bash
instruction is interpreted. There is basically no compilation at all, so bash
is really a purely interpreted language.



# YOUR NAME HERE

## Language_1: _ / 5

Blah.

## Language_2: _ / 5

Blah.

## Language_3: _ / 5

Blah.

