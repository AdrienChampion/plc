/*! Brainfrak interpreter.

Homework 1 was about parsing (and lexing) and constructing a structured
representation of user-provided information, so that the software can use this
information to do whatever it is supposed to do. For this homework we go *one
level upward*: you will work on the structured information obtained after
parsing. This structure represents a brainfrak program, which is composed of
commands that your code will interpret.


So, the goal of this homework is to implement an interpreter for the brainfrak
language. You are not expected to spend a lot of time on it, *if you have the
right workflow* you could be done in one afternoon (3~5 hours).

The idea is to implement the [`run` function](method.run) in this module. It's
basically a big loop that treats commands one by one. As an interpreter, it
should look at what the command is and act accordingly, generally by using the
*runtime*.

The runtime is [a structure I provide][runtime] that handles the memory and the
environment stack for you. Well, it handles the low-level details, but you
still have to tell it what to do for each command.

I suggest you **do not write any code at all** before you completely
understand both the language ([see here](../index.html)) and the types I
provide, [`Runtime`][runtime] and [`Cmd`][cmd]. Once you understand them, most
of what you have to write should be very simple, and the difficult parts will
be much easier to deal with.

Last, read [the explanation of the code I provide for the run function](#run-function).


# Submissions

You will submit your `interpreter.rs` file and only that file. Write your name
and the name of your teammate(s) (if any) in the documentation of the `run`
function. **All members of the group must submit a file.**

The tests for each tasks use files in the `rsc` (resource) folder. When dealing
with a problem in your code, it's probably better to run the interpreter
directly on the file causing an error instead of running the tests everytime.


# Task 1 (3 points)

```none
cargo test --features task_1
```

Implement full support for pure brainfuck commands. For this task you don't
even need to use the runtime's environment. The main difficulty of this task
is how to handle loops, the rest is completely obvious.

About the loop command **do not** start coding right away. **Think** about and
anticipate the problems, some solution(s), and then see if there could be any
more problems. Once you think you got it, that's when you start coding.
Do not write code before you know relatively precisely what that code's going
to do. It will save you a lot of time.

Because most command can fail, many of the functions defined on the runtime
return a Result, and so does the `run` function. The
[`try!`](https://doc.rust-lang.org/stable/std/macro.try!.html) macro is
**extremely useful here** and will simplify your code. Writing
`try!( expr )` is basically the same as writing

```no_use
match expr {
  Ok(something) => something,
  Err(e) => return Err(e),
}
```


# Task 2 (4 points)

```none
cargo test --features task_2
```

Implement support for all the brainfrak operators, without scoping (local
environment). You will need to create bindings in the runtime's environment,
resolve function calls and cell references, but you can omit pushing and
popping.

In other words, for this task you don't need to push / pop anything from the
environment stack. Using only one environment is enough, you don't have to
enforce the scoping rules. Focus on inserting bindings to and recovering
bindings from the [runtime][runtime].


# Task 3 (6 points)

```none
cargo test --features task_3
```

Support the full semantics of brainfrak: function declarations, calls, cell
naming and jumping, and the scope rules.

## About scoping

The only kind of scoping you can implement is *dynamic scoping* (as opposed to
*static scoping*). That is, the bindings in scope depends on the runtime.
For example, when interpreting the brainfrak program

```none
{"weird print"
  ^"some cell"   | This jump does not seem to make sense
  .
}

>:"some cell"    | Naming the second cell
<,[->++<]
!"weird print"
```
when you reach the call to `weird print` your local environment contains the
binding for `some cell`. You push a new local environment on the stack and
start interpreting the body of `weird print`. The binding for `some cell` is
**visible**, and the jump in `weird print` works.

With *static scoping*, the jump in `weird print` would be illegal. That's
because with *static scoping*, a command can only use definitions that appear
in the current block or the surrounding ones **before (syntactically)** this
command. See
[here](https://msujaws.wordpress.com/2011/05/03/static-vs-dynamic-scoping/)
for another example of dynamic scoping versus static scoping.


# Extra credit 1 (2 points)

It is very difficult to implement static scoping (discussed above) using the
[`Runtime`][runtime] structure I provide.

Discuss in the documentation of the [`run`][run] function a modification to
[`Runtime`][runtime] that would make static scoping easy to implement in the
interpreter. Providing small pieces of code, such as the signature of a
function you're discussing, would be a good idea.


# Run function

The run function I provide contains a partial implementation that uses two
vectors to process the commands and handle blocks. Feel free to choose a
different approach, but do read my explanations to be aware of the problems
you will encounter.

The first vector `cmds_now` is easy to understand: it contains the next
commands to process in the current block.

The second vector `cmds_next` stores the commands to process in the blocks
surrounding the current one. Its type is `Vec< Vec<Cmd> >`: it's a stack of
commands to process. That is, when interpreting the following program

```none
{"plus minus" >+<-}
{"do stuff" . !"plus minus" .>.<}
+++++>,<
!"do stuff"
>>++>.
```
when you reach the `+` in the call to `plus minus` inside the call to
`do stuff`, `cmds_now` should contain `<` and `-`, the next commands of the
block. You also need to remember the next commands in the surrounding blocks so
that you know what to do next when you exit the current block (the call to
`plus minus`). So here, the stack `cmds_next` should encode the fact that

- after the current commands, we must run `.>.<` (rest of the call to
    `do stuff`),
- after that, we must run `>>++>.` (rest of the program after the call to
    `do stuff`.

(I do not have loops in this example on purpose. You will have to be a bit
creative, but you can implement support for loops without modifying this design
pattern.)

Note that I provide the code handling exiting a block:

```no_run
// Done with current commands, checking the next ones.
None => if let Some(next) = cmds_next.pop() {
  // Updating current commands.
  cmds_now = next ;
  // Popping environment.
  let _ = runtime.pop(conf) ;
  // Loopin on new current commands.
  ()
} else {
  // No next command, reached the end of the program.
  break 'run
}
```

It runs when `cmds_now` is empty, and tries to pop a *layer* from the stack. If
there's one, this layer becomes the new `cmds_now`. If there's none, we have
reached the end of the program and we exit.

The stack `cmds_next` basically stores a [continuation](continuation), an
extremely useful design pattern.



[runtime]: ../struct.Runtime.html (Runtime structure documentation)
[run]: fn.run.html (run function documentation)
[cmd]: ../enum.Cmd.html (Cmd enum documentation)
[continuation]: https://en.wikipedia.org/wiki/Continuation (Continuation wiki)

*/

use time::PreciseTime ;

use clap::Conf ;
use ast::Prog ;

/** Runs a brainfrak program by interpreting its commands.

If there is no runtime error, returns

- the value stored in the current cell when the program terminates,
- the total number of operations performed, and
- the time in milliseconds.

# Submission

- Adrien Champion
- with *no one*

# Extra credit 1

Blah.

# Comments

If you have comments about this homework you can write them here.

*/
pub fn run(prog: Prog, conf: & mut Conf) -> Result<
  (usize, usize, usize), String
> {
  use runtime::Runtime ;
  use ast::Cmd::* ;
  use std::thread::sleep ;

  let duration = conf.step_time() ;

  let mut runtime = Runtime::mk() ;

  let mut cmds = prog.cmds ;
  cmds.reverse() ;

  let (
    // Commands we're executing currently.
    mut cmds_now,
    // Commands to execute once we're done with `cmds_now`.
    mut cmds_next
  ) = (
    cmds, vec![]
  ) ;

  let start = PreciseTime::now() ;
  let mut query_time = ::time::Duration::seconds(0) ;
  let mut count = 0 ;

  'run: loop {

    cmds_now.last().map(
      |cmd| conf.cmd_log(
        & format!("{}", conf.bold().paint(format!("{}", cmd)))
      )
    ) ;

    match cmds_now.pop() {
      // Moving right or left.
      Some(Rgt) => unimplemented!(),
      Some(Lft) => unimplemented!(),

      // Inc(dec)reasing current cell (wrapped).
      Some(Inc) => unimplemented!(),
      Some(Dec) => unimplemented!(),

      // Print(read)ing values.
      Some(Print) => unimplemented!(),
      Some(Read) => {
        // Measure the time spent waiting.
        let start = PreciseTime::now() ;
        // Checking if we have some CLAP inputs left.
        try!( runtime.read_usize(conf) ) ;
        let time = start.to( PreciseTime::now() ) ;
        query_time = query_time + time
      },

      // Looping.
      Some( Loop(mut body) ) => unimplemented!(),

      // Function declaration.
      Some( Fun(name, body) ) => unimplemented!(),

      // Function call.
      Some( Call(name) ) => unimplemented!(),

      // Name a cell.
      Some( Name(name) ) => unimplemented!(),

      // Jump to a cell.
      Some( Jump(name) ) => unimplemented!(),

      // Done with current commands, checking the next ones.
      None => if let Some(next) = cmds_next.pop() {
        // Updating current commands.
        cmds_now = next ;
        // Popping environment.
        let _ = runtime.pop(conf) ;
        // Loopin on new current commands.
        ()
      } else {
        // No next command, reached the end of the program.
        break 'run
      }
    } ;

    count += 1 ;

    sleep(duration.clone())
  } ;

  let time = start.to( PreciseTime::now() ) - query_time ;

  Ok(
    (runtime.curr_cell(), count, time.num_milliseconds() as usize)
  )
}











// |===| TEST STUFF |===|


#[cfg(test)]
macro_rules! try_panic {
  ($e:expr, $( $blah:expr ),+) => (
    match $e {
      Ok(res) => res,
      Err(e) => {
        println!("Test failed:") ;
        for line in e.lines() {
          println!("> {}", line)
        } ;
        panic!($( $blah ),+)
      },
    }
  ) ;
}

#[cfg(test)]
fn fib(n: usize) -> usize {
  if n == 0 { 0 } else {
    let (mut prev, mut next) = (1, 1) ;
    let mut tmp ;
    for _ in 2..n {
      tmp = prev ;
      prev = next ;
      next = tmp + prev
    } ;
    next
  }
}

#[cfg(test)]
fn load(file: & str) -> Prog {
  println!("Testing \"{}\"", file) ;
  println!("") ;

  print!("Parsing file ... ") ;
  let prog = try_panic!(
    Prog::of_file(file),
    "could not load test file \"{}\"", file
  ) ;
  println!("done.") ;
  println!("") ;
  prog
}

#[cfg(test)]
macro_rules! should_fail {
  ($e:expr, $( $blah:expr ),+) => (
    match $e {
      Ok( (res, _, _) ) => {
        println!("Test failed:") ;
        println!("> expected runtime error") ;
        print!("> ") ;
        println!( $( $blah ),+ ) ;
        println!("but interpreter was successful and returned {}", res) ;
        panic!("test failed")
      },
      Err(_) => (),
    }
  ) ;
}

#[cfg(test)]
fn fib_test(file: & str) {
  use interpreter::run ;
  let prog = load(file) ;
  for n in 0..20 {
    let expected = fib(n) ;
    let mut conf = Conf::hack_mk( vec![ n ] ) ;
    match run(prog.clone(), & mut conf) {
      Ok( (res, _, _) ) => if res != expected {
        println!("error on input \"{}\":", n) ;
        println!("expected {} but got {}", expected, res) ;
        panic!("result mismatch")
      },
      Err(e) => {
        println!("error on input \"{}\":", n) ;
        println!("expected {} but got an error:", expected) ;
        for line in e.lines() {
          println!("> {}", line)
        } ;
        panic!("runtime error in interpreter")
      },
    }
  }
}

/// Task 1: testcases that should not return an error.
#[cfg(test)]
mod task_1_correct {
  use super::fib_test ;

  #[test]
  fn fibonacci() {
    fib_test("rsc/task1/fibonacci.bf")
  }
}



/// Task 2: testcases that should not return an error.
#[cfg(
  all( test, not( feature="task_1" ) )
)]
mod task_2_correct {
  use super::fib_test ;

  #[test]
  fn fibonacci_funs() {
    fib_test("rsc/task2/fibonacci_funs.bf")
  }

  #[test]
  fn fibonacci_refs() {
    fib_test("rsc/task2/fibonacci_refs.bf")
  }

}



/// Task 2: testcases that should return an error.
#[cfg(
  all( test, not( feature="task_1" ) )
)]
mod task_2_incorrect {
  use interpreter::run ;
  use super::load ;
  use clap::Conf ;

  #[test]
  fn fun_undef() {
    let prog = load("rsc/task2/fun_undef.bf") ;

    should_fail!(
      run(prog, & mut Conf::empty()),
      "because test case contains call(s) to undefined function(s)"
    )
  }

  #[test]
  fn jump_undef() {
    let prog = load("rsc/task2/jump_undef.bf") ;

    should_fail!(
      run(prog, & mut Conf::empty()),
      "because test case contains jump(s) to undefined cell ref(s)"
    )
  }
}



/// Task 3: testcases that should not return an error.
#[cfg(
  all( test, not( feature="task_1" ), not( feature="task_2" ) )
)]
mod task_3_correct {
  use super::fib_test ;

  #[test]
  fn fibonacci() {
    fib_test("rsc/task3/fibonacci.bf")
  }

}



/// Task 3: testcases that should return an error.
#[cfg(
  all( test, not( feature="task_1" ), not( feature="task_2" ) )
)]
mod task_3_incorrect {
  use interpreter::run ;
  use super::load ;
  use clap::Conf ;

  #[test]
  fn fun_scope_1() {
    let prog = load("rsc/task3/fun_scope_1.bf") ;

    should_fail!(
      run(prog, & mut Conf::empty()),
      "because test case contains a jump to a cell ref that's not in scope"
    )
  }

  #[test]
  fn fun_scope_2() {
    let prog = load("rsc/task3/fun_scope_2.bf") ;

    should_fail!(
      run(prog, & mut Conf::empty()),
      "because test case contains a call to a function that's not in scope"
    )
  }

  #[test]
  fn loop_scope_1() {
    let prog = load("rsc/task3/loop_scope_1.bf") ;

    should_fail!(
      run(prog, & mut Conf::empty()),
      "because test case contains a jump to a cell ref that's not in scope"
    )
  }

  #[test]
  fn loop_scope_2() {
    let prog = load("rsc/task3/loop_scope_2.bf") ;

    should_fail!(
      run(prog, & mut Conf::empty()),
      "because test case contains a call to a function that's not in scope"
    )
  }
}


