//! Brainfrak parser.

use std::str::from_utf8 ;

use nom::{ IResult, eof } ;

use ast::Cmd ;
use ast::Cmd::* ;

/// A comment is anything but the 8 commands.
named!(comment<()>,
  map!(
    many0!(none_of!("\"^:!{}<>+-,.[]")),
    |_| ()
  )
) ;

/// A name: anything between double quote.
named!(quoted_name<String>,
  chain!(
    char!('"') ~
    inner: is_not!("\"") ~
    char!('"'),
    || from_utf8(inner).unwrap().to_string()
  )
) ;

/// A leaf is anything but a loop or a function declaration.
named!(leaf<Cmd>,
  alt!(
    chain!(
      char!(':') ~
      comment ~
      name: quoted_name,
      || Name(name)
    ) |
    chain!(
      char!('^') ~
      comment ~
      name: quoted_name,
      || Jump(name)
    ) |
    chain!(
      char!('!') ~
      comment ~
      name: quoted_name,
      || Call(name)
    ) |
    map!( char!('>'), |_| Rgt )   |
    map!( char!('<'), |_| Lft )   |
    map!( char!('+'), |_| Inc )   |
    map!( char!('-'), |_| Dec )   |
    map!( char!('.'), |_| Print ) |
    map!( char!(','), |_| Read )
  )
) ;

/// A command is a leaf or a loop with a program inside, or a function
/// declaration.
named!(cmd<Cmd>,
  alt!(
    leaf |
    delimited!(
      char!('['),
      map!(prog, |prog| Loop( prog )),
      char!(']')
    ) |
    delimited!(
      char!('{'),
      chain!(
        name: delimited!(comment, quoted_name, comment) ~
        prog: prog,
        || Fun( name, prog )
      ),
      char!('}')
    )
  )
) ;

/// A program is a list of commands.
named!(prog< Vec<Cmd> >,
  delimited!(
    comment,
    many0!(
      chain!(
        command: cmd ~ comment,
        || command
      )
    ),
    comment
  )
) ;

/// Top level, parses until EOF.
named!(top_prog< Vec<Cmd> >, terminated!(prog, eof)) ;

/// Turns a `nom::IResult` into a `Result`.
fn to_result<O>(res: IResult<& [u8], O>) -> Result<O, String> {
  use std::str::from_utf8 ;
  use nom::IResult::* ;
  use nom::Needed::* ;
  use nom::Err::* ;
  match res {
    Done(_, prog) => Ok(prog),
    Error(Position(err, bytes)) => Err(
      format!("parse error:\n> {:?}\n> {}", err, from_utf8(bytes).unwrap())
    ),
    Error(err) => Err(
      format!("parse error: {:?}", err)
    ),
    Incomplete(needed) => Err(
      format!(
        "incomplete input: missing {} bytes",
        match needed {
          Unknown => "?".to_string(),
          Size(n) => format!("{}", n),
        }
      )
    ),
  }
}

/// Parses a program from a file.
pub fn of_file(file_path: & str) -> Result<Vec<Cmd>, String> {
  use std::fs::OpenOptions ;
  use std::io::Read ;
  let mut buffer = Vec::with_capacity(700) ;
  match OpenOptions::new().read(true).open(file_path) {
    Ok(mut file) => match file.read_to_end(& mut buffer) {
      Ok(_) => to_result( top_prog(& buffer) ),
      Err(e) => Err(
        format!("could not read file \"{}\":\n{}", file_path, e)
      ),
    },
    Err(e) => Err(
      format!("could not open file \"{}\":\n{}", file_path, e)
    ),
  }
}

/// Parses a program from a string.
pub fn of_str(s: & str) -> Result<Vec<Cmd>, String> {
  to_result( top_prog( s.as_bytes() ) )
}