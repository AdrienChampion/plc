/*! Command line argument parsing.

A call to this brainfrak interpreter has form

```none
<binary_name> [options]* <file> [int]*
```

that is a (potentially empty) list of options, the file containing the
program you want to run, and a (potentially empty) list of integers.

The **trailing list of integers** is used to pass values to the interpreter.
When the programs want to read a value, the first integer in the list will
be consumed, then the second *etc.*

When there are no more integers in the list and the programs wants to read a
value, then the user will be asked to provide a value.

An **option** is one of the following:

| Option | Description |
|:---:|:---|
| `-h`  | prints usage and option information |
| `-cmd` | output each command processed |
| `-rtm` | output information about changes in the runtime (environment) |
| `--step <int>` | the interpreter will wait for `<int>` milliseconds between the processing of each command |

*/

use std::time::Duration ;

use ansi::{ Style, Colour } ;

/// Default value for `cmd_verb` flag.
const cmd_verb_default: bool = false ;
/// Default value for `rtm_verb` flag.
const rtm_verb_default: bool = false ;
/// Default value for step.
const step_default: usize = 0 ;

/// Bold style.
#[cfg(any(target_os="linux",target_os="macos"))]
#[inline(always)]
fn bold() -> Style { Style::new().bold() }
#[cfg(target_os="windows")]
fn bold() -> Style { Style::new() }
/// Red style.
#[cfg(any(target_os="linux",target_os="macos"))]
#[inline(always)]
fn red() -> Style { Colour::Red.normal() }
#[cfg(target_os="windows")]
fn red() -> Style { Style::new() }
/// Green style.
#[cfg(any(target_os="linux",target_os="macos"))]
#[inline(always)]
fn green() -> Style { Colour::Green.normal() }
#[cfg(target_os="windows")]
fn green() -> Style { Style::new() }
/// Cyan style.
#[cfg(any(target_os="linux",target_os="macos"))]
#[inline(always)]
fn cyan() -> Style { Colour::Cyan.normal() }
#[cfg(target_os="windows")]
fn cyan() -> Style { Style::new() }
/// Yellow style.
#[cfg(any(target_os="linux",target_os="macos"))]
#[inline(always)]
fn yellow() -> Style { Colour::Yellow.normal() }
#[cfg(target_os="windows")]
fn yellow() -> Style { Style::new() }

/// Stores the result of CLAP.
pub struct Conf {
  /// The file to interpret.
  file: String,
  /// Command verbosity.
  cmd_verb: bool,
  /// Runtime verbosity.
  rtm_verb: bool,
  /// Time between steps in ms.
  step: usize,
  /// Inputs given as CLA.
  inputs: Vec<usize>,
  /// Bold style.
  bold: Style,
  /// Red style.
  red: Style,
  /// Green style.
  green: Style,
}
impl Conf {
  /// CLAP.
  pub fn mk() -> Result<Self, String> {
    use std::env::args ;
    let mut args = args() ;
    // First argument is the name of the program.
    let _ = args.next() ;

    let mut cmd_verb = cmd_verb_default ;
    let mut rtm_verb = rtm_verb_default ;
    let mut step = step_default ;
    let file ;

    // CLAP loop.
    'clap: loop {
      if let Some(arg) = args.next() {
        match & arg as & str {
          "-h" => {
            Conf::help() ;
            ::std::process::exit(0)
          },
          "-cmd" => cmd_verb = true,
          "-rtm" => rtm_verb = true,
          "--step" => match args.next() {
            Some(arg) => match usize::from_str_radix(& arg, 10) {
              Ok(int) => step = int,
              Err(e) => return Err(
                format!(
                  "expected an integer, got \"{}\":\n{}", arg, e
                )
              ),
            },
            None => return Err(
              format!("expected an integer after \"--step\", got nothing")
            ),
          },
          _ => if let Some('-') = arg.chars().next() {
            return Err(
              format!("unknown option \"{}\"", arg)
            )
          } else {
            file = arg.to_string() ;
            break 'clap
          },
        }
      } else {
        return Err(
          format!("expected file path, got nothing")
        )
      }
    } ;

    // Parsing input values.
    let mut inputs = vec![] ;
    'inputs: loop {
      if let Some(input) = args.next() {
        match usize::from_str_radix(& input, 10) {
          Ok(input) => inputs.push(input),
          Err(e) => return Err(
            format!(
              "expected usize after file name, got \"{}\":\n{}", input, e
            )
          )
        }
      } else { break }
    } ;

    // Reverse input list for pop semantics.
    inputs.reverse() ;

    Ok(
      Conf {
        file: file,
        cmd_verb: cmd_verb,
        rtm_verb: rtm_verb,
        step: step,
        inputs: inputs,
        bold: bold(),
        red: red(),
        green: green(),
      }
    )
  }

  /// Creates a configuration manually. Used in tests.
  #[cfg(test)]
  pub fn hack_mk(inputs: Vec<usize>) -> Self {
    Conf {
      file: String::new(),
      cmd_verb: true,
      rtm_verb: true,
      step: 0,
      inputs: inputs,
      bold: Style::new(),
      red: Style::new(),
      green: Style::new(),
    }
  }

  /// Creates an empty configuration. Used in tests.
  #[cfg(test)]
  pub fn empty() -> Self {
    Conf {
      file: String::new(),
      cmd_verb: true,
      rtm_verb: true,
      step: 0,
      inputs: vec![],
      bold: Style::new(),
      red: Style::new(),
      green: Style::new(),
    }
  }

  /// Time to wait between each step in ms.
  pub fn step_time(& self) -> Duration {
    Duration::from_millis(self.step as u64)
  }

  /// File stored in the configuration.
  pub fn file(& self) -> & str {
    & self.file
  }

  /// Next usize input given as CLA.
  pub fn next_input(& mut self) -> Option<usize> {
    self.inputs.pop()
  }

  /// Prints a usage message.
  pub fn help() {
    let stl = bold() ;
    println!(
      "
{}: `brainfrak [options]* <file> [integer]*`
Interprets the brainfuck program in `<file>`.
The integers after the file path are fed as inputs to the program.

{}:
  {}           prints this message
  {}         activates command verbosity
  {}         activates runtime verbosity
  {} specifies the time (in ms) to wait between each cycle\
      ",
      stl.paint("Usage"),
      stl.paint("Options"),
      stl.paint("-h"),
      stl.paint("-cmd"),
      stl.paint("-rtm"),
      stl.paint("--step <int>"),
    )
  }

  /// Outputs usage, an error and exits with return code `2`.
  pub fn error(blah: String) -> ! {
    use std::process::exit ;
    let red = red() ;
    let pref = red.paint("| ") ;
    println!("{}", red.paint("|===| Error:")) ;
    for line in blah.lines() {
      println!("{}{}", pref, line)
    } ;
    println!("{}", red.paint("|===|")) ;
    println!("") ;
    exit(2)
  }

  /// Prints something if the `cmd_verb` flag is true.
  #[inline(always)]
  pub fn cmd_log(& self, blah: & str) {
    if self.cmd_verb {
      let mut pref = format!("{}",
        cyan().paint(
          format!("{:<7}", "[cmd]")
        )
      ) ;
      for line in blah.lines() {
        println!("| {} {}", pref, line) ;
        pref = format!("{:<7}", "")
      }
    }
  }

  /// Prints something if the `rtm_verb` flag is true.
  #[inline(always)]
  pub fn rtm_log(& self, blah: & str) {
    if self.rtm_verb {
      let mut pref = format!("{}",
        yellow().paint(
          format!("{:<7}", "[rtm]")
        )
      ) ;
      for line in blah.lines() {
        println!("| {} {}", pref, line) ;
        pref = format!("{:<7}", "")
      }
    }
  }

  /// Prints something.
  #[inline(always)]
  pub fn log(& self, blah: & str) {
    let should_sep = self.cmd_verb || self.rtm_verb ;
    if should_sep { println!("|") } ;
    for line in blah.lines() {
      println!("| {}", line)
    } ;
    if should_sep { println!("|") }
  }

  /// Prints something without a new line.
  #[inline(always)]
  pub fn log_no_nl(& self, blah: & str) {
    let should_sep = self.cmd_verb || self.rtm_verb ;
    if should_sep { println!("|") } ;
    print!("| {}", blah)
  }

  /// Bold style.
  #[inline(always)]
  pub fn bold(& self) -> & Style { & self.bold }
  /// Red style.
  #[inline(always)]
  pub fn red(& self) -> & Style { & self.red }
  /// Green style.
  #[inline(always)]
  pub fn green(& self) -> & Style { & self.green }
}