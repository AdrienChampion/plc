//! Module for the AST representing a brainfuck program.
//!
//! From [the brainfuck wiki page][bf wiki].
//!
//! [bf wiki]: https://en.wikipedia.org/wiki/Brainfuck#Commands (Brainfuck wikipedia page)

use std::fmt ;

use parser ;

use self::Cmd::* ;

/// Convenience macro to build a loop.
macro_rules! l00p {
  ($( $e:expr ),+) => (
    Loop(
      vec![ $( $e ),+ ]
    )
  ) ;
  ($( $e:expr ),+ ,) => ( loop!($($e),+) ) ;
}

/// Brainfrak commands.
#[derive(Debug, Clone)]
pub enum Cmd {
  /// `>`: increment the data pointer (to point to the next cell to the right).
  Rgt,
  /// `<`: decrement the data pointer (to point to the next cell to the left).
  Lft,
  /// `+`: increment (increase by one) the byte at the data pointer.
  Inc,
  /// `-`: decrement (decrease by one) the byte at the data pointer.
  Dec,
  /// `.`: output the byte at the data pointer.
  Print,
  /// `,`: accept one byte of input, storing its value in the byte at the data
  /// pointer.
  Read,
  /// `[ ... ]`: loop construction.
  ///
  /// - `[`: if the byte at the data pointer is zero, then instead of moving
  ///     the instruction pointer forward to the next command, jump it forward
  ///     to the command after the matching `]` command.
  ///
  /// - `]`: if the byte at the data pointer is nonzero, then instead of moving
  ///     the instruction pointer forward to the next command, jump it back to
  ///     the command after the matching `[` command.
  Loop(Vec<Cmd>),
  /// `{"name" ... }` named function. **Not in vanilla brainfuck.**
  Fun(String, Vec<Cmd>),
  /// `!"name"`: calls a block. **Not in vanilla brainfuck.**
  Call(String),
  /// `:"name"`: names a location. **Not in vanilla brainfuck.**
  Name(String),
  /// `^"name"`: jump to a named location. **Not in vanilla brainfuck.**
  Jump(String),
}
impl fmt::Display for Cmd {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    match * self {
      Loop(ref body) => {
        try!( write!(fmt, "[") ) ;
        write!(fmt, "_({} commands)_]", body.len())
      },
      Fun(ref name, ref body) => write!(
        fmt, "{{\"{}\" ({})}}", name, body.len()
      ),
      Call(ref name) => write!(
        fmt, "!\"{}\"", name
      ),
      Name(ref name) => write!(
        fmt, ":\"{}\"", name
      ),
      Jump(ref name) => write!(
        fmt, "^\"{}\"", name
      ),
      _ => write!(
        fmt, "{}", match * self {
          Rgt => '>', Lft => '<',
          Inc => '+', Dec => '-',
          Print => '.', Read => ',',
          _ => unimplemented!(),
        }
      ),
    }
  }
}

/// A program is a list of commands.
#[derive(Clone)]
pub struct Prog {
  pub cmds: Vec<Cmd>,
  pub source: String,
}
impl Prog {
  /// Creates a new program.
  pub fn mk(cmds: Vec<Cmd>, source: String) -> Self {
    Prog { cmds: cmds, source: source }
  }

  /// Creates a new program from a file.
  pub fn of_file(file: & str) -> Result<Self, String> {
    parser::of_file(file).map(
      |cmds| Prog { cmds: cmds, source: file.to_string() }
    )
  }

  /// Creates a new program from a string.
  pub fn of_str(input: & str) -> Result<Self, String> {
    parser::of_str(input).map(
      |cmds| Prog { cmds: cmds, source: "<from string>".to_string() }
    )
  }

  /// String representation of a program (lines).
  pub fn to_str(& self) -> String { self.to_str_pref("") }

  /// String representation of a program with line prefix (lines).
  pub fn to_str_pref(& self, pref: & str) -> String {
    let mut pref = pref.to_string() ;
    let mut res = pref.clone() ;
    let mut todo = vec![
      ( self.cmds.iter(), "".to_string() )
    ] ;

    'outer: loop {
      if let Some( (mut cmds, closing) ) = todo.pop() {
        'inner: loop {
          match cmds.next() {
            Some( & Loop(ref kids) ) => {
              res.push_str( & format!("\n{}[", pref) ) ;
              todo.push( (cmds, closing) ) ;
              todo.push( (kids.iter(), "]".to_string()) ) ;
              pref = format!("{}  ", pref) ;
              res.push_str( & format!("\n{}", pref) ) ;
              continue 'outer
            },
            Some( & Fun(ref name, ref kids) ) => {
              res.push_str( & format!("\n{}{{\"{}\"", pref, name) ) ;
              todo.push( (cmds, closing) ) ;
              todo.push( (kids.iter(), "}".to_string()) ) ;
              pref = format!("{}  ", pref) ;
              res.push_str( & format!("\n{}", pref) ) ;
              continue 'outer
            },
            Some(leaf) => {
              match * leaf {
                Rgt => res.push('>'), Lft => res.push('<'),
                Inc => res.push('+'), Dec => res.push('-'),
                Print => res.push('.'), Read => res.push(','),
                Name(ref name) => res.push_str(
                  & format!("\n{}:\"{}\"\n{}", pref, name, pref)
                ),
                Jump(ref name) => res.push_str(
                  & format!("\n{}^\"{}\"\n{}", pref, name, pref)
                ),
                Call(ref name) => res.push_str(
                  & format!("\n{}!\"{}\"\n{}", pref, name, pref)
                ),
                Fun(_,_) | Loop(_) => unreachable!(),
              }
            },
            None => break 'inner,
          }
        } ;
        if ! todo.is_empty() {
          let _ = pref.pop() ;
          let _ = pref.pop() ;
          res.push_str( & format!("\n{}{}\n{}", pref, closing, pref) )
        }
      } else {
        break 'outer
      }
    } ;

    res
  }
}