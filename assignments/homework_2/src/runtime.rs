//! Brainfrak runtime.

use std::collections::HashMap ;
use std::ops::Index ;

use ansi::Colour::Red ;

use clap::Conf ;
use ast::Prog ;

/// A hash map from `String` to something.
pub type Map<T> = HashMap<String, T> ;

/// A stack is just a vector.
pub type Stack<T> = Vec<T> ;

/** Stores runtime info.

Runtime information consists of

- the memory
- a stack of environments
- a buffer for reading inputs

# Environment management

Internally, the runtime maintains a stack of environments. An element of the
stack is actually a pair of `HashMap`s:

- a cell reference map, from `String` to `usize`
  (index of the cell referenced)
- a function map, from `String` to [`Prog`](struct.Prog.html)
  (body of the function)

To interact with the stack of environments, see functions

- [`push`](#method.push)
- [`pop`](#method.pop)
- [`insert_ref`](#method.insert_ref)
- [`get_ref`](#method.get_ref)
- [`insert_fun`](#method.insert_fun)
- [`get_fun`](#method.get_fun)

*/
pub struct Runtime {
  /// Memory cells on the left.
  mem_lft: Vec<usize>,
  /// Memory cells on the right (reversed for usual `push` semantics).
  mem_rgt: Vec<usize>,
  /// Reference and function map stack.
  stack: Stack< (Map<usize>, Map<Prog>) >,
  /// Buffer for reading input.
  buff: String,
}
impl Runtime {
  /// Creates a new runtime. Stack will contain one (empty) environment.
  ///
  /// Initial capacities:
  ///
  /// - memory: `769`
  /// - stack: `23`
  /// - buffer: `3`
  pub fn mk() -> Self {
    let mem_lft = Vec::with_capacity(769) ;
    let mut mem_rgt = mem_lft.clone() ;
    mem_rgt.push(0) ;
    let mut stack = Stack::with_capacity(23) ;
    stack.push( (Map::new(), Map::new()) ) ;
    Runtime {
      mem_lft: mem_lft, mem_rgt: mem_rgt, stack: stack,
      buff: String::with_capacity(3)
    }
  }

  /// Current size of the whole memory.
  pub fn len(& self) -> usize {
    self.mem_lft.len() + self.mem_rgt.len()
  }

  /// Length of the reference and function stack.
  ///
  /// Corresponds to the current number of nested blocks (including the
  /// implicit *top block*).
  pub fn hgt(& self) -> usize {
    self.stack.len()
  }

  /// Current position in memory.
  pub fn pos(& self) -> usize {
    self.mem_lft.len()
  }

  /// Goes left in the memory.
  ///
  /// Returns an error if `self.pos()` is `0`.
  pub fn lft(& mut self) -> Result<(), String> {
    if let Some(lft_cell) = self.mem_lft.pop() {
      self.mem_rgt.push( lft_cell ) ;
      Ok(())
    } else {
      Err(
        "cannot go left of first cell in memory".to_string()
      )
    }
  }

  /// Goes right in the memory.
  ///
  /// Should **never** return an error, unless there's a bug in memory vectors
  /// management.
  pub fn rgt(& mut self) -> Result<(), String> {
    if let Some(rgt_cell) = self.mem_rgt.pop() {
      self.mem_lft.push( rgt_cell ) ;
      if self.mem_rgt.is_empty() {
        // Cell we just popped was the last one, pushing a new one.
        self.mem_rgt.push(0)
      } ;
      Ok(())
    } else {
      Err(
        "memory is an illegal state: there is no cell pointed to".to_string()
      )
    }
  }

  /// Jumps to the cell corresponding to the specified index.
  pub fn jump(& mut self, index: usize) -> Result<(), String> {
    while self.pos() > index { try!( self.lft() ) } ;
    while self.pos() < index { try!( self.rgt() ) } ;
    Ok(())
  }

  /// Mutable reference to the value of the `n`th cell.
  fn cell_mut_get(& mut self, index: usize) -> & mut usize {
    debug_assert!( index < self.len() ) ;
    if index < self.pos() {
      & mut self.mem_lft[index]
    } else {
      let index = self.mem_rgt.len() - (index - self.pos() + 1) ;
      & mut self.mem_rgt[index]
    }
  }

  /// The value of the `n`th cell.
  pub fn cell_get(& self, index: usize) -> & usize {
    if index < self.pos() {
      & self.mem_lft[index]
    } else {
      & self.mem_rgt[
        self.mem_rgt.len() - (index - self.pos() + 1)
      ]
    }
  }

  /// Applies a function to the current memory cell.
  pub fn cell_do<F: Fn(usize) -> usize>(& mut self, fun: F) {
    debug_assert!( ! self.mem_rgt.is_empty() ) ;
    let curr = self.mem_rgt.len() - 1 ;
    self.mem_rgt[curr] = fun(self.mem_rgt[curr])
  }

  /// The value of the current cell.
  pub fn curr_cell(& self) -> usize {
    debug_assert!( ! self.mem_rgt.is_empty() ) ;
    self.mem_rgt[self.mem_rgt.len() - 1]
  }

  /// Pushes new maps on the reference and function stack.
  pub fn push(& mut self, conf: & Conf) {
    self.stack.push( (Map::new(), Map::new()) ) ;
    conf.rtm_log(
      & format!(
        "{}\n> {} layers in stack now",
        conf.green().paint("pushed new maps on env stack"),
        self.hgt()
      )
    )
  }

  /// Pops the last maps on the reference and function stack.
  ///
  /// Returns the reference and function environments that have just been
  /// popped.
  ///
  /// Can return an error if trying to pop the last (*top*) pair of maps.
  pub fn pop(
    & mut self, conf: & Conf
  ) -> Result<(Map<usize>, Map<Prog>), String> {
    if let Some(maps) = self.stack.pop() {
      if ! self.stack.is_empty() {
        conf.rtm_log(
          & format!(
            "{} ({} left in stack):\n  refs: [{}\n  ]\n  funs: [{}\n  ]",
            conf.green().paint("popped from env stack"),
            self.hgt(),
            maps.0.iter().fold(
              String::new(), |s, (name, val)| format!(
                "{}\n    {} -> {}",
                s, conf.bold().paint(format!("{}", name)), val
              )
            ),
            maps.1.iter().fold(
              String::new(), |s, (name, prog)| format!(
                "{}\n    {} -> prog of {} commands",
                s, conf.bold().paint(format!("{}", name)), prog.cmds.len()
              )
            )
          )
        ) ;
        Ok(maps)
      } else {
        Err(
          "attempting to pop the top maps from the stack".to_string()
        )
      }
    } else { unreachable!() }
  }

  /// Inserts a reference in the current (local) map.
  pub fn insert_ref(
    & mut self, name: String, cell: usize, conf: & Conf
  ) -> Option<usize> {
    conf.rtm_log(
      & format!(
        "{} ref binding in environment:\n  {} -> {}",
        conf.green().paint("inserting"),
        conf.bold().paint(name.clone()),
        cell
      )
    ) ;
    if let Some(& mut (ref mut refs, _)) = self.stack.last_mut() {
      refs.insert(name, cell)
    } else { unreachable!() }
  }

  /// Gets a reference from the map stack.
  pub fn get_ref(& self, name: & str) -> Option<usize> {
    let mut stack = self.stack.iter().map(|pair| & pair.0) ;
    'stack_loop: loop {
      if let Some(ref map) = stack.next() {
        if let Some(cell) = map.get(name) {
          return Some(* cell)
        }
      } else {
        return None
      }
    }
  }

  /// Inserts a function in the current (local) map.
  pub fn insert_fun(
    & mut self, name: String, body: Prog, conf: & Conf
  ) -> Option<Prog> {
    conf.rtm_log(
      & format!(
        "{} function binding in environment:\n  \
        {} -> function with {} commands",
        conf.green().paint("inserting"),
        conf.bold().paint(name.clone()),
        body.cmds.len()
      )
    ) ;
    if let Some(& mut (_, ref mut funs)) = self.stack.last_mut() {
      funs.insert(name, body)
    } else { unreachable!() }
  }

  /// Gets a function from the map stack.
  pub fn get_fun(& self, name: & str) -> Option<Prog> {
    let mut stack = self.stack.iter().map(|pair| & pair.1) ;
    'stack_loop: loop {
      if let Some(ref map) = stack.next() {
        if let Some(prog) = map.get(name) {
          return Some(prog.clone())
        }
      } else {
        return None
      }
    }
  }

  /// Reads a `usize` and writes it in the current cell.
  pub fn read_usize(& mut self, conf: & mut Conf) -> Result<(), String> {
    let pos = self.pos() ;
    self.read_usize_to(pos, conf)
  }


  /// Reads a `usize` and writes it in the cell specified.
  pub fn read_usize_to(
    & mut self, index: usize, conf: & mut Conf
  ) -> Result<(), String> {
    use std::io::{ stdin, stdout, Write } ;
    let (stdin, mut stdout) = (stdin(), stdout()) ;
    if let Some(val) = conf.next_input() {
      conf.log(
        & format!(
          "| retrieved value \"{}\" from CLA inputs",
          conf.bold().paint(format!("{}", val))
        )
      ) ;
      * self.cell_mut_get(index) = val ;
      return Ok(())
    } else {
      loop {
        conf.log_no_nl("> please enter an integer in [0,255] (usize): ") ;
        match stdout.flush() {
          Ok(()) => (), Err(e) => return Err(
            format!("could not flush stdout:\n{}", e)
          ),
        } ;
        self.buff.clear() ;
        match stdin.read_line(& mut self.buff) {
          Ok(_) => (),
          Err(e) => return Err(
            format!("could not read from standard input:\n{}", e)
          ),
        } ;
        let s = self.buff.trim().to_string() ;
        match usize::from_str_radix(& s, 10) {
          Ok(val) => {
            * self.cell_mut_get(index) = val ;
            return Ok(())
          },
          Err(e) => {
            conf.log(
              & format!(
                "{}   {}, could not parse \"{}\" as a usize:\n\
                {}   > {}",
                Red.paint("|"), Red.paint("error"), Red.paint(s),
                Red.paint("|"), e
              )
            )
          },
        }
      }
    }
  }
}
impl Index<usize> for Runtime {
  type Output = usize ;
  fn index(& self, index: usize) -> & usize {
    self.cell_get(index)
  }
}