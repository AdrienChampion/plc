/*! Interpreter for the brainfrak language, an augmentation of the brainfuck
language.

Usage (options, how to call the actual software) of this interpreter is
described in the [clap (Command Line Argument Parsing) module](clap/index.html).


# Homework

The documentation below discusses the brainfrak language itself. The actual
tasks for the assignment are given in [the documentation of the interpreter
module](interpreter/index.html).


# Vanilla brainfuck commands

See [the brainfuck wiki page, *Commands* section][bf wiki]. To get a better understanding of braifuck you can use
[this online interpreter](http://www.iamcal.com/misc/bf_debug/). It is rather
convenient to use and shows all the debug information you need.


# Brainfrak reserved characters

Brainfrak augments brainfuck with several commands. Some characters considered
comments in brainfuck are **not** comments in brainfrak. Namely `{`, `}`, `"`,
`!`, `:`, and `^`.

Any character that's not a brainfuck command and is not in the list above is a
brainfrak comment.


# Brainfrak additional commands

Brainfrak introduces the notion of *name*. A name is always given between two
by `"`, and can be any sequence of characters **except** `"`.

Names can be bound to functions and memory locations (cells). **Function and
cell bindings are stored in different maps**. This means that when giving the
same name to a function and a cell, one is not *shadowed* or *overwritten* by
the other.

**N.B.** brainfrak has the notion of *scope*. That is, all bindings are local
to the current scope. The scope rules are discussed in the
[Scope rules section](#scope-rules). The two following sections describe the
brainfrak-specific constraints without getting in the details of scoping.


## Functions

### Declaring a function

Functions are declared in blocks delimited by `{` and `}`. The block starts
with a name (see above for the definition of *name*). Functions in brainfrak
**do not take parameters**. They simply package some commands.

**N.B.** Function declarations can appear anywhere any other command can
appear. So, for example, declaring a function in a function declaration is
legal.

The following code declares a function called `copy sub`.

```none
{"copy sub"
  Let 'n' be the index of the current cell when the function is called
  [
    -  Decrement cell 'n'
    >+ Increment cell 'n plus 1'
    >- Decrement cell 'n plus 2'
    << Go back to cell 'n'
  ]
}
```

### Calling a function

The syntax for calling a function `some name` is `!"some name"`. Reusing the
previous example:

```none
{"copy sub"
  Let 'n' be the index of the current cell when the function is called
  [
    -  Decrement cell 'n'
    >+ Increment cell 'n plus 1'
    >- Decrement cell 'n plus 2'
    << Go back to cell 'n'
  ]
}

,           Read user input and write in cell 0
>>,         Read user input and write in cell 2
<<          Go back to cell 0
!"copy sub" Call `copy sub` function (on exit we're at cell 0)
>.          Print current cell 1
>.          Print cell 2
```

In a call, using name that's undefined --*i.e.* no function with that name has
been declared-- is a runtime error.


## Cell references

### Naming a cell

The syntax to bind a name `some name` to the current memory location is
`:"some name"`. For instance:

```none
 :"cell 0" Naming the first  cell 'cell 0'
>:"cell 1" ___________second cell 'cell 1'
>:"cell 2" ___________third  cell 'cell 2'
```

### Jumping to a cell

Jumping `some name` to the current memory location is
`:"some name"`. For instance:

```none
 :"cell 0" Naming the first  cell 'cell 0'
>:"cell 1" ___________second cell 'cell 1'
>:"cell 2" ___________third  cell 'cell 2'

^"cell 0"  Jumping to 'cell 0'
++
^"cell 2"  Jumping to 'cell 2'
```

In a jump, using name that's undefined --*i.e.* no cell has been given that
name-- is a runtime error.


## Scope rules

### A block defines a scope

Brainfrak only has two *block* commands, which both have a local environment:

- loops: `[ ___ ]`
- [function declarations](#declaring-a-function): `{"name" ___ }`

That is, *all bindings introduced in such blocks are local and cannot be seen
from outside the block*. In other words, a block defines a scope.

For instance:

```none
[                    Entering loop and thus a new scope
  :"starting cell"   Naming the cell we start from
  +++
  {"print" .}        Declaration in the local environment of the loop
  !"print"           This is fine
  >-!"print"         So is this
  >+!"print"         And this
  ^"starting cell"   Jumping back to 'starting cell', this is fine
]                    Exiting the loop and discarding everything in its scope

!"print"             Illegal as no function called 'print' is in scope here
^"starting cell"     Illegal as 'starting cell' is not bound to anything in this scope
```

### Forward references

Using a name before it is bound to something is illegal. In more technical
terms, forward-referencing is illegal.

```none
!"print"     Illegal as 'print' is undefined at call site
{"print" .}  Too late =)
```

### Nested blocks

Everything defined in the blocks surrounding the current block is *in scope*
(visible in the current block). That is, everything defined **previously**
(above), since forward-referencing is illegal.

```none
:"starting cell"
{"print" .}
[
  >>+++++
  ^"starting cell"
  !"print"             'print' is in scope
  {"start cell add 5"
    ^"starting cell"   'starting cell' is in scope
    {"add 5" +++++}
    !"add 5"
  }
  >-->++
  !"start cell add 5"
]
```


[bf wiki]: https://en.wikipedia.org/wiki/Brainfuck#Commands (Brainfuck wikipedia page)
*/
#![allow(non_upper_case_globals)]

#[macro_use]
extern crate nom ;
extern crate ansi_term as ansi ;
extern crate time ;

pub mod clap ;
#[macro_use]
mod ast ;
pub use ast::{ Cmd, Prog } ;
pub mod parser ;
mod runtime ;
pub use runtime::Runtime ;
pub mod interpreter ;

fn title(s: & str) {
  println!("|{:=^68}|", format!(" {} ", s))
}

/// Similar to try but if `Err`, prints the error and exits.
macro_rules! try_err {
  ($e:expr, help) => (
    match $e {
      Ok(res) => res,
      Err(e) => {
        clap::Conf::help() ;
        clap::Conf::error(e)
      },
    }
  ) ;
  ($e:expr , $pre_err:expr) => (
    match $e {
      Ok(res) => res,
      Err(e) => {
        $pre_err ;
        clap::Conf::error(e)
      },
    }
  ) ;
  ($e:expr) => ( try_err!($e, ()) ) ;
}

/// Entry point.
fn main() {

  // CLAP.
  let mut conf = try_err!( clap::Conf::mk(), help ) ;

  println!("") ;
  title("Parsing") ;
  println!("|") ;

  // Attempting to parse file.
  let prog = try_err!( Prog::of_file(conf.file()), help ) ;

  println!("| {}", conf.green().paint("success")) ;

  println!("|") ;
  title("Running") ;
  println!("|") ;

  let (_, ops, runtime) = try_err!(
    interpreter::run(prog, & mut conf)
  ) ;

  println!("|") ;
  title(
    & format!(
      "Done: {} operations in {}ms ({:.3} ops/ms)",
      ops, runtime, match runtime {
        0 => ops as f64,
        _ => (ops as f64) / (runtime as f64),
      }
    )
  ) ;
  println!("") ;
}
