|===| Checks if local environments are indeed popped
|
| * should raise a runtime error
|
|===|

{"add curr to right"
  :"current cell"
  [
    ->+
    ^"current cell"
  ]
}

:"cell 0"
+++++
>++
^"cell 0"
!"add curr to right"

| The following jump is illegal
| The cell reference was defined in the function definition
^"current cell"
.