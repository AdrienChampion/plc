|===| Checks if local environments are indeed popped
|
| * should raise a runtime error
|
|===|

{"add curr to right"
  [
    :"current cell"
    ->+
    ^"current cell"
  ]

  | The following jump is illegal
  | The cell reference was defined in the loop
  ^"current cell"
}

:"cell 0"
+++++
>++
^"cell 0"
!"add curr to right"
.