|===| Fibonacci 3
|
| * scoping IS needed
|
| * functions
| * nested functions
| * function calls
|
| * cell naming
| * nested naming
| * jumping
|
|===|

{"fibonacci"
  Fibonacci function

  Setting up pointers to first three cells
   :"cell 0"
  >:"cell 1"
  >:"cell 2"

  Going back to cell 0
  ^"cell 0"

  {"print"
    Simply prints something
    .
  }

  {"print cell 0"
    Self explanatory
    Confusing name for original position is on purpose =D
    It's to test scoping
    :"cell 1"  | Memorizing original position
    ^"cell 0"  | Jumping to cell 0
    !"print"   | Printing current cell (cell 0)
    ^"cell 1"  | Going back to original position
  }

  {"print cell 1"
    Self explanatory
    Confusing name for original position is on purpose =D
    It's to test scoping
    :"cell 2"  | Memorizing original position
    ^"cell 1"  | Jumping to cell 0
    !"print"   | Printing current cell (cell 1)
    ^"cell 2"  | Going back to original position
  }

  {"print cell 2"
    Self explanatory
    Confusing name for original position is on purpose =D
    It's to test scoping
    :"cell 0"  | Memorizing original position
    ^"cell 2"  | Jumping to cell 0
    !"print"   | Printing current cell (cell 2)
    ^"cell 0"  | Going back to original position
  }

  {"big loop"
    Fibonacci loop

    |===| More functions

    {"loop inc 2"
      Adds the current cell to the two next cells
      [->+>+<<]
    }
    {"loop inc 1"
      Adds the current cell to the previous cell
      [-<+>]
    }

    [
      ->
       !"loop inc 2"
      >!"loop inc 1"
      >!"loop inc 1"
      !"print cell 1" | Print cell 2
      ^"cell 0"       | and go back to cell 0
    ]
  }

  {"setup"
    ,
    ^"cell 2" +
    ^"cell 0"
  }

  !"setup"
  !"big loop"
  >
}

Entry point
!"fibonacci"