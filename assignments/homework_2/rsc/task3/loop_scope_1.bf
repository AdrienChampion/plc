|===| Checks if local environments are indeed popped
|
| * should raise a runtime error
|
|===|

{"add curr to right"
  :"current cell"
  [
    {"minus right plus"
      ->+ ^"current cell"
    }
    !"minus right plus"
  ]
  +

| The following call is not defined
| The function is not in scope as it was defined in the loop
  !"minus right plus"

}

:"cell 0"
+++++
>++
^"cell 0"
!"add curr to right"
>.