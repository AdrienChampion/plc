|===| Checks if local environments are indeed popped
|
| * should raise a runtime error
|
|===|

{"add curr to right"

  {"minus right plus"
    ->+
  }
  [
    :"current cell"
    !"minus right plus"
    ^"current cell"
  ]
}

:"cell 0"
+++++
>++
^"cell 0"
!"add curr to right"

| The following call is illegal
| The function is not in scope as it was defined in the function
| definition above
!"minus right plus"

.