|===| Jump to undefined cell ref
|
| * should raise a runtime error
|
|===|

{"print" .}

:"cell 0"
++++ !"print"

[ Multiply current cell by 2 and put result in next cell
  ->++ ^"cell 0"
]

^"cell 1"
!"print"