|===| Fibonacci
|
| * cell naming
| * jumping
|
| * no scoping needed
|
| * no function declaration
| * no function call
|
|===|

 :"cell 0"
>:"cell 1"
>:"cell 2"

^"cell 0" ,
^"cell 2" +
^"cell 0" [
  -
  >>> :"cell 3"
  ^"cell 1" [
    ->+>+
    ^"cell 1"
  ] ^"cell 2" [
    -<+>
  ] ^"cell 3" [
    -<+>
  ]
  ^"cell 1" .
  ^"cell 0"
]
^"cell 1"
