|===| Fibonacci
|
| * function declarations
| * function calls
|
| * no scoping needed
|
| * no cell naming
| * no jumping
|
|===|


{"print"
  Simply prints something
  .
}

{"setup"
  ,>>+<<
}

{"loop inc 2"
  Adds the current cell to the two next cells
  [->+>+<<]
}

{"loop inc 1"
  Adds the current cell to the previous cell
  >[-<+>]
}

!"setup" [
  ->
  !"loop inc 2"
  !"loop inc 1"
  !"loop inc 1"
  <<
  !"print"
  <
]

>