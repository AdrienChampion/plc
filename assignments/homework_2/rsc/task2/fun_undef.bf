|===| Call to undefined function
|
| * should raise a runtime error
|
|===|


{"print" .}

++++ !"print"

[ Multiply current cell by 2 and put result in next cell
  ->++<
]

!"print cell 1"