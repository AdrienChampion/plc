|===| Fibonacci 2
|
| * scoping IS needed
|
| * functions
| * nested functions
| * function calls
|
| * no cell naming
| * no jumping
|
|===|

{"fibonacci"
  Fibonacci function

  {"print"
    Simply prints something
    .
  }

  {"big loop"
    Fibonacci loop

    |===| More functions

    {"loop inc 2"
      Adds the current cell to the two next cells
      [->+>+<<]
    }
    {"loop inc 1"
      Adds the current cell to the previous cell
      [-<+>]
    }

    [->!"loop inc 2">!"loop inc 1">!"loop inc 1"<<!"print"<]
  }

  {"setup"
    ,>>+<<
  }

  !"setup"
  !"big loop"
  >
}

Entry point
!"fibonacci"