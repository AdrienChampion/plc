//! Skeleton crate for a 2048 AI.

extern crate lib_2048 ;

use lib_2048::Evolution ;
use lib_2048::rendering::{ Frame, rendering_loop } ;

use lib_2048::clap ;

/// Sleeps for some time (in ms).
fn sleep(ms: u64) {
  use std::thread::sleep ;
  use std::time::Duration ;
  sleep(Duration::from_millis(ms)) ;
}

/// Dumb AI that tries to go up, then left, then right, then down.
fn ai_move(frame: & mut Frame) -> Evolution {
  use std::process::exit ;
  sleep(20) ;
  match frame.up() {
    Evolution::Nothing => match frame.left() {
      Evolution::Nothing => match frame.right() {
        Evolution::Nothing => match frame.down() {
          Evolution::Nothing => {
            println!("I lost T_T") ;
            println!("") ;
            exit(0)
          },
          evol => evol,
        },
        evol => evol,
      },
      evol => evol,
    },
    evol => evol,
  }
}

fn main() {
  use std::process::exit ;

  // Getting seed and painter from command line arguments.
  let (seed, painter) = match clap::parse() {
    Ok( (seed, painter) ) => (seed, painter),
    Err( (e, painter) ) => {
      println!("{}\n> {}", painter.error("Error:"), e) ;
      exit(2)
    },
  } ;

  rendering_loop(seed, painter, ai_move)
}