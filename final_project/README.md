# Final project

The final project is *à la carte*. A few ideas follow, but you can do
**almost** whatever you want. That is, you can contact me by email to
discuss what you would like to do for the final project.

Halfway into the project (mid-April), you will have to submit a report
discussing the progress you made. Then, the last week of class, you will
present your work to me and the rest of the class. Each member of the group
will speak and discuss something interesting about the project: challenges,
achievements, demo, *etc.*

Let me make one thing clear. The trade-off is that I give you freedom, and you
give me actual work. That is, I want to see **at least a month of regular
work**. I strongly recommend you choose a project that motivates you so that
working on it and a pleasure and not a pain.

More guidelines and information follow, but the bottom line is that you should
choose something you want to invest time and energy into.

## Language

The propositions below are all in Rust. If you want to build your own project,
I strongly recommend you choose Rust. Not (only) because I want you to suffer,
but because I may be asked to argue of the relevance of your project for the
class.

If you use Rust, anything goes. Writing code in Rust implies dealing with
bounded parametric polymorphism, regional memory management (ownership /
borrowing), algebraic data types, *etc.* These are relatively advanced
programming language concepts, so whatever you do I can defend it (and
actually already have).

If you decide to use another language, then when you contact me you will have
to explain what this project has to do with programming language concepts.


## Grading

Given the freedom, I cannot give you a grading scheme. Depending on their
difficulty, some projects will have extra credit just for choosing them.

You are not necessarily expected to actually *finish the project*. When you
contact me to let me know what you choose, we will set up objectives so that
you can organize yourselves and have an idea of how we will grade them.

In the end, trying to tackle a difficult project but failing to complete all of
the objectives might be more rewarding than choosing an easy project that you
(almost) finish.

Of course, choosing a difficult project just to get extra credit but not really
working on it will not be worth it.


## Project ideas (Rust)


### 2048 (GUI)

Design a Graphical User Interface (GUI) for [the game 2048][2048]. I will
provide the code for the game itself (see [here](https://bitbucket.org/AdrienChampion/rust_2048)),
on top of which you will implement a GUI
in the spirit of [the 2048 piston-based GUI][2048 piston].

* rendering, 2D
* user interaction
* relatively easy

You will use my version of 2048 available here:
> [https://bitbucket.org/AdrienChampion/rust_2048](https://bitbucket.org/AdrienChampion/rust_2048)


### 2048 (AI)

Write an Artificial Intelligence (AI) that can play [the game 2048][2048]. I
will provide the code for the game itself. Your code will basically get a grid,
decide on a move, and get the next grid.

* known to be non-trivial, because of the randomness
* best approaches (as far as I know) use machine-learning
* using SMT solvers instead would be interesting (difficult, extra credits)

Skeleton project for the game, featuring a dumb AI to get you started, can be
found in the [2048_ai](https://bitbucket.org/AdrienChampion/plc/src/949e852340071f64b92ab3d32f00d781a9efc9f8/final_project/2048/?at=master) folder.


### Game of Life (GUI)

Design a Graphical User Interface (GUI) for the Game of Life. You will build on
the code from homework 1 to implement a GUI rendering the grid and allowing
some amount of user interaction, to be defined.

* rendering, 2D or 3D (more difficult, extra credit)
* user interaction
* not that difficult


### Brainfrak 3.0

Enrich the brainfrak language from homework 2 with more advanced commands.
Typically, functions with parameters, structures, enums, recursive functions,
*etc.*

* parsing
* interpreter or compiler (more difficult, extra credit)
* not that difficult if you stick to interpretation


### Texdown

Design a markdown-like language for presentations (slides). Design an AST and
write a parser for the language. Designing the language is actually relatively
easy. This could turn out to be **extremely useful**. Once you have designed
the AST, it can be turned into HTML, LaTeX, or something else. As far as I
know no satisfactory language / tool exist for this use case.

It would be interesting to have two groups coordinate on the design of the AST.
One would be in charge of going from the markdown-like language to the AST, and
the other from the AST to HTML or something else. If the result is mature,
enough I will write the AST-to-LaTeX conversion and we can start making money
:)

* language / AST design
* parsing
* could have a big impact
* non-trivial (extra credit)


### ZDDs to BDDs

Rust has a [*Zero-suppressed binary Decision Diagram* official library](zdd)
(ZDD). A ZDD is a very interesting structure with amazing properties. It is far
from being trivial though. It is an evolution of Binary Decision Diagrams
(BDDs) for which Rust has no official library.

ZDDs and BDDs are extremely close. Modify the official ZDD library to turn it
into a BDD library. I can help you quite a lot for this project since I'm the
author of the ZDD library.

* borderline research
* very little code to write, but complex
  (non-negligible amount of thinking required)
* advanced data structures
* advanced design patterns ([hashconsing][hash])
* could end up being an official Rust library
* lots of extra credit



[2048]: http://2048game.com (2048 video game)
[2048 piston]: https://github.com/Coeuvre/rust-2048 (2048 piston implementation)
[zdd]: https://crates.io/crates/zdd (ZDD Rust library)
[hash]: https://crates.io/crates/hashconsing (Hashconsing Rust library)