\documentclass[10pt]{beamer}
\usetheme{Warsaw}

% |===| Package imports.

\usepackage{
  etex, graphicx, amssymb, amsmath, amstext, amsfonts, mathtools,
  multicol, multirow, pgfplots, array, listings, colortbl, ulem, ifthen,
  xcolor, mathrsfs, xspace, rotating
}
\usepackage[scaled=1]{beramono}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{zlmtt}

\usetikzlibrary{shapes,arrows}

\renewcommand{\baselinestretch}{1.2}

% Trying to waste less space

% \addtolength{\topmargin}{1pt}
% \addtolength{\headsep}{5pt}
% \addtolength{\oddsidemargin}{-4pt}
% \addtolength{\marginparwidth}{20pt}
% \addtolength{\marginparsep}{-3pt}
% \addtolength{\textwidth}{-5pt}


% |===| Beamer things.

\input{conf/colors}
\input{conf/beamer}

% |===| Tikz things.

\input{conf/tikz}

% |===| Macros.

\input{conf/macros}

% |===| Nice links.

\hypersetup{
  colorlinks=true,
  linkcolor=url,
  urlcolor=url,
  pdftitle={Programming Language Concepts: Memory Management}
}


% |===| Title page info.

\title{
  Programming Language Concepts\\[3em]
  Memory Management\\[2em]
}

\author[]{
  Adrien Champion\\
  \href{mailto:adrien.champion@email.com}{adrien.champion@email.com}
}


\date{}

\begin{document}
\addtolength{\leftmargin}{-20pt}
\addtolength{\rightmargin}{-20pt}



\begin{frame}{}
  \titlepage
\end{frame}


\begin{frame}{Runtime: Reminder}

  \begin{center}
    \input{figures/abstract_machine}
  \end{center}

  \begin{center}
    \input{figures/runtime_execution_cycle}
  \end{center}

\end{frame}


\begin{frame}{Runtime: Data}
  In almost all \daiji{hi-level} languages, data at runtime is split in
  \medskip
  \begin{itemize}
    \item \daiji{the stack},
    \item \daiji{the heap}, and
    \item \daiji{read-only (static)} memory.
  \end{itemize}
  \bigskip

  Why?
  \begin{itemize}
    \item<2-> each manages \daiji{a specific aspect} of the runtime with
      \daiji{particular constraints}
    \item<2-> managing them uniformly would be \daiji{very inefficient}
  \end{itemize}

\end{frame}


\section{The stack}

\begin{frame}{Manages block semantics}

  The stack handles the \daiji{local environment} in blocks in a
  \daiji{contiguous area in memory}
  \bigskip

  \daiji{LIFO} behavior: \daiji{L}ast \daiji{I}n \daiji{F}irst \daiji{O}ut
  \begin{itemize}
    \item can \daiji{push (add)} a new local environment
    \item can \daiji{pop (remove)} the most recent environment
  \end{itemize}
  \bigskip
  \pause

  The stack is important to handle \daiji{function calls} at runtime:\\
  $\impl$ sometimes called the \daiji{call stack}

\end{frame}


\begin{frame}{Behavior}

  \begin{minipage}{.7\textwidth}
    \begin{itemize}\bigsep
    \item \daiji{Program init}\\
      some space is allocated for the stack
    \item<2-> \daiji{Program start}\\
      first local environment is \daiji{pushed}
    \item<4-> When \daiji{entering a block}\\
      push a new local environment
    \item<6-> When \daiji{exiting a block}\\
      pop the most recent local environment
      \uncover<7->{}
    \end{itemize}
  \end{minipage}~\begin{minipage}{.29\textwidth}
    \begin{center}
      \input{figures/stack}
    \end{center}
  \end{minipage}

\end{frame}


\begin{frame}[fragile]{Example}

  \begin{minipage}{.7\textwidth}

    \begin{large_rust}
fn fib(n: usize) -> usize {
  if n <= 1 { 1 }
  else { fib(n - 1) + fib(n - 2) }
}
    \end{large_rust}

    Called from somewhere:
    \begin{large_rust}
...
let fib_res = fib(param) ;
println!(
  "fib({}) is {}", param, fib_res
) ;
...
    \end{large_rust}

    What information does this local environment contain?
  \end{minipage}~\begin{minipage}{.29\textwidth}
    \input{figures/stack_fib}
  \end{minipage}

\end{frame}



\begin{frame}[fragile,t]{Example}
  \begin{minipage}{.49\textwidth}
    \begin{rust}
fn fib(n: usize) -> usize {
  if n <= 1 { 1 }
  else {
    fib(n - 1) + fib(n - 2)
  }
}
    \end{rust}
  \end{minipage}~\begin{minipage}{.5\textwidth}
    \begin{rust}
...
let fib_res = fib(param) ;
println!(
  "fib({}) is {}", param, fib_res
) ;
...
    \end{rust}
  \end{minipage}
  \bigskip

  Local environment:
  \begin{center}
    \begin{tabular}{c c l}
      value of \code{param} &
        \uncover<2->{--} &
        \uncover<2->{parameter (\code{n})} \\
      address of \code{fib\_res} &
        \uncover<3->{--} &
        \uncover<3->{result address} \\
      address of \code{println} instruction &
        \uncover<4->{--} &
        \uncover<4->{return address (next instruction)} \\
      \code{usize} value ``\code{res\_1}'' &
        \uncover<5->{--} &
        \uncover<5->{intermediary result (\code{fib(n-1)})} \\
      \code{usize} value ``\code{res\_2}'' &
        \uncover<6->{--} &
        \uncover<6->{intermediary result (\code{fib(n-2)})} \\
    \end{tabular}
  \end{center}
  \medskip

  \uncover<7->{
    If \code{param = n > 1}, a new environment is pushed for the call
    \code{fib(n-1)}
  }
\end{frame}



\begin{frame}[fragile,t]{Example}
  \begin{minipage}{.49\textwidth}
    \begin{rust}
fn fib(n: usize) -> usize {
  if n <= 1 { 1 }
  else {
    fib(n - 1) + fib(n - 2)
  }
}
    \end{rust}
  \end{minipage}~\begin{minipage}{.5\textwidth}
    \begin{rust}
...
let fib_res = fib(param) ;
println!(
  "fib({}) is {}", param, fib_res
) ;
...
    \end{rust}
  \end{minipage}
  \bigskip

  Local environment:
  \begin{center}
    \begin{tabular}{c | c}
      env for \code{fib(param)} & new env pushed for \code{fib(n-1)}\\\hline
      value of \code{param} &
        \uncover<2->{value of \code{param - 1}} \\
      address of \code{fib\_res} &
        \uncover<3->{address of \code{res\_1}} \\
      address of \code{println} instruction &
        \uncover<4->{call to \code{fib(n-2)}} \\
      \code{usize} value ``\code{res\_1}'' &
        \uncover<5->{\code{usize} value ``\code{res\_1\_2}''} \\
      \code{usize} value ``\code{res\_2}'' &
        \uncover<6->{\code{usize} value ``\code{res\_2\_2}''} \\
    \end{tabular}
  \end{center}
\end{frame}


\begin{frame}{Environment}

  The local enviromnent contains the information necessary to
  \medskip
  \begin{itemize}\bigsep
    \item perform some computations:
      \begin{itemize}
        \item<2-> value of the \daiji{arguments} --- for a function
        \item<2-> \daiji{intermediate results} --- includes local variables
      \end{itemize}
    \item produce a result:
      \begin{itemize}
        \item<3-> \daiji{result address} --- where to write the value the
          block evaluates to
      \end{itemize}
    \item exit the current block:
      \begin{itemize}
        \item<4-> \daiji{return address} --- next instruction to execute after
          the block
      \end{itemize}
  \end{itemize}

\end{frame}



\begin{frame}{Overflows}
  \begin{minipage}{.7\textwidth}
    Every \daiji{nested} block and function call pushes a new environment
    \bigskip

    But the stack is \daiji{not growable}
    \bigskip

    If too many environments are pushed \ldots

    \uncover<4->{
      \begin{center}
        \daiji{stack overflow}
      \end{center}

      For example: \url{http://is.gd/9rvGmE}
    }
  \end{minipage}~\begin{minipage}{.29\textwidth}
    \begin{center}
      \input{figures/stack_overflow}
    \end{center}
  \end{minipage}
\end{frame}



\begin{frame}{Allocation}
  Allocating on the stack is \daiji{fast} because everything \daiji{is
  ordered}
  \medskip

  Size of the environment:\[
    \begin{array}{c c c}
      \code{size\_of(params)} & + &
        \overbrace{\code{size\_of(reference)}}^{\text{result ref}} +
        \overbrace{\code{size\_of(reference)}}^{\text{return ref}}\\
      & + & \code{size\_of(intermediary\_results)}
    \end{array}
  \]
  \medskip

  \daiji{Allocate} that space, set all the values, done
  \medskip

  \daiji{Deallocate} when exiting block
\end{frame}


\begin{frame}{Restriction}
  Unfortunately, the stack can only store \daiji{constant-size types}:
  \begin{itemize}
    \item numeric types, booleans, \daiji{constant-size} strings
    \item custom types \daiji{containing only constant-size types}
    \item<2-> references (to \daiji{anything})
  \end{itemize}
  \medskip

  \uncover<3->{Why?}
\end{frame}



\begin{frame}[fragile]{Example}
  \begin{minipage}{.7\textwidth}
    \begin{large_rust}
let mut vec = ... ;
vec.push(7) ;
    \end{large_rust}

    So what happens here?
    \bigskip

    \begin{itemize}
      \item<2-> a new environment is pushed for \code{push}
      \item<3-> which \daiji{augments} the vector with the value \code{7}
      \item<4-> everything after \code{vec} \daiji{has to be moved} to
        acommodate for the change
      \item<4-> all the structures handling the stack \daiji{have to be
        updated}
    \end{itemize}
  \end{minipage}~\begin{minipage}{.29\textwidth}
    \begin{center}
      \input{figures/stack_dynamic}
    \end{center}
  \end{minipage}
\end{frame}


\begin{frame}{Constant-size types}
  If the stack accepted non-constant-size types, it would be \daiji{very
    inefficient}
  \bigskip

  So what can we do instead?\\
  \uncover<2->{For values of a non-constant-size type, we}
  \smallskip
  \begin{itemize}
    \item<2-> store \daiji{constant-size info} in the stack
    \item<2-> store \daiji{the actual data} elsewhere
  \end{itemize}
  \bigskip

  \uncover<3->{
    For instance, vectors are stored in the stack as references
      (\daiji{constant-size})
  }
  \bigskip

  \uncover<4->{
    Where is \daiji{the actual data} stored then?
  }
\end{frame}



\section{The heap}

\begin{frame}{Purpose}
  \begin{itemize}\bigsep
    \item stores \daiji{values of non-constant-size type}
    \item that can be pointed to in the stack (\daiji{references are
      constant-size})
  \end{itemize}

\end{frame}



\begin{frame}{Characteristics}
  \begin{itemize}\bigsep
    \item \daiji{much bigger} than the stack
    \item \daiji{allocation} can be expensive
    \item \daiji{growing a value} can be expensive
  \end{itemize}
  \bigskip

  Why?
  \medskip
  \pause

  Unlike the stack, the \daiji{order} in which things
  \begin{itemize}
    \item are allocated,
    \item grow, and
    \item are freed
  \end{itemize}
  \daiji{depends on the runtime}
\end{frame}


\begin{frame}[t]{Workflow}
  \begin{center}
    \input{figures/heap_1}
  \end{center}
  \bigskip

  The heap is a \daiji{big section of memory} where values of
  non-constant-size types are stored
  \bigskip

  \uncover<2->{
    Values \daiji{can grow} \uncover<5->{
      and \daiji{can run into each other}
    }
  }
\end{frame}


\begin{frame}[t]{Collisions}
  \begin{center}
    \input{figures/heap_2}
  \end{center}
  \bigskip

  Before that happens
  \begin{itemize}
    \item memory management \daiji{must move data around}
    \item<2-> so that there is \daiji{enough space} for the growing structure
  \end{itemize}
\end{frame}


\begin{frame}[t]{(External) Fragmentation}
  \begin{center}
    \input{figures/heap_3}
  \end{center}
  \bigskip

  Problems can happen when \daiji{allocating} space too
  \medskip

  \begin{itemize}
    \item if we must allocate space that \daiji{does not fit anywhere}
    \item we need to \daiji{find a new arrangement} to create the contiguous
      space necessary
  \end{itemize}
\end{frame}

\begin{frame}{Collisions and Fragmentation}
  \daiji{Minimizing fragmentation} means
  \begin{itemize}
    \item \daiji{less space} between chunks of data
    \item \daiji{more collisions} (that must be resolved)
  \end{itemize}
  \medskip

  No perfect solution, designers choose whatever works well in practice
  \medskip

  \emph{Note that} all this memory management \daiji{happens at runtime}
\end{frame}



\begin{frame}{Why care?}

  We're not (well \daiji{you're not}) language designers, why care?
  \bigskip

  Because knowledge gives \daiji{power}
  \bigskip

  For instance the power to implement a simple task \daiji{much more
  efficiently}:
  \begin{center}
    \url{http://is.gd/lFDHcc}
  \end{center}

\end{frame}


\begin{frame}{Why care?}
  \daiji{Slice}s allow to pass \daiji{windows} over array-like structures
    stored on the heap \daiji{for free}
  \smallskip

  \begin{center}
    \url{http://is.gd/sc9kPz}
  \end{center}
  \bigskip

  A \daiji{slice} is just
  \begin{itemize}
    \item a \daiji{pointer to the first element} (64 bits)
    \item the \daiji{length of the slice} (integer, 64 bits)
  \end{itemize}
  \smallskip

  \begin{center}
    \url{http://is.gd/hvaivb}
  \end{center}
\end{frame}





\section{Read-only Memory}

\begin{frame}{Memory you can only read}
  Very optimized, very simple:
  \medskip
  \begin{itemize}\bigsep
    \item put the data there
    \item \daiji{cannot be modified}, ever (checked by compiler), so no
      management
    \item can be read \daiji{at any time by any thread / process}
  \end{itemize}
  \pause
  \bigskip

  That's it
\end{frame}

\begin{frame}{\code{String} and \code{\& 'static str}}
  In \daiji{Rust}:
  \medskip

  \begin{itemize}\bigsep
    \item values of type \code{String} are stored \daiji{on the heap}
    \item values of type \code{\& 'static str} are stored \daiji{in read-only
      memory}
  \end{itemize}
\end{frame}


\begin{frame}{Why not use the heap?}
  Why bother with read-only when we could store the data in the heap?
  \bigskip

  Fewer things in the heap mean less \daiji{fragmentation and collisions}
  \bigskip

  For realistic programs, \daiji{there is actually a lot of static data} that
  the compiler can \daiji{optimize in read-only memory}
\end{frame}


\end{document}
