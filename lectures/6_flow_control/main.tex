\documentclass[10pt]{beamer}
\usetheme{Warsaw}

% |===| Package imports.

\usepackage{
  etex, graphicx, amssymb, amsmath, amstext, amsfonts, mathtools,
  multicol, multirow, pgfplots, array, listings, colortbl, ulem, ifthen,
  xcolor, mathrsfs, xspace, rotating
}
\usepackage[scaled=1]{beramono}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{zlmtt}

\usetikzlibrary{shapes,arrows}

\renewcommand{\baselinestretch}{1.2}

% Trying to waste less space

% \addtolength{\topmargin}{1pt}
% \addtolength{\headsep}{5pt}
% \addtolength{\oddsidemargin}{-4pt}
% \addtolength{\marginparwidth}{20pt}
% \addtolength{\marginparsep}{-3pt}
% \addtolength{\textwidth}{-5pt}


% |===| Beamer things.

\input{conf/colors}
\input{conf/beamer}

% |===| Tikz things.

\input{conf/tikz}

% |===| Macros.

\input{conf/macros}

% |===| Nice links.

\hypersetup{
  colorlinks=true,
  linkcolor=url,
  urlcolor=url,
  pdftitle={Programming Language Concepts: Control Flow}
}


% |===| Title page info.

\title{
  Programming Language Concepts\\[3em]
  Control Flow\\[2em]
}

\author[]{
  Adrien Champion\\
  \href{mailto:adrien.champion@email.com}{adrien.champion@email.com}
}


\date{}

\begin{document}
\addtolength{\leftmargin}{-20pt}
\addtolength{\rightmargin}{-20pt}



\begin{frame}{}
  \titlepage
\end{frame}


\begin{frame}[t]{Memory Layout}

  \begin{center}
    \input{figures/memory_layout}
  \end{center}

  At runtime, some \textcolor{cstm_orange}{memory} is allocated for the program
  \smallskip

  It contains:
  \begin{itemize}
    \item the \daiji{data} handled by the program:
    \begin{itemize}
      \item constant-size (scoped) data in the \textcolor{cstm_red}{stack}
      \item non-constant-size data in the \textcolor{cstm_green}{heap}
      \item \textcolor{cstm_yellow}{static} data
    \end{itemize}
    \item the \daiji{commands} of the program
  \end{itemize}

\end{frame}


\begin{frame}[t]{Program Counter}

  \begin{center}
    \input{figures/pc}
  \end{center}

  At hardware level, the \daiji{Program Counter (PC)} points to the current
  command
  \smallskip
  \begin{itemize}
    \item incrementing it moves to the next command \uncover<6->{}
    \item<4-> a (conditional) \code{goto} can make it jump backward or forward
  \end{itemize}

\end{frame}

\begin{frame}{Control Flow}
  \daiji{Control flow} refers to the order in which the commands are executed
  \bigskip

  Hi-level languages
  \medskip
  \begin{itemize}
    \item provide constructs \daiji{much more abstract} than the hardware
    \item \daiji{do not provide} low-level control flow constructs (anymore)
  \end{itemize}
\end{frame}


\section{Expressions}

\begin{frame}[fragile]{Intro}
  An \emph{expression} is a syntactic entity whose evaluation either
  \begin{itemize}
    \item \daiji{produces a value}, or
    \item fails to terminate, the expression is \daiji{undefined}
  \end{itemize}
  \medskip

  All ``human-friendly'' languages have the notion of expression:
  \begin{large_rust}
4 + 3 * 2
Some(
  if n >= 2 { "value is big enough" }
  else { "value's too small" }
)
  \end{large_rust}
\end{frame}

\begin{frame}{Syntax}
  Expression: a single entity (token) applied to \daiji{some arguments}
  \smallskip
  \begin{itemize}
    \item arguments are expressions
    \item no arguments $\impl$ expression is a \daiji{value}
    \item<2-> \daiji{huge variations} in the syntax
  \end{itemize}
  \bigskip

  Examples?
\end{frame}

\begin{frame}{Notations}
  \daiji{Infix} --- mostly for primitive types:
  \begin{itemize}
    \item \code{3 + n * 2}
    \item \code{a \&\& b}
    \item note that precedence is important with infix
  \end{itemize}
  \bigskip

  \daiji{Prefix} --- typically for function applications:
  \begin{itemize}
    \item in ocaml: \code{f\_1 arg1 arg2 (f\_2 arg) arg4}
    \item in Rust: \code{f\_1(arg1, arg2, f\_2(arg), arg4)}
  \end{itemize}
  \bigskip

  \daiji{``Method style''}: \code{obj.method(arg1, ....)}
  \bigskip

  Conceptually, what's the difference between \daiji{functions} and
  \daiji{methods}?
  \smallskip
  \pause

  None besides the syntax for application: see ``method'' definitions in Rust.
\end{frame}

% \begin{frame}[fragile]{Digression on functions}
%   We can think of everything as \daiji{functions} being applied
%   \medskip

%   Point of view adopted in \daiji{functional languages}
%   \bigskip

%   Does Rust follow this approach?
%   \smallskip
%   \pause

%   Yes:

%   \begin{large_rust}
% impl ... {
%   pub fn blah(& self, args) -> ... { ... }
% }

% fn main() {
%   ...
%   obj.blah(args) ; // Application is method-style though.
%   ...
% }
%   \end{large_rust}

% \end{frame}

\begin{frame}{Evaluation Strategy}
  The \daiji{evaluation strategy} deals with how sub-expressions are evaluated:
  \begin{itemize}
    \item \daiji{order}
    \item \daiji{optimizations} (explained soon)
  \end{itemize}
  \medskip

  Does the evaluation strategy matter when there is a choice?
  \begin{tabular}{r l c}
    $\bullet$ &
      \code{7.0 * 0.00001 / 10000000.0} &
      \url{http://is.gd/E7sNvl}\\
    $\bullet$ &
      \uncover<2->{ok with integers then?} &
      \url{http://is.gd/htjwWM}\\
    $\bullet$ &
      \uncover<3->{\code{if}-\code{then}-\code{else}?} &
      \url{http://is.gd/N0XbJm}\\
    $\bullet$ &
      \uncover<4->{boolean operators?} &
      \url{http://is.gd/578gr8}\\
  \end{tabular}
  \bigskip

  \uncover<5->{
    The evaluation strategy \daiji{matters a great deal} for the following
    reasons
  }
\end{frame}


\begin{frame}{Side effects}
  A function or an expression has a \daiji{side effect} if it has an
  \daiji{observable} interaction with something outside of its local
  environment
  \bigskip
  \pause

  \begin{itemize}
    \item modifying a variable in an outter block (\url{http://is.gd/N0XbJm})
    \item raising an exception
    \item printing / reading something
    \item writing / reading a file
    \item network communication
    \item \ldots
  \end{itemize}
  \bigskip
  \pause

  What can be said about a function that has no side effect?
  \smallskip
  \pause

  It will \daiji{always} return the same output when given the
  \daiji{same inputs}.
\end{frame}


\begin{frame}{Finite Arithmetic}
  Arithmetic types
  \begin{itemize}\bigsep
    \item can \href{http://is.gd/htjwWM}{overflow}: \code{MAX\_INT + 1}
    \item can underflow: \code{MIN\_INT - 1}
    \item \href{http://is.gd/E7sNvl}{fail to represent a number precisely}:
      \code{1.0 / 3.0}
  \end{itemize}
\end{frame}


\begin{frame}{Eager / Lazy Evaluation}
  \daiji{Eager}: evaluate all arguments, and then the operation\\
  \uncover<3->{
    \daiji{$\impl$} much more efficient in practice (why?)
  }
  \medskip

  \daiji{Lazy}: evaluate arguments only when needed\\
  \uncover<2->{
    \daiji{$\impl$} theoretically optimal
  }
  \bigskip

  \uncover<4->{Most languages use \daiji{eager evaluation}}
  \begin{itemize}
    \item<4-> for speed
    \item<4-> but also \daiji{because of side-effects}
    \item<4-> with the exception of \code{if}-\code{then}-\code{else}
    \item<4-> and \href{http://is.gd/578gr8}{boolean operators}
      (\code{\&\&}, \code{||})
  \end{itemize}
\end{frame}




\section{Commands}


\begin{frame}{Intro}
  \daiji{Expression} (reminder):\\
    a syntactic element the evaluation of which produces a value
  \bigskip

  \daiji{Command}:\\
    a syntactic element the evaluation of which \daiji{does not} produce a
    value
\end{frame}


\begin{frame}{Intro}
  The purpose of commands is to perform \daiji{side effects}
  \begin{itemize}
    \item assignments
    \item printing / reading, raising exceptions
    \item communication with other processes / threads, over the network
    \item \ldots
  \end{itemize}
  \bigskip

  Arguably the most important one is \daiji{assignments}\\
  \daiji{$\impl$} deals with \daiji{variables}, what's a variable?
\end{frame}


\begin{frame}{Variables (Math)}
  A \daiji{variable} is a \daiji{value} from some set
  \bigskip

  From a CS point of view, it is \daiji{immutable}:\medskip
  \begin{itemize}
    \item Let \daiji{$x$} be a real number
    \item If \daiji{$x$} is odd, then \emph{(math stuff)}, otherwise
      \emph{(math stuff)}
    \item \daiji{$x$} is still the same after the math stuff
  \end{itemize}
\end{frame}


\begin{frame}[fragile]{Variables (CS, \daiji{coarse} view)}
  A \daiji{variable} is a name for a \daiji{memory location} storing a value
  \bigskip

  In \daiji{imperative languages}, variables are \daiji{mutable}:
  \begin{javal}
long n = 3 ;
long sum = 0 ;
while (n > 0) { sum += n ; n -= 1 ; }
  \end{javal}
  \bigskip

  In \daiji{functional languages}, variables are \daiji{immutable}
  \begin{itemize}
    \item equivalent to the mathematical definition
    \item<2-> still Turing-complete? What can while/for loops do?
    \item<3-> Not much, but still Turing-complete thanks to \daiji{recursion}
  \end{itemize}
\end{frame}


\begin{frame}{Assignments}
  Syntax: \[ \mita{lhs} \quad \code{op} \quad \mita{rhs} \]
  \medskip
  where \code{op} is typically among\\
  \begin{tabular}{r c l}
    $\bullet$ & \code{=} & rust, java, C, python, \ldots\\
    $\bullet$ & \code{:=} & ocaml, pascal, \ldots\\
    $\bullet$ & \code{<-} & APL, \ldots
  \end{tabular}
  \bigskip

  For example:
  \begin{itemize}
    \item \code{x = 2 ;}
    \item \code{x = y + 7 ;}
    \item \code{x = y + x ;}
  \end{itemize}
\end{frame}

\begin{frame}{Assignments}
  \[ \mita{lhs} \quad \code{=} \quad \mita{rhs} \]

  The \daiji{evaluation processes} differs for $\mita{lhs}$ and
  $\mita{rhs}$
  \medskip

  \begin{itemize}\bigsep
    \item $\mita{\daiji{l}hs}$ evaluates to an \daiji{l-value}: a memory
      location
    \item $\mita{\daiji{r}hs}$ evaluates to a \daiji{r-value}: an actual value
  \end{itemize}
  \medskip

  \code{x = x + 2 ;}
  \begin{itemize}
    \item evaluate \code{x + 2}: add \code{2} to the \daiji{value} stored at
      the location of \code{x}
    \item store the result at the \daiji{memory location} represented by
      \code{x}
  \end{itemize}
\end{frame}


\begin{frame}{Assignments versus Bindings}
  An assignment is a \daiji{dynamic} (runtime) operation:
  \begin{itemize}
    \item put the \daiji{r-value} at the location of the \daiji{l-value}
  \end{itemize}

  \bigskip

  A \daiji{binding} is a \daiji{static} (compile-time) operation:
  \begin{itemize}
    \item it gives a name to something that is already there
    \item has \daiji{no effect} on memory
  \end{itemize}
  \begin{center}
    Example: \url{http://is.gd/LFuigd}
  \end{center}
\end{frame}


\begin{frame}{Assignments versus Bindings}
  Most languages either
  \smallskip
  \begin{itemize}
    \item do not (explicitely) have bindings, or
    \item \daiji{unify} the notions of binding and assignment
  \end{itemize}
  \bigskip

  Rust, and most functional languages follow the latter
  \medskip

  This means whether a \daiji{let-binding} allocates memory or is just an alias
  is decided (mostly) during the \daiji{optimization phase}
  \medskip

  $\impl$ the programmer can only \daiji{trust} the compiler
\end{frame}




\section{Sequence Control ``Commands''}


\begin{frame}[fragile]{Semicolon}
  The \code{;} almost systematically denotes a \daiji{sequence} of commands
  \medskip

  \begin{rust}
let x = 7 ;
println!("x: {}", 7) ;
...
  \end{rust}
  \medskip

  Commands are executed \daiji{sequentially}, one after the other
\end{frame}


\begin{frame}[fragile]{Composite command}
  Commands can be aggregated in \daiji{blocks}, forming a \daiji{composite
    command}
  \medskip

  Blocks are delimited
  \begin{itemize}
    \item usually by \code{\{} and \code{\}} (rust, java, \ldots)
    \item sometimes by \code{begin} and \code{end} (ocaml, pascal, algol,
      \ldots)
  \end{itemize}
  \medskip

  \begin{rust}
let vec = vec![1, 2, 3] ;
{ let sum = vec.fold(0, |sum, elm| sum + elm) ;
  println!("sum: {}", vec)
} ;
...
  \end{rust}
\end{frame}


\begin{frame}[fragile]{\code{goto}-s}
  \code{goto} allows to jump anywhere in the program
  \medskip

  \begin{large_rust}
...
'label_1: x += 1 ; // Labelling an point in the program.
...
match something {
  Some( value ) =>
    if x == 0 { goto('label_1) } else { goto('label_2) },
  None => ...
}
...
'label_2: x = 7 ;
...
  \end{large_rust}
\end{frame}

\begin{frame}{\code{goto}-s}

  Introduce countless \daiji{pragmatic} problems:
  \begin{itemize}
    \item jump between points \daiji{thousands of lines} apart
    \item exit functions using \daiji{goto}-s
    \item jump \daiji{into a function / loop}?
    \item<2-> jump \daiji{inside a block}, hijacking the stack mechanism
    \item<2-> \ldots
  \end{itemize}
  \medskip
  \pause

  \code{goto} makes code hard to
  \begin{itemize}
    \item write, read and maintain
    \item compile: can hijack the stack, hard to optimize
  \end{itemize}
\end{frame}

\begin{frame}{\code{goto}-s}
  \code{goto} is replaced by alternatives that are more
  \begin{itemize}
    \item \daiji{local} --- respects the stack
    \item \daiji{readable}
    \item \daiji{easier on the compiler}
  \end{itemize}
  \bigskip

  Typically:
  \begin{itemize}
    \item \code{return}:\\
      $\quad$ exit current function with some value
    \item \code{break}:\\
      $\quad$ exit current loop, go to instruction after it
    \item \code{continue}:\\
      $\quad$ execute next iteration of the current loop
  \end{itemize}
\end{frame}


\begin{frame}{Branching (conditional ``commands'')}
  \begin{itemize}
    \item if-then-else (\code{ite}) --- standard branching
    \item switch / case --- equivalent to nested \code{ite}-s
    \item pattern matching
  \end{itemize}
  \medskip

  Pattern matching is a \daiji{safer} and \daiji{more powerful} take on
  branching:
  \begin{itemize}
    \item checks \daiji{exhaustiveness}
    \item can \daiji{deconstruct} nested values
    \item can introduce \daiji{bindings}
  \end{itemize}
  \smallskip

  \href{https://doc.rust-lang.org/book/patterns.html}{Patterns} give pattern
  matching its full power
  \bigskip
  \pause

  \daiji{NB}: in expression-based languages (\ita{e.g.} rust), conditionals are
  actually \daiji{expressions}
\end{frame}


\begin{frame}{Loops: \code{while}}
  \code{while} loops have a \daiji{condition} and a \daiji{body}
  \medskip

  \begin{itemize}
    \item[1.] evaluate \daiji{condition}
    \item[2.] if condition is true, execute \daiji{body}, otherwise go to $4.$
    \item[3.] go to $1.$
    \item[4.] instruction following the \code{while} loop
  \end{itemize}
  \medskip

  \daiji{Can loop forever}
\end{frame}


\begin{frame}[fragile]{Loops: \code{for}}
  \code{for} loops come in two different flavors
  \bigskip

  First, \daiji{numerically-controlled}: four arguments
  \medskip
  \begin{large_rust}
for (init, condition, step) {
  body
}
  \end{large_rust}
  \medskip

  \begin{center}
    \href{http://www.tutorialspoint.com/cplusplus/cpp_for_loop.htm}{%
      Example in C++
    }
  \end{center}
  \medskip
  \pause

  \begin{itemize}
    \item introduces a \daiji{binding}: ``control variable''
    \item \daiji{Can also loop forever}
  \end{itemize}
\end{frame}



\begin{frame}[fragile]{Loops: \code{for}}

  Second flavor, \daiji{iterators}:
  \medskip

  \begin{large_rust}
for element in iterator { body }
  \end{large_rust}
  \medskip

  \begin{itemize}
    \item requires a special \daiji{iterator/range type} the compiler can
      recognize
    \item can loop forever?
    \item<2-> depends on how the iterator/range is defined
    \item<2-> \href{https://doc.rust-lang.org/stable/std/iter/trait.Iterator.html}{in rust}, can loop forever: \url{http://is.gd/nlBABk}
    \item<2-> (\href{http://is.gd/lwOiR3}{range example in rust})
  \end{itemize}
\end{frame}



\section{A World Without While}




\begin{frame}[fragile]{Recursion}
  Recursion is an \daiji{alternative} to loop commands
  \begin{itemize}
    \item achieve \daiji{the same expressive power}
    \item recursion is \daiji{ubiquitous in functional programming}
  \end{itemize}
  \medskip

  \url{http://is.gd/uU7JR8}:
  \begin{large_rust}
fn sum(n: usize) -> usize {
  if n > 0 { n + sum(n - 1) } else { 0 }
}
  \end{large_rust}
  \bigskip

  See any problem with recursion?
  \medskip
  \pause

  Can \daiji{stack overflow} if too many nested calls are made
\end{frame}


\begin{frame}[fragile]{Recursion}
  \url{http://is.gd/uU7JR8}:
  \begin{large_rust}
fn sum(n: usize) -> usize {
  if n > 0 { n + sum(n - 1) } else { 0 }
}
  \end{large_rust}
  \medskip
  
  Every new call \daiji{pushes a context} on the stack\\
  Is it really necessary?
  \pause
  \medskip

  \daiji{Yes}
  \begin{itemize}
    \item we must know the value of \code{sum(n-1)}
    \item so that we can evaluate \code{n + sum(n - 1)}
  \end{itemize}
\end{frame}


\begin{frame}[fragile]{Recursion}
  How about this then (\url{http://is.gd/mfE62V}):
  \begin{large_rust}
fn sum(n: usize) -> usize {
  fn l00p(sum: usize, n: usize) -> usize {
    if n > 0 { l00p(sum + n, n - 1) } else { sum }
  }
  l00p(0, n)
}
  \end{large_rust}
  \medskip
  \pause
  
  \begin{itemize}
    \item in the recursive case, we just \daiji{return the value}
    \item we \daiji{do not do anything else} with result
    \item can't we just pop the current context before pushing the new one?
    \item (meaning we cannot stack overflow)
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Recursion}
  \begin{rust}
fn sum(n: usize) -> usize {
  fn l00p(sum: usize, n: usize) -> usize {
    if n > 0 { l00p(sum + n, n - 1) } else { sum }
  }
  l00p(0, n)
}
  \end{rust}
  \medskip

  Yes we can: \daiji{Tail Call Optimization (TCO)}
  \medskip
  \pause

  Actually, it depends (on memory management, among other things):
  \begin{itemize}
    \item not possible \daiji{systematically} in rust or scala for instance
    \item systematic in functional languages: ocaml, haskel, \ldots
  \end{itemize}
\end{frame}


\end{document}
