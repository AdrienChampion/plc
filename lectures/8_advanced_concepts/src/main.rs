#![allow(dead_code, )]


// |===| 3D vectors.

pub struct Vec3 {
  vec: Vec<f64>,
}

impl Vec3 {
  pub fn mk(x: f64, y: f64, z: f64) -> Self {
    Vec3 { vec: vec![x, y, z] }
  }

  pub fn x(& self) -> f64 { self.vec[0] }
  pub fn y(& self) -> f64 { self.vec[1] }
  pub fn z(& self) -> f64 { self.vec[2] }

  pub fn fold<
    T, Fun: Fn(T, f64) -> T
  >(& self, fun: Fun, init: T) -> T {
    let mut acc = init ;
    for elm in self.vec.iter() {
      acc = fun(acc, * elm)
    } ;
    acc
  }
}



// |===| Alternative implementation for 3D vectors.

pub struct Vec32 {
  x: f64, y: f64, z: f64
}



// |===| 4D vectors structure.

pub struct Vec4 {
  x: f64, y: f64, z: f64, last: f64
}



/// Provides vector functionnalities, given a way to iterate on the
/// coordinates.
pub trait Vec3Trait {
  /// Mutable iteration.
  fn iter_mut<Fun: Fn(& mut f64)>(& mut self, fun: Fun) ;
  /// Non-mutable iteration.
  fn iter< Fun: Fn(f64) >(& self, fun: Fun) ;

  /// Pretty prints the vector with a title.
  fn print(& self, title: & str) {
    println!("{}:", title) ;
    self.iter(
      |elm| println!("  {}", elm)
    )
  }

  /// Multiplication by a scalar.
  fn mul_scalar(& mut self, scalar: f64) {
    self.iter_mut(
      |elm: & mut f64| * elm = (* elm) * scalar
    ) ;
  }

}

impl Vec3Trait for Vec3 {
  fn iter< Fun: Fn(f64) >(& self, fun: Fun) {
    for elm in self.vec.iter() {
      fun(* elm)
    }
  }
  fn iter_mut<Fun: Fn(& mut f64)>(& mut self, fun: Fun) {
    for elm in self.vec.iter_mut() {
      fun(elm)
    }
  }
}

impl Vec3Trait for Vec4 {
  fn iter< Fun: Fn(f64) >(& self, fun: Fun) {
    fun(self.x) ;
    fun(self.y) ;
    fun(self.z) ;
    fun(self.last)
  }
  fn iter_mut<Fun: Fn(& mut f64)>(& mut self, fun: Fun) {
    fun(& mut self.x) ;
    fun(& mut self.y) ;
    fun(& mut self.z) ;
    fun(& mut self.last)
  }
}

impl Vec3Trait for Vec32 {
  fn iter< Fun: Fn(f64) >(& self, fun: Fun) {
    fun(self.x) ;
    fun(self.y) ;
    fun(self.z)
  }
  fn iter_mut<Fun: Fn(& mut f64)>(& mut self, fun: Fun) {
    fun(& mut self.x) ;
    fun(& mut self.y) ;
    fun(& mut self.z)
  }
}


fn main() {
  let mut vec = Vec3::mk(1f64, 2f64, 3f64) ;
  vec.print("vec") ;

  let scalar = 5f64 ;

  vec.iter_mut(|elm: & mut f64| * elm = (* elm) * scalar) ;

  vec.iter_mut(|elm: & mut f64| * elm = (* elm) + 2.0 * scalar) ;
  vec.iter_mut(|elm: & mut f64| * elm = (* elm) - scalar) ;

  vec.print("vec") ;
  println!("") ;

  let sum = vec.fold(|acc, elm| {acc + elm}, 0f64) ;

  println!("sum is: {}", sum) ;
  println!("") ;

  let (_, intermediate) = vec.fold(
    |(sum, mut vec), elm| {
      // Compute the new sum.
      let sum = sum + elm ;
      // Update the vector.
      vec.push(sum) ;
      // Return the new sum and the updated vector.
      (sum, vec)
    },
    // Start with the sum being `0`, and an empty vector for the intermediary
    // sums
    (0f64, vec![])
  ) ;

  println!("intermediary sums:") ;
  for blah in intermediate.iter() {
    println!("  {}", blah)
  }
}