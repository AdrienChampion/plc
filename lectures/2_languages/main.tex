\documentclass[10pt]{beamer}
\usetheme{Warsaw}

% |===| Package imports.

\usepackage{
  etex, graphicx, amssymb, amsmath, amstext, amsfonts, mathtools,
  multicol, multirow, pgfplots, array, listings, colortbl, ulem, ifthen,
  xcolor, mathrsfs, xspace
}
\usepackage[scaled=1]{beramono}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{zlmtt}

\usetikzlibrary{shapes,arrows}

\renewcommand{\baselinestretch}{1.2}

% Trying to waste less space

% \addtolength{\topmargin}{1pt}
% \addtolength{\headsep}{5pt}
% \addtolength{\oddsidemargin}{-4pt}
% \addtolength{\marginparwidth}{20pt}
% \addtolength{\marginparsep}{-3pt}
% \addtolength{\textwidth}{-5pt}


% |===| Beamer things.

\input{conf/colors}
\input{conf/beamer}

% |===| Tikz things.

\input{conf/tikz}

% |===| Macros.

\input{conf/macros}

% |===| Nice links.

\hypersetup{
  colorlinks=true,
  linkcolor=url,
  urlcolor=url,
  pdftitle={Programming Language Concepts: Languages}
}


% |===| Title page info.

\title{
  Programming Language Concepts\\[3em]
  Languages\\[2em]
}

\author[]{
  Adrien Champion\\
  \href{mailto:adrien.champion@email.com}{adrien.champion@email.com}
}


\date{}

\begin{document}
\addtolength{\leftmargin}{-20pt}
\addtolength{\rightmargin}{-20pt}



\begin{frame}{}
  \titlepage
\end{frame}



\section{Levels of description}


\begin{frame}{Theory of signs}

Morris $1946$, ``Writings on the general theory of signs''\\
\medskip

Three levels of description:

\medskip

\begin{tabular}{r l | c}
$\bullet$ &
  \daiji{syntax} &
  which sentences are correct? (grammar)\\
$\bullet$ &
  \daiji{semantics} &
  which correct sentences have meaning and what is it?\\
$\bullet$ &
  \daiji{pragmatics} &
  how do we use meaningful sentences?\\
\end{tabular}

\bigskip

\pause

Examples:
\medskip

\begin{tabular}{r c | c}
$\bullet$ &
  \emph{Sweet love you me.} &
  incorrect: \daiji{ill-formed}\\\hline
$\bullet$ &
  \uncover<3->{\emph{It's sweet that you love me.}} &
  \uncover<3->{correct: \daiji{well-formed}}\\\hline
\multirow{2}{*}{$\bullet$} &
  \multirow{2}{*}{
    \uncover<4->{\emph{It's pink that sky loves cat.}}
  } &
  \uncover<4->{well-formed but}\\
& &
  \uncover<4->{\daiji{undefined semantics}}\\\hline
\multirow{2}{*}{$\bullet$} &
  \uncover<5->{\emph{Your person loving myself is a fact}} &
  \uncover<5->{well-formed but not very}\\
&
  \uncover<5->{\emph{that I would qualify as being sweet.}} &
  \uncover<5->{\daiji{idiomatic} or \daiji{pragmatic}}\\
\end{tabular}

\end{frame}



\begin{frame}[fragile]{In programming}

For programming languages
\medskip

\begin{tabular}{r l c l}
$\bullet$ &
  syntax: &
  \emph{static} &
  compile time\\
$\bullet$ &
  semantics: &
  \emph{static / dynamic} &
  compile / runtime\\
$\bullet$ &
  pragmatics: &
  \emph{human} &
  code-writing time\\
\end{tabular}

\bigskip
\pause
Example:
\medskip

\begin{rust}
// Ill-formed: syntax error.
if struct whatever enum > println {} ;
// Well-formed.
struct Blah { x: usize }

fn main () {
  let blah = Blah { x: 7 } ;
  println!("{}", blah.x) ;
  // Well-formed but undefined semantics: parse error.
  println!("{}", blah.y) ;
  println!("{}", blah.1) ;
  println!("{}", blah[42]) ;
}
\end{rust}

\end{frame}



\section{Syntax}


\begin{frame}{Inductive definitions}

A syntax is based on a set of symbols $\term{T}$
\smallskip

It contains the symbols the language allows,
  \emph{e.g.} $\{ \term{a}, \term{b} \}$
\bigskip

A set of \daiji{inductive rules} specify the \daiji{well-formed sentences}
over $\term{T}$
\pause
\smallskip

They use
\medskip
\begin{itemize}
  \item a \daiji{special symbol $\empt$} to represent the empty sentence
  \item \term{terminal symbols} from $\term{T}$
  \item a set of \nterm{non-terminal symbols} \nterm{NT}
    ($\nterm{NT} \cap \term{T} = \emptyset$)
\end{itemize}
\bigskip

\emph{NB:} ``rules'' are also sometimes called \emph{productions} or
\emph{production rules}

\end{frame}




\begin{frame}{Inductive definitions}

A rule has form \daiji{$\nterm{S} \; \to \; w$} where
\medskip

\begin{itemize}
  \item $\nterm{S}$ is a non-terminal symbol
  \item $w$ is a sequence of symbols from $\nterm{NT} \cup \term{T}$
\end{itemize}
and is read \daiji{$\nterm{S}$ can be $w$}
\bigskip

Non-terminal symbols give the rules their \daiji{inductive / recursive nature}
\bigskip

\end{frame}


\begin{frame}{Inductive definitions: example}

Let $\term{T} = \{ \term{a}, \term{b} \}$ and the rule(s) be
\medskip

\[
  \begin{array}{c c l}
    \uncover<3->{\nterm{P} & \to & \empt}\\
    \nterm{P} & \to & \term{a}\\
    \uncover<3->{\nterm{P} & \to & \term{b}}\\
    \uncover<2->{\nterm{P} & \to & \term{a} \; \nterm{P} \; \term{a}}\\
    \uncover<3->{\nterm{P} & \to & \term{b} \; \nterm{P} \; \term{b}}\\
  \end{array}
\]
\bigskip

What can $\nterm{P}$ be?

\end{frame}



\begin{frame}{Context-free grammars (CFG)}

A \daiji{context-free grammar} is a quadruple $(\nterm{NT}, \term{T}, R, S)$
where
\medskip

\begin{itemize}
  \item $\nterm{NT}$ is the set of non-terminal symbols
  \item $\term{T}$ is the set of terminal symbols
  \item $R$ is the \daiji{set of rules}
  \item $S \in \nterm{NT}$ is the \daiji{initial (\textbf{S}tart) symbol}
\end{itemize}
\bigskip

The set of well-formed sentences for a CFG is the \daiji{language recognized /
generated} by this CFG.

\pause
The previous example corresponds to the CFG\[
  (\{\nterm{P}\}, \; \{\term{a}, \term{b}\}, \; R, \; \nterm{P} )
\]
where $R$ is the set of rules given previously.

The language it recognizes is the palindromes made of \term{a} and \term{b}

\end{frame}



\begin{frame}{Context-free grammars (CFG): example}

Consider the CFG $(\;
  \{\nterm{E}, \nterm{I}\}, \quad
  \{
    \term{a}, \term{b},
    \term{+}, \term{-}, \term{*}, \term{/}, \term{(}, \term{)}
  \}, \quad
  R, \quad
  \nterm{E}
\;)$
\pause
where $R$ is\[
  \begin{array}{c c c c | c c c c}
    1. & \nterm{E} & \to & \nterm{I} &
    \quad 7. & \nterm{I} & \to & \term{a} \\
    2. & \nterm{E} & \to & \nterm{E} \term{+} \nterm{E} &
    \quad 8. & \nterm{I} & \to & \term{b} \\
    3. & \nterm{E} & \to & \nterm{E} \term{-} \nterm{E} &
    \quad 9. & \nterm{I} & \to & \nterm{I}\term{a} \\
    4. & \nterm{E} & \to & \nterm{E} \term{*} \nterm{E} &
    \quad 10. & \nterm{I} & \to & \nterm{I}\term{b} \\
    5. & \nterm{E} & \to & \nterm{E} \term{/} \nterm{E} \\
    6. & \nterm{E} & \to & \term{(}\nterm{E}\term{)} \\
  \end{array}
\]
\bigskip

Intuitively, what do $\nterm{I}$ and $\nterm{E}$ represent?

What is the language recognized by this CFG?

\end{frame}


\begin{frame}{Context-free grammars (CFG): derivations}

  Given a CFG, we can \daiji{apply rules} to the start the symbol to
  \daiji{derive} sentences
  \bigskip

  From the previous example we can derive ``$ab\;*\;(a+b)$'':

  \begin{minipage}{.59\textwidth}
    \[
      \begin{array}{c c c c | c c c c}
        1. & \nterm{E} & \to & \nterm{I} &
        \quad 7. & \nterm{I} & \to & \term{a} \\
        2. & \nterm{E} & \to & \nterm{E} \term{+} \nterm{E} &
        \quad 8. & \nterm{I} & \to & \term{b} \\
        3. & \nterm{E} & \to & \nterm{E} \term{-} \nterm{E} &
        \quad 9. & \nterm{I} & \to & \nterm{I}\term{a} \\
        4. & \nterm{E} & \to & \nterm{E} \term{*} \nterm{E} &
        \quad 10. & \nterm{I} & \to & \nterm{I}\term{b} \\
        5. & \nterm{E} & \to & \nterm{E} \term{/} \nterm{E} \\
        6. & \nterm{E} & \to & \term{(}\nterm{E}\term{)} \\
      \end{array}
    \]
  \end{minipage}~\begin{minipage}{.4\textwidth}
    \[
      \begin{array}{c l l}
        \nterm{E}
        & \impl_4  \nterm{E} \term{*} \nterm{E}\\
        & \impl_1  \nterm{I} \term{*} \nterm{E}\\
        & \impl_{10} \nterm{I} \term{b} \term{*} \nterm{E}\\
        & \impl_7  \term{ab} \term{*} \nterm{E}\\
        & \impl_6  \term{ab} \term{*} \term{(} \nterm{E} \term{)}\\
        & \impl_2  \term{ab} \term{*}
          \term{(} \nterm{E} \term{+} \nterm{E} \term{)}\\
        & \impl_1  \term{ab} \term{*}
          \term{(} \nterm{I} \term{+} \nterm{E} \term{)}\\
        & \impl_7  \term{ab} \term{*}
          \term{(} \term{a} \term{+} \nterm{E} \term{)}\\
        & \impl_1  \term{ab} \term{*}
          \term{(} \term{a} \term{+} \nterm{I} \term{)}\\
        & \impl_8  \term{ab} \term{*}
          \term{(} \term{a} \term{+} \term{b} \term{)}\\
      \end{array}
    \]
  \end{minipage}

\end{frame}


\begin{frame}{Context-free grammars (CFG): generated language}

Given a CFG, let us write \daiji{$v \; \impl^* \; w$} if $w$ can be derived
from $v$ using the CFG's rules.
\bigskip

For the CFG of the previous example, \[
  \begin{array}{c c l}
  \nterm{E} \term{*} \nterm{E} & \impl^* & \term{ab*(a+} \nterm{I} \term{)}\\
  \nterm{E} & \impl^* & \term{ab*(a+} \nterm{I} \term{)}\\
  \nterm{E} & \impl^* & \term{ab*(a+b)}
  \end{array}
\]

\end{frame}


\begin{frame}{Context-free grammars (CFG): generated language}

The \daiji{language generated} by the CFG \[
  \gramm = (NT,\; \term{T},\; R,\; S)
\] is defined as \[
  \daiji{\langOf{\gramm} \; = \; \{ w \in \term{T}^* \; | \; S \impl^* w \}}
\]
\bigskip

where the $\;^*$ operator in $\term{T}^*$ is called \daiji{Kleene's star}

$S^*$ represents the set of all \daiji{finite strings over the set of symbols
$S$}

\end{frame}


\begin{frame}{Context-free grammars (CFG): derivation trees}

  A \daiji{derivation tree} is an \emph{ordered} tree in which:
  \medskip
  \begin{itemize}\bigsep
    \item each node is labelled with a symbol in
      $\nterm{NT} \cup \term{T} \cup \{ \empt \}$
    \item the root is labelled with the start symbol
    \item each interior (non-leaf) node is labelled with a symbol in
      $\nterm{NT}$
    \item for all nodes with label $\nterm{A}$ and kids $X_1, \ldots, X_n$
      (where $X_i \in \nterm{NT} \cup \term{T}$), \[
        \nterm{A} \to X_1 \ldots X_n
      \] is a rule of the CFG
    \item a node labelled $\term{\empt}$ has no siblings
  \end{itemize}

\end{frame}


\begin{frame}{Context-free grammars (CFG): derivation trees}

  Derivation tree\uncover<2->{\daiji{s}} for \emph{$a*b \; + \; b$}
  \bigskip

  \begin{minipage}{.49\textwidth}
    \begin{center}
      \input{figures/deriv_tree_1}
    \end{center}
  \end{minipage}~\begin{minipage}{.49\textwidth}
    \begin{center}
      \uncover<2->{\input{figures/deriv_tree_2}}
    \end{center}
  \end{minipage}

  \medskip

  \uncover<2->{
    We want derivation trees to represent the \daiji{canonical structure} of a
    string, \daiji{ambiguity is bad}
  }

\end{frame}



\begin{frame}{Context-free grammars (CFG): ambiguity}

  A grammar $\gramm$ is \daiji{ambiguous} if there exists one string of
  $\langOf{\gramm}$ which admits more than one derivation tree.
  \bigskip

  \begin{itemize}
    \item an ambiguous grammar cannot be used to translate (compile) a program
      in a \daiji{unique fashion}
    \item ambiguous grammar can often be \daiji{transformed in an unambiguous
      one} generating the same language
    \item an unambiguous grammar for a \daiji{human-friendly} language is often
      a bit complex as a result of removing ambiguities
  \end{itemize}

\end{frame}



\begin{frame}{Context-free grammars (CFG): ambiguity}

  Example
  \bigskip
  We saw the following grammar is ambiguous
  \medskip
  \[
    \begin{array}{c c c}
      \nterm{E} & \to &
        \nterm{I} \;|\;
        \nterm{E} \term{+} \nterm{E} \;|\;
        \nterm{E} \term{-} \nterm{E} \;|\;
        \nterm{E} \term{*} \nterm{E} \;|\;
        \nterm{E} \term{/} \nterm{E} \;|\;
        \term{(}\nterm{E}\term{)}\\
      \nterm{I} & \to &
        \term{a} \;|\;
        \term{b} \;|\;
        \nterm{I}\term{a} \;|\;
        \nterm{I}\term{b}\\
    \end{array}
  \]
  Intuitively, what is the problem?
  \medskip
  \begin{itemize}
    \item<2-> \daiji{order of the rules}, is it a problem in practice?
    \item<3-> corresponds (here) to \daiji{operator precedence}
  \end{itemize}
  \bigskip

  \uncover<4->{
    Version with \daiji{operator precedence}, \daiji{constants} (\nterm{C}) and
    \daiji{unary minus}
    \[
      \begin{array}{c c c}
        \nterm{E} & \to &
          \nterm{T} \;|\;
          \nterm{T} \term{+} \nterm{E} \;|\;
          \nterm{T} \term{-} \nterm{E} \\
        \nterm{T} & \to &
          \nterm{A} \;|\;
          \nterm{A} \term{*} \nterm{E} \;|\;
          \nterm{A} \term{/} \nterm{E} \\
        \nterm{A} & \to &
          \nterm{L} \;|\;
          \term{-} \nterm{A} \;|\;
          \term{(}\nterm{E}\term{)} \\
        \nterm{L} & \to &
          \nterm{I} \;|\;
          \nterm{C} \\
        \nterm{I} & \to &
          \term{a} \;|\;
          \term{b} \;|\;
          \nterm{I}\term{a} \;|\;
          \nterm{I}\term{b}\\
        \nterm{C} & \to &
          \term{[1-9]} \;|\;
          \nterm{C} \term{[0-9]}\\
      \end{array}
    \]
  }

\end{frame}


\begin{frame}{Context-free grammars (CFG): context}

  \daiji{``Context-free''}: rules are oblivious of what happened before they
  are applied
  \bigskip

  \daiji{Contextual syntactic constraints} say which sentences are legal
  \ita{w.r.t.} the context in which they appear.\\
  For instance:
  \medskip

  \begin{itemize}
    \item identifiers must be declared before they are used
    \item function calls respect the function's signature
    \item assignement should type-check
    \item \ldots
  \end{itemize}

\end{frame}



\section{Compilers}


\begin{frame}{Concrete and abstract syntax}

  The \daiji{concrete syntax} is the actual language: the set of strings
  recognized by the grammar.
  \medskip

  The \daiji{abstract syntax} is the set of derivation trees respecting the
  contextual syntactic constraints
  \medskip

  \begin{itemize}
  \item in a compiler, the \daiji{concrete syntax} (CFG) of the program is
    irrelevant
  \item what matters is its \daiji{canonical structure} (derivation tree)
  \end{itemize}

\end{frame}

\begin{frame}{From source to AST}

  \daiji{Any compiler} begins with a preprocessing phase to produce an
  \daiji{Abstract Syntax Tree (AST)} the compiler can
  \begin{itemize}
  \item simplify,
  \item type check,
  \item optimize, and
  \item generate code from.
  \end{itemize}
  \medskip

  {\small
    (In practice compilers can have several distinct AST structures for each
    task.)
  }

\end{frame}


\begin{frame}{Lexing}

  \daiji{Lexing (lexical analysis)}: from string to tokens
  \bigskip

  \daiji{Tokens} are \emph{meaningful logical units:} \term{terminal} symbols\[
    \begin{array}{l}
      \underbrace{\code{let}}\uncover<2->{_{\;}} \quad
      \underbrace{\code{mut}}\uncover<2->{_{\;}} \quad
      \underbrace{\code{blah}}\uncover<2->{_{\;}} \quad
      \underbrace{\code{=}}\uncover<2->{_{\;}} \quad
      \underbrace{\code{7}}\uncover<2->{_{\;}} \quad
      \underbrace{\code{;}}\uncover<2->{_{\;}}\\[2em]\pause
      \underbrace{\code{blah}}\uncover<2->{_{\;}} \quad
      \underbrace{\code{=}}\uncover<2->{_{\;}} \quad
      \underbrace{\code{blah}}\uncover<2->{_{\;}} \quad
      \underbrace{\code{++}}\uncover<2->{_{\;}} \quad
      \underbrace{\code{;}}\uncover<2->{_{\;}}\\
    \end{array}
  \]
  \medskip

  \begin{itemize}
  \item \daiji{No check} at all is performed during lexing
  \item the string is just \daiji{translated} into something we can apply the
    CFG to
  \end{itemize}

\end{frame}


\begin{frame}{Parsing}

  \daiji{Parsing (syntactic analysis)} constructs an \daiji{Abstract Syntax Tree (AST, derivation tree)} from the tokens
  \bigskip

  \begin{itemize}\bigsep
  \item is the program \emph{recognized by the rules of the grammar}?
  \item \daiji{context-free} checks, ``basic syntax well-formedness''
  \end{itemize}
  \bigskip

  \pause

  \emph{NB:}
  \begin{itemize}
  \item modern ``parser'' generators often collapse lexing and parsing
    together
  \item because it is more \daiji{efficient} (two actions in one traversal)
  \item and \emph{to some extent} simpler
  \item \daiji{parsing} often means lexing + parsing
  \end{itemize}

\end{frame}


\begin{frame}{Parsing: contextual syntactic analysis}

  \daiji{Contextual syntactic analysis} is when contextual syntactic
  constraints are checked
  \medskip

  \begin{itemize}
  \item performed on the \daiji{derivation tree} from the previous step
  \item usually augments the nodes with information such as types
  \end{itemize}
  \bigskip

  \pause

  \emph{NB:}
  \begin{itemize}
  \item also often referred to as a step of \daiji{parsing}
  \item some approaches to parser generation allow to collapse this phase with
  lexing and parsing (\daiji{parser combinators} for instance)
  \end{itemize}

\end{frame}


\begin{frame}{Translation to intermediate representation}

  \begin{itemize}\bigsep
    \item at this point we have an \daiji{annotated AST} (derivation tree) for
      the program
    \item the \daiji{source-language-specific} treatments and checks are over
    \item next we want to reason about the program to \daiji{optimize} it
    \item this treatment is \daiji{independent} from the source and object
      languages
    \item the AST is translated to an \daiji{intermediate structure} for
      \emph{flexibility}
  \end{itemize}

\end{frame}


\begin{frame}{Optimizations}

  We now have an \daiji{intermediate structure} representing the program
  \medskip

  \begin{itemize}
    \item \daiji{independent} from source / object language
    \item so that changes in either \daiji{do not impact the optimization
      phase}
  \end{itemize}

  Optimizations typically include
  \medskip

  \begin{itemize}
    \item \emph{dead code removal}
    \item \emph{function call inlining}
    \item \emph{subexpression factorisation}
    \item \emph{loop optimizations}
    \item \ldots
  \end{itemize}

\end{frame}


\begin{frame}{Code generation}

  The last step is to generate code in the object language
  \medskip

  \begin{itemize}\bigsep
    \item usually preceeded by a \daiji{object-language-specific optimization
      phase}
    \item gives the language its actual \daiji{semantics}
    \item \ldots{} produces code in the object language
  \end{itemize}

\end{frame}


\begin{frame}[fragile]{Hi-level recap}

  \begin{center}
    \input{figures/compil}
  \end{center}

\end{frame}









\section{Practical grammars}


\begin{frame}{Backus-Naur Form (BNF)}

  \daiji{BNF} give a more concrete syntax to rules
  \medskip

  \begin{itemize}
    \item \nterm{non-terminals} between angular brackets \bnfNterm{...}
    \item \term{terminals} between double quotes \bnfTerm{...}
  \end{itemize}
  \bigskip

  For example
  \medskip
  \begin{center}
    \begin{tabular}{c c l}
      \bnfNterm{E} & \texttt{::=} &
        \bnfNterm{T} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{T} \bnfTerm{+} \bnfNterm{E} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{T} \bnfTerm{-} \bnfNterm{E} \\
      \bnfNterm{T} & \texttt{::=} &
        \bnfNterm{A} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{A} \bnfTerm{*} \bnfNterm{E} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{A} \bnfTerm{/} \bnfNterm{E} \\
      \bnfNterm{A} & \texttt{::=} &
        \bnfNterm{L} \hspace{.2em}|\hspace{.2em}
        \bnfTerm{-} \bnfNterm{A} \hspace{.2em}|\hspace{.2em}
        \bnfTerm{(} \bnfNterm{E} \bnfTerm{)} \\
      \bnfNterm{L} & \texttt{::=} &
        \bnfNterm{I} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{C} \\
      \bnfNterm{I} & \texttt{::=} &
        \bnfTerm{a} \hspace{.2em}|\hspace{.2em}
        \bnfTerm{b} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{I}\bnfTerm{a} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{I}\bnfTerm{b}\\
      \bnfNterm{C} & \texttt{::=} &
        \bnfTerm{[1-9]} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{C} \bnfTerm{[0-9]}\\
    \end{tabular}
  \end{center}

\end{frame}


\begin{frame}{Extended Backus-Naur Form (EBNF)}

  \daiji{EBNF} adds some useful operators to improve readability
  \medskip

  \begin{itemize}
    \item \group{...}\many \hspace{1em} for \daiji{potentially empty
      repetitions}
    \item \group{...}\manyOne \hspace{1em} for \daiji{non-empty repetitions}
  \end{itemize}
  \bigskip

  For example
  \medskip
  \begin{center}
    \begin{tabular}{r c c l}
      & \bnfNterm{E} & \texttt{::=} &
        \bnfNterm{T} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{T} \bnfTerm{+} \bnfNterm{E} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{T} \bnfTerm{-} \bnfNterm{E} \\
      $\to$ & \bnfNterm{E} & \texttt{::=} &
        \uncover<2->{%
          \bnfNterm{T} \group{ \bnfTerm{[+ -]} \bnfNterm{T} }\many%
        } \\[1em]

      & \bnfNterm{I} & \texttt{::=} &
        \bnfTerm{a} \hspace{.2em}|\hspace{.2em}
        \bnfTerm{b} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{I}\bnfTerm{a} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{I}\bnfTerm{b}\\
      $\to$ & \bnfNterm{I} & \texttt{::=} &
        \uncover<3->{%
          \bnfTerm{[ab]}\manyOne%
        }\\[1em]

      & \bnfNterm{C} & \texttt{::=} &
        \bnfTerm{[1-9]} \hspace{.2em}|\hspace{.2em}
        \bnfNterm{C} \bnfTerm{[0-9]}\\
      $\to$ & \bnfNterm{C} & \texttt{::=} &
        \uncover<4->{%
          \bnfTerm{[1-9]} \bnfTerm{[0-9]}\many%
        }
    \end{tabular}
  \end{center}

\end{frame}









\section{Pragmatics}


\begin{frame}{Idiomatic code}

  \begin{itemize}\bigsep
    \item \daiji{human-level}
    \item pragmatics describes \daiji{good practice}
    \item informal
    \item partially given as \daiji{documentation} (standard library)
  \end{itemize}

\end{frame}


\begin{frame}[fragile,t]{Example in Rust (\url{http://is.gd/EMViVA})}

  \begin{large_rust}
/// Sums the elements of a vector.
fn terrible_sum(vec: Vec<usize>) -> usize {
  let mut sum = 0 ;
  let mut index = 0 ;
  while index < vec.len() {
    sum += vec[index] ;
    index += 1
  } ;
  sum
}
  \end{large_rust}
  \bigskip

  \uncover<2->{
    \daiji{Error-prone} and \daiji{extremely \textbf{not} idiomatic}
  }

\end{frame}


\begin{frame}[fragile,t]{Example in Rust (\url{http://is.gd/EMViVA})}

  \begin{large_rust}
/// Sums the elements of a vector.
fn bad_sum(vec: Vec<usize>) -> usize {
  let mut sum = 0 ;
  for i in 0..vec.len() {
    sum += vec[i]
  } ;
  sum
}
  \end{large_rust}
  \bigskip

  \uncover<2->{
    Still \daiji{error-prone} and \daiji{not very idiomatic}
  }

\end{frame}


\begin{frame}[fragile,t]{Example in Rust (\url{http://is.gd/EMViVA})}

  \begin{large_rust}
/// Sums the elements of a vector.
fn ok_sum(vec: Vec<usize>) -> usize {
  let mut sum = 0 ;
  for val in vec.iter() { // Can be optimized to
    sum += * val          // `vec.into_iter()`.
  } ;
  sum
}
  \end{large_rust}
  \bigskip

  \uncover<2->{
    Relatively \daiji{safe} and \daiji{idiomatic}
  }

\end{frame}


\begin{frame}[fragile,t]{Example in Rust (\url{http://is.gd/EMViVA})}

  \begin{large_rust}
/// Sums the elements of a vector.
fn awesome_sum(vec: Vec<usize>) -> usize {
  vec.into_iter().fold(0, |sum, val| sum + val)
}
  \end{large_rust}
  \bigskip

  \uncover<2->{
    \daiji{Concise}, \daiji{safe} and \daiji{idiomatic}
  }

\end{frame}


\begin{frame}[fragile,t]{Factorial in Rust (\url{http://is.gd/njsOV6})}

  \begin{large_rust}
/// Factorial of some number. Bad style.
fn terrible_factorial(mut n: usize) -> usize {
  let mut res = 1 ;
  while n > 1 {
    res = res * n ;
    n -= 1
  } ;
  res
}

/// Factorial of some number. Good style.
fn factorial(n: usize) -> usize {
  ( 2..(n+1) ).fold(1, |res, n| res * n)
}
  \end{large_rust}
  \bigskip

\end{frame}


\end{document}
