//! Re-explaining the same things over and over.
//!
//! # Grammar (starts with `Info`)
//!
//! |    |     |    |
//! |---:|:---:|:---|
//! | *Info* | `::=` | _ *FirstName* _ *LastName* _ *Workload* _ |
//! | *FirstName* | `::=` | `first` _ `name` _ `:` _ *JsonString* |
//! | *LastName* | `::=` | `last` _ `name` _ `:` _ *JsonString* |
//! | *Workload* | `::=` | `workload` _ `:` _ `{` _ **rep_sep(** `,` \| *Load* **)** _ `}`
//! | *Load* | `::=` | _ *JsonString* _ : _ *Int* _ `%` _ |
//!
//! # Structure to build
//!
//! ```
//! struct Info {
//!   first_name: String,
//!   last_name: String,
//!   work_load: Vec<(String, usize)>,
//! }
//! ```

extern crate ansi_term as ansi ;
extern crate range ;
extern crate piston_meta as grammar ;

use range::Range ;
use grammar::{ MetaData, Syntax } ;

macro_rules! error {
  ($err:expr) => ({
    use std::process::exit ;
    let red = ansi::Colour::Red ;
    let prefix = red.paint("|") ;
    let title = red.paint("|===|") ;
    println!("{} Error:", title) ;
    for line in $err.lines() {
      println!("{} {}", prefix, line)
    } ;
    println!("{}", title) ;
    println!("") ;
    exit(2)
  }) ;
}

fn try_error<T>(input: Result<T, String>) -> T {
  match input {
    Ok(data) => data,
    Err(err) => error!(err),
  }
}

/// Creates a parser.
pub fn mk_parser() -> Result<Syntax, String> {
  use grammar::* ;

  let rules = r#"
    1 first_name = [ "first" .w! "name" .w? ":" .w? .t!:"first_name" ]

    2 last_name = [ "last" .w! "name" .w? ":" .w? .t!:"last_name" ]

    3 work_load = [ "workload" .w? ":" .w? "{" .w? .s!("," load) .w? "}" ]

    4 load = [ .w? .t!:"load_desc" .w? ":" .w? .$:"load" .w? "%" ]

    5 info = [ .w? first_name .w? last_name .w? work_load .w? ]
  "# ;
  
  match syntax_errstr(rules) {
    Err(err) => Err(
      format!("could not create parser:\n{}", err)
    ),
    Ok(parser) => Ok(parser),
  }
}

/// Workload info.
pub struct Info {
  first_name: String,
  last_name: String,
  work_load: Vec< (String, usize) >,
}
impl Info {
  pub fn load(& self) -> usize {
    self.work_load.iter().fold(0, |sum, & (_, load)| sum + load)
  }
}

fn main() {
  use grammar::* ;

  let input = r#"
first name: "Dame"
last name: "Ningen"
workload: {
  "project 1": 75%,
  "project 2": 75%,
  "teaching": 40%,
  "research": 30%,
}
  "# ;

  let mut buffer = vec![] ;

  println!("") ;

  println!("|===| creating parser...") ;

  let parser = try_error(
    mk_parser()
  ) ;

  println!("| success") ;
  println!("|===| input:") ;
  for line in input.lines() {
    println!("| {}", line)
  } ;
  println!("|===| parsing input...") ;
  try_error(
    parse_errstr(
      & parser,
      & input,
      & mut buffer,
    )
  ) ;
  println!("| success") ;

  println!("|===| AST:") ;
  let mut spaces = 0 ;
  let bold = ansi::Style::new().bold() ;
  for token in buffer.iter() {
    match token.data {
      MetaData::StartNode(ref blah) => {
        println!(
          "| {:>3$}{}({})",
          "", bold.paint("start"), blah, spaces
        ) ;
        spaces += 2
      },
      MetaData::EndNode(ref blah) => {
        spaces -= 2 ;
        println!(
          "| {:>3$}{}({})",
          "", bold.paint("end"), blah, spaces
        )
      },
      _ => println!("| {:>2$}{:?}", "", token.data, spaces),
    }
  } ;

  println!("|===| building info") ;
  let info = try_error(
    ast_to_info(& buffer)
  ) ;
  println!("| success") ;
  println!("|===| info:") ;
  println!("| name: {} {}", info.first_name, info.last_name) ;
  println!("| work load:") ;
  for & (ref desc, ref load) in info.work_load.iter() {
    println!("|   {:>17}: {:>3}%", desc, load)
  } ;
  let load = info.load() ;
  println!("|   {:>17}: {:>3}%", "total", load) ;
  let risk = ::std::cmp::max(100, load) - 100 ;
  println!(
    "|   {}: {}%",
    bold.paint("heart attack risk"),
    if risk > 50 {
      ansi::Colour::Red.paint(format!("{:>3}", risk))
    } else {
      ansi::Colour::Green.paint(format!("{:>3}", risk))
    }
  ) ;
  println!("|===|") ;
  println!("") ;

  ()
}

/// Builds info from AST.
pub fn ast_to_info(ast: & [ Range<MetaData> ]) -> Result<Info, String> {
  use grammar::MetaData::* ;

  let mut first_name = None ;
  let mut last_name = None ;
  let mut load = vec![] ;

  let mut ast = ast.iter() ;

  loop {
    if let Some(token) = ast.next() {
      match token.data {
        String(ref key, ref val) => match & * key as & str {
          "first_name" => first_name = Some(val.clone()),
          "last_name" => last_name = Some(val.clone()),
          "load_desc" => match ast.next() {
            Some( & Range{ data: F64(ref key, ref int_val), .. } ) => {
              if (& * key as & str) == "load" {
                use std::str::FromStr ;
                match usize::from_str(& format!("{}", int_val)) {
                  Ok(int) => load.push( (format!("{}", val), int) ),
                  Err(err) => return Err(
                    format!(
                      "expected load (int) but got {}:\n{}", int_val, err
                    )
                  ),
                }
              } else {
                return Err(
                  format!(
                    "expected load (int) but got F64({},{})", key, int_val
                  )
                )
              }
            },
            Some(token) => return Err(
              format!("expected load (int) but got {:?}", token.data)
            ),
            None => return Err(
              "expected load (int) but got nothing".to_string()
            ),
          },
          _ => return Err(
            format!(
              "unexpected `String` token:\n  key: {}\n  val: {}", key, val
            )
          ),
        },
        StartNode(_) => (),
        EndNode(_) => (),
        _ => unimplemented!(),
      }
    } else { break }
  }

  match (first_name, last_name, load) {
    (None, _, _) => Err(
      "no first name given".to_string()
    ),
    (_, None, _) => Err(
      "no last name given".to_string()
    ),
    (Some(fst), Some(lst), loads) => if loads.len() > 0 {
      Ok(
        Info {
          first_name: format!("{}", fst),
          last_name: format!("{}", lst),
          work_load: loads,
        }
      )
    } else {
      Err(
        "no load given".to_string()
      )
    },
  }
}