\documentclass[10pt]{beamer}
\usetheme{Warsaw}

% |===| Package imports.

\usepackage{
  etex, graphicx, amssymb, amsmath, amstext, amsfonts, mathtools,
  multicol, multirow, pgfplots, array, listings, colortbl, ulem, ifthen,
  xcolor, mathrsfs, xspace, rotating, soul
}
\usepackage[scaled=1]{beramono}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{zlmtt}

\usetikzlibrary{shapes,arrows}

\renewcommand{\baselinestretch}{1.2}

% Trying to waste less space

% \addtolength{\topmargin}{1pt}
% \addtolength{\headsep}{5pt}
% \addtolength{\oddsidemargin}{-4pt}
% \addtolength{\marginparwidth}{20pt}
% \addtolength{\marginparsep}{-3pt}
% \addtolength{\textwidth}{-5pt}


% |===| Beamer things.

\input{conf/colors}
\input{conf/beamer}

% |===| Tikz things.

\input{conf/tikz}

% |===| Macros.

\input{conf/macros}

% |===| Nice links.

\hypersetup{
  colorlinks=true,
  linkcolor=url,
  urlcolor=url,
  pdftitle={Programming Language Concepts: Memory Management}
}


% |===| Title page info.

\title{
  Programming Language Concepts\\[3em]
  Memory Management\\[2em]
}

\author[]{
  Adrien Champion\\
  \href{mailto:adrien.champion@email.com}{adrien.champion@email.com}
}


\date{}

\begin{document}
\addtolength{\leftmargin}{-20pt}
\addtolength{\rightmargin}{-20pt}



\begin{frame}{}
  \titlepage
\end{frame}


\begin{frame}{Manual Memory Management}
  Originally, memory management was performed by the programmer:
  \smallskip

  \begin{itemize}\bigsep
    \item \texttt{addr = }\code{malloc}\texttt{(n) ;}\\
      \daiji{allocate} \code{n} bytes of memory somewhere\\
      returns the (start) \daiji{address} of this memory (\code{addr})
    \item \code{free}\texttt{(addr) ;}\\
      \daiji{release} the memory pointed to by \code{addr}
  \end{itemize}
  \bigskip

  An address is really an integer (\code{usize})
\end{frame}


\begin{frame}{Manual Memory Management}
  The address can be used \daiji{without restriction}
  \medskip

  \texttt{addr = }\code{malloc}\texttt{(16) ;}
  \begin{itemize}
    \item can copy the \daiji{value of the address}
    \item can \daiji{store it} in a struct, an array, or anything else
    \item can \daiji{pass it} to a function
    \item or anything you want at all
  \end{itemize}
  \medskip

  Problem: easy to violate \daiji{memory safety}
\end{frame}

\begin{frame}[fragile]{Dangling Pointers}
  Most common problem is \daiji{dangling pointers}:\\
  A pointer that points to memory that \daiji{has been freed}

  \begin{cee}
{
   char *dp = NULL;
   /* ... */
   {
       char c;
       dp = &c;
   } 
   /* c falls out of scope (pop stack, `c` is discarded) */
   /* dp is now a dangling pointer */
}
  \end{cee}
  (from \href{https://en.wikipedia.org/wiki/Dangling_pointer#Cause_of_dangling_pointers}{wikipedia})
\end{frame}


\begin{frame}{Dangling Pointers}
  How bad can it be?

  \begin{minipage}[fragile]{.6\textwidth}
    \begin{itemize}
      \item create pointer \hfill {\small \texttt{char *dp = NULL;}}
      \item<2-> create value \hfill {\small \texttt{char c = 'u';}}
      \item<3-> link \hfill {\small \texttt{dp = \&c;}}
      \item<4-> free! \hfill {\small \texttt{free(c);}}
      \item<6-> allocate! \hfill {\small \texttt{string blah = "blah";}}
      \item<7-> read value! \hfill {\small \texttt{* dp}}
      \item<10-> write value! \hfill {\small \texttt{* dp = 'n' ;}}
    \end{itemize}
  \end{minipage}~\begin{minipage}{.39\textwidth}
    \begin{center}
      \include{figures/dangling_pointers}
    \end{center}
  \end{minipage}

  \medskip

  \temporal<4-5>{
    Creating a dangling pointer...\\
    \uncover<5->{W'?}
  }{
    What's the problem here?\\
    \uncover<5->{\daiji{use after free} (can crash), can it get worse?}
  }{
    \temporal<7>{
      Of course!\\
      Say we end up allocating on the \daiji{freed memory}
    }{
      We can now \daiji{read arbitrary stuff}\\
      \daiji{Silent bug}, anything can happen, very hard to debug
    }{
      Can it get worse?\\
      \uncover<9->{
        Of course! Let's (further) \daiji{corrupt} memory!
        \uncover<10->{(Silent bug too)}
      }
    }
  }
\end{frame}


\begin{frame}{Manual Memory Management}
  \href{https://en.wikipedia.org/wiki/Memory_safety}{A lot of things}
  can go wrong:
  \medskip

  \begin{itemize}
    \item dangling pointers
    \item double free \hfill memory corruption
    \item invalid free \hfill heap corruption
    \item null pointer access \hfill crash, kernel corruption
  \end{itemize}
  \bigskip

  Violating memory safety often results in \daiji{security vulnerabilities}
  \bigskip
  \pause

  More memory problems:
  \smallskip
  \begin{itemize}
    \item arrays out-of-bounds errors
    \item uninitialized variables
    \item out-of-memory errors: memory leaks, stack overflow, heap exhaustion
  \end{itemize}
\end{frame}


\begin{frame}{Automatic Memory Management}
  Manual memory management is
  \begin{itemize}
    \item \daiji{unsafe}, thus \daiji{not secure}
    \item hard to \daiji{write} (design, debug, \ldots)
    \item hard to \daiji{read and maintain}: a lot can go wrong
  \end{itemize}
  \bigskip

  Virtually all modern languages do automatic memory management
  \begin{itemize}
    \item (almost) always \daiji{dynamic} (at runtime)
    \item do \daiji{safety checks}: out-of-bounds, overflow, initialization
    \item at the cost of performance
  \end{itemize}
  \medskip

  Goal: prevent \daiji{memory-unsafe code} as much as possible
\end{frame}



\begin{frame}{Automatic Memory Management}
  \daiji{Garbage Collection (GC)} identifies \daiji{dynamically} what's not
    ``used'' anymore on the heap and frees it
  \bigskip

  \st{But nothing is ever ``free''}\\
  But everything has \daiji{a cost}
\end{frame}




\section{On Demand Automatic GC}


\begin{frame}{Example: Smart Pointers}
  Most languages without a \daiji{GC} built in the runtime have \daiji{smart
  pointers}:
  \smallskip
  \begin{itemize}
    \item \ita{e.g.} C++, rust, \ldots
    \item \daiji{on demand} GC
    \item \ita{a.k.a.} Reference-Counted
      (\href{https://doc.rust-lang.org/stable/std/rc/}{\texttt{Rc}} in rust)
  \end{itemize}
  \medskip

  A smart pointer for a value stores two things:
  \begin{itemize}
    \item the value itself, and
    \item an integer counting the number of \daiji{live references}
  \end{itemize}
\end{frame}


\begin{frame}{Example: Smart Pointers}
  \begin{center}
    \include{figures/rc}
  \end{center}

  \begin{itemize}
    \item creation: \hfill \texttt{let ref\_1 = Rc::new(value) ;}
    \item<2-> \daiji{reference} duplication: \hfill \texttt{ref\_1.clone()}
    \item<4-> \daiji{reference} destruction: \daiji{decrement} \texttt{count},
      and
    \begin{itemize}
      \item<6-> if \texttt{count > 0}, \daiji{nothing happens}
      \item<9-> otherwise, \daiji{free the memory}: not used anymore
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{Example: Smart Pointers}
  Concrete example: \url{http://is.gd/VlCaaN}
  \medskip

  What cost are we paying?
  \medskip

  \begin{itemize}\bigsep
    \item \daiji{memory}-wise: \code{usize} overhead \hfill (the counter)
    \item when \daiji{allocating}: not much
    \item every time you \daiji{duplicate}:\\
      \uncover<2->{$100\%$ slower, copy address \daiji{and increment counter}}
    \item every time you \daiji{destruct}:\\
      \uncover<3->{\daiji{much} slower}
      \begin{itemize}
        \item<4-> in addition to discarding the reference, you
        \item<4-> decrement the \code{counter} and
        \item<4-> compare it to \code{0}
      \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{On Demand Dynamic GC}
  Smart pointers are \daiji{memory-safe}: no dangling pointers and such
  \bigskip

  Other on demand dynamic GC approaches include:
  \begin{itemize}
    \item \href{https://en.wikipedia.org/wiki/Tombstone_(programming)}{tombstones}
    \item \href{https://en.wikipedia.org/wiki/Locks-and-keys}{locks and keys}
      \hfill (actually unsafe)
  \end{itemize}
  \bigskip

  All have a cost to pay in terms of \daiji{memory and performance}.\\
  Does it matter?
  \begin{itemize}
    \item<2-> yes, if you need to go fast\\
      web browsers, video games, high performance calculus, high frequency
      trading, \ldots
    \item<2-> otherwise, maybe not
    \item<2-> the point is, you \daiji{must understand the consequences} to
      decide if it is appropriate for your use case
  \end{itemize}
\end{frame}




\section{Static Memory Management}


\begin{frame}{Motivation}
  Idea
  \smallskip
  \begin{itemize}
    \item have the \daiji{compiler} do \daiji{manual memory management} for us
    \item while \daiji{guaranteeing} as much memory safety as possible
  \end{itemize}
  \bigskip

  What do we gain?
  \smallskip
  \begin{itemize}
    \item<2-> \daiji{memory safety}
    \item<2-> for virtually \daiji{zero} runtime overhead
    \item<2-> a compiler (sometimes) able to optimize \daiji{more}
  \end{itemize}
\end{frame}


\begin{frame}{A Challenging Approach}
  Very bright people have been working on this for decades, but
  \medskip

  \begin{itemize}\bigsep
    \item difficult to make \daiji{useable}:\\
      prototype languages demanded \daiji{a lot} from programmers
    \item actually not easy to make \daiji{memory-efficient}:\\
      \code{free}-s should happen \daiji{as early as possible}
    \item non-trivial to implement
  \end{itemize}
  \bigskip

  Rust is the first mainstream language to feature \daiji{completely static
  memory management}
\end{frame}


\begin{frame}{Disclaimer}
  This section presents \daiji{Rust's take} on static memory management
  \bigskip

  The concepts will be presented in a generic context, but we will
  \smallskip
  \begin{itemize}\bigsep
    \item use \daiji{Rust's terms} to describe the concepts
    \item rely heavily on examples in Rust
  \end{itemize}
\end{frame}


\begin{frame}{Rust}
  \daiji{Rust} is very recent language (reached \code{1.0} in June 2015)
  \smallskip

  Three goals:
  \smallskip
  \begin{itemize}
    \item safety
    \item concurrency
    \item speed
  \end{itemize}
  \medskip

  Builds on ``region-based memory management'' --- three key concepts:
  \begin{itemize}
    \item \href{https://doc.rust-lang.org/book/ownership.html}{ownership}
    \item \href{https://doc.rust-lang.org/book/references-and-borrowing.html}{borrowing}
    \item \href{https://doc.rust-lang.org/book/lifetimes.html}{lifetimes}
      \hfill (advanced borrows, not discussed here)
  \end{itemize}
  \medskip

  {\small
    (the two first links above are \daiji{(extremely) recommended reading} for
    the project)
  }
\end{frame}



\begin{frame}{Three key concepts}
  The concepts discussed in this section are relevant
  \medskip
  \begin{itemize}\bigsep
    \item for the \daiji{programmer}, and
    \item \daiji{in the compiler}
    \begin{itemize}
      \item for checking \daiji{memory safety} and
      \item generate code, in particular \code{malloc}-s and \code{free}-s
    \end{itemize}
  \end{itemize}
  \bigskip
  \pause

  At runtime
  \medskip
  \begin{itemize}
    \item these concepts \daiji{do not exist} anymore
    \item memory is \daiji{allocated} and \daiji{freed} ``manually'' (by the
      compiler)
    \item no \daiji{garbage collection} (memory management is \daiji{free})
  \end{itemize}
\end{frame}



\begin{frame}{Ownership: Basics}
  Variable bindings have \daiji{ownership} of what they're bound to:
  \medskip
  \begin{itemize}
    \item means the data they're bound to is \daiji{freed} when the binding
      \daiji{dies}
    \item unless the \daiji{ownership} is \daiji{moved} (transfered)
  \end{itemize}
  \bigskip

  \daiji{A binding dies} when it goes \daiji{out of scope}\\
  Meaning when we close the block it is in:
  \begin{center}
    \url{http://is.gd/wcKPeu}
  \end{center}
\end{frame}



\begin{frame}[fragile]{Ownership: Move Semantics}
  For memory safety, there must be \daiji{exactly one binding} to any given
  resource
  \bigskip

  So, we can assign data to another binding:
  \smallskip
  \begin{large_rust}
// Binding `v` has ownership of the vector.
let v = vec![1, 2, 3] ;
// **Moves** the ownership to the binding `new_v`.
let new_v = v ;
  \end{large_rust}
  \smallskip

  but the binding for \code{v} cannot be used anymore:
  \begin{center}
    \url{http://is.gd/NqR5bV}
  \end{center}
\end{frame}



\begin{frame}[fragile]{Ownership: Move Semantics}
  ``Moving ownership'' \daiji{does not mean ``moving the value at runtime''}
  \bigskip

  Ownership checking (actually called \daiji{borrow checking})
  \medskip
  \begin{itemize}
    \item is a purely \daiji{static} (compile-time) operation, and
    \item lets the compiler generate the \code{free}-s and optimize
      \daiji{a lot of things}
  \end{itemize}
  \bigskip
  \pause

  In the previous examples,
  \smallskip
  \begin{itemize}
    \item the vector is stored \daiji{on the heap},
    \item bindings correspond to (stack) pointers to the vector,
    \item and all operations including \code{free}-s are \daiji{memory safe}
  \end{itemize}
  \smallskip
  Why memory safe?
  \pause
  Because the borrow checking \daiji{guarantees it is}
\end{frame}


\begin{frame}{Ownership: Copy Semantics}
  Sometimes, moves semantics is not appropriate
  \medskip

  Typically with primitive types which have \daiji{the same size as a
  reference}
  \begin{itemize}
    \item might as well \daiji{copy} the value directly
    \item copying, obviously, \daiji{happens at runtime}
  \end{itemize}
  \bigskip

  In rust the \href{https://doc.rust-lang.org/stable/std/marker/trait.Copy.html}{\texttt{Copy}} trait let you change from move to copy semantics:
  \begin{center}
    \url{http://is.gd/WKeYYR}
  \end{center} 
\end{frame}



\begin{frame}[fragile]{Ownership: Move Semantics}
  Move semantics for \daiji{functions}:
  \smallskip
  \begin{large_rust}
fn sausage(v: Vec<i32>) {
  // When exiting the function, `v` goes out of scope
  // and the corresponding resource is freed
}
  \end{large_rust}
  \bigskip
  \pause

  \daiji{$\impl$} binding no longer valid after the ownership has been
  \daiji{moved} (passed) to a function in a call:
  \begin{center}
    \url{http://is.gd/yyigoG}\\
    What about \url{http://is.gd/R7aXAX}?
  \end{center}
  \bigskip
  \pause

  What do you think we could do to \daiji{transfer the ownership back?}
\end{frame}



\begin{frame}[fragile]{Ownership: Move Semantics}
  Just return the value:
  \smallskip
  \begin{large_rust}
fn sausage(v: Vec<i32>) -> Vec<i32> {
  // Do stuff with `v`.
  ...
  // Return `v`
  v
}
  \end{large_rust}

  \begin{center}
    \url{http://is.gd/NkO5XC}
  \end{center}
  \pause

  From \daiji{outside} the \code{sausage} function, what's the difference with
  \begin{center}
    \url{http://is.gd/YjSoAi}?
  \end{center}
  \medskip
  \pause

  None
\end{frame}


\begin{frame}[fragile]{Ownership: Tedious Functions}

  From \href{https://doc.rust-lang.org/book/ownership.html#more-than-ownership}{The Book}:
  \smallskip
  \begin{large_rust}
fn foo(
  v1: Vec<i32>, v2: Vec<i32>
) -> (Vec<i32>, Vec<i32>, i32) {
    // Do stuff with v1 and v2
    // Hand back ownership, and the result of our function
    (v1, v2, 42)
}

let v1 = vec![1, 2, 3];
let v2 = vec![1, 2, 3];

let (v1, v2, answer) = foo(v1, v2);
  \end{large_rust}
  \medskip
  \pause

  This is terrible code that should use \daiji{borrowing}
\end{frame}



\begin{frame}{Borrowing: Basics}
  If \code{var} owns some data, \code{\& var} creates a \daiji{reference} to
  this data
  \medskip

  A reference
  \begin{itemize}
    \item does \daiji{not} move ownership
    \item \daiji{borrows} ownership \hfill
      \url{http://is.gd/YMgAwg}
    \item is \daiji{immutable} by default \hfill \url{http://is.gd/pHOKc3}
  \end{itemize}
\end{frame}



\begin{frame}{Borrowing: Mutation}
  If \code{var} owns some data, \code{\& mut var} creates a \daiji{mutable reference} to this data
  \medskip

  \begin{center}
    \url{http://is.gd/AcKRUj}
  \end{center}
  \medskip

  Note that the binding \daiji{must be mutable} to allow a \daiji{mutable}
  borrow
\end{frame}


\begin{frame}{Borrowing: Unsafe?}
  What did we gain? Seems like we can still create \daiji{dangling pointers}:
  \begin{center}
    \url{http://is.gd/MCNleM}
  \end{center}
  \medskip
  \pause

  The compiler won't let you :)
  \bigskip
  \pause

  Alright, but rust focuses on \daiji{safety}, efficiency and
  \daiji{concurrency}\\
  But we can't do \daiji{thread-unsafe} things (read while we write):
  \begin{center}
    \url{http://is.gd/LXmln6}
  \end{center}
  \medskip
  \pause

  The compiler won't let you :D
\end{frame}


\begin{frame}{Borrowing: Safe}
  Why won't the compiler let us do unsafe things?
  \medskip

  Because of the borrowing rules
  \medskip
  \pause

  \begin{block}{Rule \textcolor{orange!60!background}{\texttt{01}} / \textcolor{orange!60!background}{\texttt{10}} \hfill \url{http://is.gd/MCNleM}}
    any borrow must last for a scope \daiji{no greater than} that of the owner
  \end{block}
  \medskip
  \pause

  \begin{block}{Rule \textcolor{orange!60!background}{\texttt{10}} / \textcolor{orange!60!background}{\texttt{10}} \hfill \url{http://is.gd/LXmln6}}
    you may have \daiji{one or the other} of these two kinds of borrows,\\
    but \daiji{not both at the same time}
    \begin{itemize}
      \item one or more references (\code{\& T}) to a resource
      \item exactly one mutable reference (\code{\& mut T})
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}{Lifetimes}

  A \daiji{lifetime} describes the scope a reference is \daiji{valid} for
  \medskip

  Relatively technical, we will not discuss it in details here
  \medskip

  Just know that
  \begin{itemize}
    \item every reference \daiji{has a lifetime}
    \item compiler lets you \daiji{elide} (omit) them most of the time
  \end{itemize}

\end{frame}



\section{Systematic Garbage Collection}

\begin{frame}{Intro}
  GC \daiji{automatically} reclaims the memory on the heap that is
  \daiji{no longer used}
  \begin{itemize}
    \item performed \daiji{at runtime}
    \item smart pointers is an example of \daiji{on-demand} garbage collection
  \end{itemize}
  \bigskip

  In languages with \daiji{no explicit memory deallocation} (or static
  management)
  \begin{itemize}
    \item \daiji{systematic} GC is necessary
    \item otherwise the heap would explode
    \item trade-off between \daiji{memory footprint} and \daiji{runtime
      overhead}
  \end{itemize}
\end{frame}


\begin{frame}{History}
  In the world of \daiji{imperative languages}, GC
  \begin{itemize}
    \item is fairly new --- Java
    \item made programming much more accessible (no manual management)
  \end{itemize}
  \bigskip

  In the word of \daiji{functional languages}, GC
  \begin{itemize}
    \item is old news --- Lisp ($1960$)
  \end{itemize}
\end{frame}


\begin{frame}{The Problem}
  In most modern languages, all non-primitive types are \daiji{references}
  \begin{itemize}
    \item references have \daiji{Copy semantics}
    \item in particular in \daiji{Object-Oriented}
  \end{itemize}

  \bigskip

  When an object is created
  \begin{itemize}
    \item it is \daiji{allocated on the heap} and
    \item you get a \daiji{reference} back
  \end{itemize}
\end{frame}

\begin{frame}{The Problem}
  The reference is copied when you
  \begin{itemize}
    \item pass it to a function as argument
    \item<4-> store it in another object's field
    \item<6-> return it from a function
  \end{itemize}
  \uncover<10->{
    At some point \daiji{the reference dies}, but \daiji{when do we free}?
  }
  \bigskip

  \begin{center}
    \input{figures/problem}
  \end{center}
\end{frame}


\begin{frame}{More Problems}

  GC is difficult, consider \daiji{circular structures} for instance:

  \begin{center}
    \input{figures/problem_circ}
  \end{center}
  
  \begin{itemize}
    \item<9->
      note that \daiji{smart pointers} (reference counting) don't work here
    \item<9->
      both reference counts are \daiji{still $1$}, won't be freed
  \end{itemize}
\end{frame}


\begin{frame}{Basics}
  GC consists of two phases
  \begin{itemize}
    \item detect what is ``not used anymore'' --- \daiji{garbage detection}
    \item collect (free) these objects
  \end{itemize}
  \bigskip

  Notion of ``not used anymore'' needs to be defined
  \smallskip

  In practice, often a \daiji{conservative approximation}
  \begin{itemize}
    \item<2-> \daiji{approximation}: we may not free as much as we could\\
      $\impl$ for \daiji{efficiency} reasons
    \item<3-> \daiji{conservative}: don't free what can still be used\\
      $\impl$ that would be \daiji{unsafe} (dangling pointers \ita{etc.})
  \end{itemize}
\end{frame}


\begin{frame}{Capitalizing on the Stack}
  Garbage detection almost always \daiji{starts with the stack}:
  \begin{itemize}
    \item the stack represents the \daiji{current execution context}
    \item anything unreachable from stack reference is \daiji{not usable}
  \end{itemize}
  \bigskip

  Garbage detection is thus a \daiji{dependency analysis} starting from the
  \daiji{references on the stack}
  \begin{itemize}
    \item can be \daiji{very expensive}, if dependencies are complex
    \item require \daiji{storing extra information}: increases memory footprint
    \item beware of \daiji{circular structures}: dependency analysis could loop
  \end{itemize}
\end{frame}


\begin{frame}{Tracing GC}
  \href{https://en.wikipedia.org/wiki/Tracing_garbage_collection}{Tracing GC}
  is the most common approach to GC
  \bigskip

  Notion of reachability:
  \begin{itemize}
    \item objects referenced from the stack are \daiji{reachable}
    \item objects referenced by reachable objects are \daiji{reachable}
  \end{itemize}
  \bigskip

  Is this \daiji{optimal}? Are object freed as early as possible?
\end{frame}


\begin{frame}[fragile]{Tracing GC}
  \daiji{Not optimal}:

  \begin{large_rust}
    {
      let blah = create_something_big() ;
      { /* Do something with `blah`. */ }
      // 10_000 lines of code not using `blah` at all.
      ...
    }
  \end{large_rust}

  With tracing GC:
  \begin{itemize}
    \item reference \code{blah} will stay on the stack \daiji{until the
      block's exited}
    \item the memory it points to will not be freed for these $10k$ lines
    \item although we \daiji{do not use it at all}
  \end{itemize}
\end{frame}


\begin{frame}{When?}
  When should we run garbage collection?
  \bigskip

  When we're almost \daiji{out of space} on the heap?
  \pause
  \begin{itemize}
    \item means we've been dealing with \daiji{dead memory} for a while
    \item and we paid the price when we dealt with \daiji{memory fragmentation}
  \end{itemize}
  \bigskip
  \pause

  ``Every now and then''?
  \pause
  \begin{itemize}
    \item must analyze dependencies \daiji{every time}: more expensive
    \item we can memorize info form previous analyses
    \item at the cost of \daiji{memory}
  \end{itemize}
\end{frame}


\begin{frame}{How?}
  How should we run garbage collection?
  \bigskip

  \daiji{Stop-the-world}?
  \begin{itemize}
    \item stop the program, garbage collect, resume execution
    \item ``embarassing pause'' syndrome
    \item not suitable for interactive software (games, apps, \ldots)
  \end{itemize}
\end{frame}


\begin{frame}{How?}
  How should we run garbage collection?
  \bigskip

  Incremental?
  \pause
  \begin{itemize}
    \item interleaves garbage collection phases with program execution
    \item much more complex to implement (cleverly)
    \item lower throughput than stop-the-world:\\
      sum of incremental phases is higher
  \end{itemize}
\end{frame}


\begin{frame}{How?}
  How should we run garbage collection?
  \bigskip

  Parallel?
  \pause
  \begin{itemize}
    \item garbage collection run in a different thread, alongside the program
    \item very complex to implement (cleverly)
    \item also has a lower throughput than stop-the-world
  \end{itemize}
\end{frame}


\begin{frame}{Conclusion}
  Many design choices are possible
  \medskip

  Different languages call for different solutions
  \begin{itemize}
    \item built-in parallelism? Changes the whole approach to GC
    \item focus on immutability? Means substructures are reused heavily
  \end{itemize}
  \medskip

  GC-s for mainstream languages (Java, ocaml) are very efficient

  But can only go so far: when choosing a \daiji{language for a task}, it is one of the most important points to think about
\end{frame}


\end{document}
