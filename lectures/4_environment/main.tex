\documentclass[10pt]{beamer}
\usetheme{Warsaw}

% |===| Package imports.

\usepackage{
  etex, graphicx, amssymb, amsmath, amstext, amsfonts, mathtools,
  multicol, multirow, pgfplots, array, listings, colortbl, ulem, ifthen,
  xcolor, mathrsfs, xspace, rotating
}
\usepackage[scaled=1]{beramono}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{zlmtt}

\usetikzlibrary{shapes,arrows}

\renewcommand{\baselinestretch}{1.2}

% Trying to waste less space

% \addtolength{\topmargin}{1pt}
% \addtolength{\headsep}{5pt}
% \addtolength{\oddsidemargin}{-4pt}
% \addtolength{\marginparwidth}{20pt}
% \addtolength{\marginparsep}{-3pt}
% \addtolength{\textwidth}{-5pt}


% |===| Beamer things.

\input{conf/colors}
\input{conf/beamer}

% |===| Tikz things.

\input{conf/tikz}

% |===| Macros.

\input{conf/macros}

% |===| Nice links.

\hypersetup{
  colorlinks=true,
  linkcolor=url,
  urlcolor=url,
  pdftitle={Programming Language Concepts: Environment}
}


% |===| Title page info.

\title{
  Programming Language Concepts\\[3em]
  Environment\\[2em]
}

\author[]{
  Adrien Champion\\
  \href{mailto:adrien.champion@email.com}{adrien.champion@email.com}
}


\date{}

\begin{document}
\addtolength{\leftmargin}{-20pt}
\addtolength{\rightmargin}{-20pt}



\begin{frame}{}
  \titlepage
\end{frame}



\section{Names and Bindings}


\begin{frame}{Definitions}

  \begin{itemize}\bigsep
    \item \emph{static}:\\
      known at \daiji{compile time}: function signatures, types\ldots\\
      or \daiji{before}: built-in types and operations\ldots
    \item \emph{dynamic}:\\
      known at \daiji{runtime}: values, inputs, network communications\ldots
  \end{itemize}

\end{frame}

\begin{frame}[fragile]{Definitions}

  \begin{itemize}\bigsep
    \item \emph{Object}: a variable, a function, a type, a struct, a reference
    \ldots
    \item \emph{Name}: a sequence of characters \emph{bound} to an
      \daiji{object}
    \item \emph{Environment}: a mapping from \daiji{names} to the
      \daiji{object} they are bound to
  \end{itemize}
  \bigskip

  Naming an object is a \daiji{binding}:
  \begin{large_rust}
let name: usize = ... ;
  \end{large_rust}
  After this line, \code{name} is bound to a value of type \code{usize} in the
  environment

\end{frame}


\begin{frame}{Denotable objects}

  Objects that can be named are called \daiji{denotable objects}
  \bigskip

  Which objects are denotable \daiji{depends on the language}
  \bigskip

  But there's typically two categories of bindings:
  \begin{itemize}
    \item \daiji{user-defined}: for variables, formal parameters, functions,
      (user-defined) types/constants/exceptions\ldots 
    \item \daiji{built-in}: for primitive types, functions and constants in the
      standard library\ldots
  \end{itemize}

\end{frame}


\begin{frame}{Creating bindings}

  More precisely bindings can be introduced at various times:
  \medskip

  \begin{itemize}\bigsep
    \item<2-> \emph{design of the language} --- built-in bindings for primitive
      types, functions and constants: \code{MAX\_INT}, \code{+}, \ldots
    \item<3-> \emph{code writing} --- user-defined bindings. Bindings at this
      stage are \daiji{mostly conceptual} and could be optimized away, or need
      to be completed at runtime
    \item<4-> \emph{compile time} --- bindings for data structures that can be
      processed \daiji{statically}: \code{\& 'static str}, global
      \daiji{constants}
    \item<5-> \emph{runtime} --- binding to structures allocated dynamically,
      local variables in functions, \ldots
  \end{itemize}

\end{frame}



\section{Environment}


\begin{frame}{Definition}
  \daiji{Environment}: a mapping from \daiji{names} to the \daiji{object} they
  are bound to.
  \bigskip

  What does \daiji{object} mean here?
  \smallskip
  \pause

  It depends: \daiji{different} but very \daiji{similar} environments are used
  at compile and runtime.
  \bigskip
  \begin{itemize}
    \item compile time: map to \daiji{type information} (mostly)
    \item runtime: map to \daiji{data}, more precisely data's \daiji{location}
      in memory
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Environment (compile time)}
  For instance:
  \smallskip
  \begin{large_rust}
type Float = f64 ;
#[derive(Debug)]
struct Vec2 {
  x: Float, y: Float
}
fn main() {
  let x: Float = 4.2 ;
  let y        = 7.0 ;
  let vec2 = Vec2 {
    x: x, y: y
  } ;
  println!("vec2: {:?}", vec2) ;
  // Legal? ~~~~~~^^^^~~~^^^^
  println!("vec2: {}", x + y)
  // Legal? ~~~~~~^^~~~^^^^^
}
  \end{large_rust}
\end{frame}

\begin{frame}[fragile]{Environment (compile time)}
  For instance:
  \smallskip
  \begin{large_rust}
type Float = f64 ; // `Float` mapped to the type `f64`.
#[derive(Debug)]
struct Vec2 {      // `Vec2` mapped to structure info.
  x: Float, y: Float
}
fn main() {
  let x: Float = 4.2 ; // `x` mapped to `Float` type info.
  let y        = 7.0 ; // `y` mapped to `Float` type info.
  let vec2 = Vec2 {    // `vec2` mapped to `Vec2` type info.
    x: x, y: y
  } ;
  println!("vec2: {:?}", vec2) ;
  // Legal? ~~~~~~^^^^~~~^^^^
  println!("vec2: {}", x + y)
  // Legal? ~~~~~~^^~~~^^^^^
}
  \end{large_rust}
\end{frame}

\begin{frame}[fragile]{Environment (runtime)}
  For instance:
  \smallskip
  \begin{large_rust}
type Float = f64 ;
#[derive(Debug)]
struct Vec2 {
  x: Float, y: Float
}
fn main() {
  let x: Float = 4.2 ;
  let y        = 7.0 ;
  let vec2 = Vec2 {
    x: x, y: y
  } ;
  println!("vec2: {:?}", vec2) ;
  println!("x+y: {}", x + y)
}
  \end{large_rust}
\end{frame}

\begin{frame}[fragile]{Environment (runtime)}
  For instance:
  \smallskip
  \begin{large_rust}





fn main() {
  let x: Float = 4.2 ; // `x` mapped to value `4.2`.
  let y        = 7.0 ; // `y` mapped to value `7.0`.
  let vec2 = {         // `vec2` mapped to its data.
    (x, y)
  } ;
  println!("vec2: Vec2 {{ x: {}, y: {} }}", vec2.0, vec2.1) ;
  println!("x+y: {}", x + y)
}
  \end{large_rust}
\end{frame}


\begin{frame}{Types of environments}

  Independently of compile / runtime differences, several types of
  environments:

  \begin{itemize}\bigsep
    \item \emph{built-in environment} --- contains all the bindings provided
      by the designers of the language (\daiji{static})
    \item \emph{global environment} --- bindings accessible from
      \daiji{everywhere in the code} (\daiji{static})
    \item \emph{local environment} --- \daiji{scoped, block-dependent} bindings
      (``\daiji{dynamic}'')
  \end{itemize}
  \bigskip

\end{frame}





\begin{frame}{Blocks}
  A \emph{block} is a textual region of the program, identified by a
  \daiji{start sign} and an \daiji{end sign}, which can contain declarations
  \daiji{local to that region}.
  \bigskip

  \emph{N.B.} in addition to declarations, a block generally groups a
  \daiji{series of commands} into a syntactic entity

  $\impl$ a block can be considered a \daiji{single (composite) command}
  \bigskip
\end{frame}



\begin{frame}[fragile]{Two kinds of blocks}

  \begin{itemize}\bigsep
    \item \daiji{in-line blocks} --- can appear anywhere a command can appear:
    \begin{large_rust}
let x = { println!("binding to x") ; 7 } ;

{ // This block can be viewed as a composite command.
  let y = 3 ;
  println!("x: {}, y: {}", x, y) ;
} ;

println!(
  "y does not exist anymore, x is {}",
  { println!("I'm gonna print x.") ; x }
)
    \end{large_rust}
  \end{itemize}
\end{frame}



\begin{frame}[fragile]{Two kinds of blocks}
  \begin{itemize}\bigsep
    \item \daiji{function bodies} --- blocks defining a function
    \begin{large_rust}
fn decide_halting_problem(
  program: String, input: String
) -> String {
  let dont_know = "I don't know" ;
  dont_know.to_string()
}
    \end{large_rust}
    \href{http://is.gd/IjEIBj}{(on play.rust)}
  \end{itemize}
  (\emph{N.B.} in a function body, \daiji{the formal parameters are local
  bindings}.)
\end{frame}


\begin{frame}[fragile]{Overlapping blocks}
  \daiji{Overlapping blocks} such as
  \smallskip
  \begin{large_rust}
open blockA ;
  open blockB ;
close blockA ;
  close blockB ;
  \end{large_rust}

  are \daiji{never} legal.
\end{frame}


\begin{frame}{Blocks}

  \daiji{Why} do we care about blocks?
  \smallskip
  \pause

  The block is the construct of \daiji{least granularity} to which a
  \daiji{local environment} is associated.
  \smallskip

\end{frame}


\begin{frame}{Blocks and environments}

  \begin{itemize}\bigsep
    \item when \daiji{entering} a block, a new local environment is created
    \item when \daiji{exiting} a block, its local environment is discarded
    \item \daiji{inside} a block $\textcolor{orange}{\mathfrak{b}}$, we see
    \begin{itemize}
      \item<2-> $\textcolor{orange}{\mathfrak{b}}$'s \daiji{local} bindings
      \item<3-> bindings from the blocks \daiji{around}
        $\textcolor{orange}{\mathfrak{b}}$
      \item<4-> \daiji{global} bindings
    \end{itemize}
  \end{itemize}
  \bigsep

  \uncover<5->{
    What's the \daiji{appropriate data structure} for keeping track of the
    local environments?
  }

\end{frame}


\begin{frame}{Blocks and the environment stack}

  A \daiji{stack}:
  \begin{itemize}
    \item FILO (First In Last Out)
    \item<3-> can \daiji{push} a new environment when entering a block
    \item<3-> previous environments \daiji{are still in the stack}
    \item<4-> can insert new \daiji{local bindings}
      \uncover<5->{(\textcolor{yellow}{shadowing} is fine)}
    \item<6-> can \daiji{pop} the local environment when leaving a block
  \end{itemize}

  \begin{center}
    \input{figures/env_stack}
  \end{center}

\end{frame}


\begin{frame}{Blocks and the environment stack}

  \begin{center}
    \huge Demo\\
    \href{https://bitbucket.org/AdrienChampion/plc/src/fd751e5ba719fad0c7c85419b4d6c7332740ef3a/lectures/4_environment/env/?at=master}{(on bitbucket)}
  \end{center}

\end{frame}



\section{Power and Expressiveness}


\begin{frame}{For convenience}

  Naming things makes it \daiji{easier for humans} to talk about things.
  \medskip

  \begin{itemize}
    \item easier to talk about variable \code{count}
    \item than \emph{%
      ``the value stored in 8 bytes in memory at position $2048$''
    }
  \end{itemize}
  \medskip
  \pause

  Pretty simple mechanism: a name is basically \daiji{a position and a length}

\end{frame}


\begin{frame}{For power?}

  What about languages that \daiji{do not allow bindings}?
  \medskip
  \pause

  For example:
  \begin{itemize}
    \item Conway's \daiji{game of life}
    \item \daiji{\brainf}
  \end{itemize}
  \bigskip

  Are these language as \emph{powerful} as, Rust, Java, \ldots?
  \bigskip

  Let's look at \brainf more closely

\end{frame}


\begin{frame}{\brainf}

  On \href{https://en.wikipedia.org/wiki/Brainfuck\#Commands}{wikipedia}.
  \bigskip

  Memory is an array of \daiji{memory cells storing integers}

  \begin{itemize}
    \item a \emph{cursor} points to the \daiji{currently active cell}
    \item at the beginning the cursor points to the first cell
  \end{itemize}

  \begin{center}
    \href{http://www.iamcal.com/misc/bf_debug/}{Online interpreter / debugger}
  \end{center}

\end{frame}


\begin{frame}{\brainf}

  8 commands (instructions):

  \begin{itemize}
    \item \code{>} --- move the pointer to the \daiji{next cell}
    \item \code{<} --- move the pointer to the \daiji{previous cell}
    \item<2-> \code{+} --- \daiji{increment} the value in the current cell
    \item<2-> \code{-} --- \daiji{decrement} the value in the current cell
    \item<3-> \code{.} --- \daiji{print} the integer in the current cell
    \item<3-> \code{,} --- \daiji{read} an integer and write it in the current
      cell
    \item<4-> \code{[}
      \begin{itemize}
        \item if the current value is \daiji{zero}, jump to after the matching
          \code{]}
        \item else keep going
      \end{itemize}
    \item<4-> \code{]}
      \begin{itemize}
        \item if the current value is \daiji{non-zero}, jump to after the
          matching \code{[}
        \item else keep going
      \end{itemize}
  \end{itemize}

\end{frame}


\begin{frame}{\brainf}

  Is \brainf as \daiji{powerful} as Rust, Java, \ldots?
  \bigskip
  \pause

  Does it have the same \daiji{expressiveness}?

  Of course not

\end{frame}



\begin{frame}{For abstraction}

  Without abstractions humans \daiji{struggle} to write even slightly complex
  code
  \medskip

  Fibonacci in \brainf: \[
    \underbrace{\code{,>{}>+<{}<}}_{\text{setup}}
    \overbrace{
      \code{[->}
      \underbrace{
        \code{[->+>+<{}<]}
      }_{\uncover<2->{\text{loop 1}}}
      \code{>}
      \underbrace{
        \code{[-<+>]}
      }_{\uncover<2->{\text{loop \daiji{2}}}}
      \code{>}
      \underbrace{
        \code{[-<+>]}
      }_{\uncover<2->{\text{loop \daiji{2}}}}
      \code{<{}<{}<]}
    }^{\text{program loop}}
    \code{>.}
  \]
  \medskip

  \begin{itemize}
    \item without naming, ``functions'' have to be \daiji{in-lined}
    \item no \brainf construct lets us factor loop \daiji{2}
  \end{itemize}

\end{frame}


\begin{frame}{Bindings}

  \begin{itemize}\bigsep
    \item naming things and creating bindings does not give more \daiji{power}
    \item but it gives more \daiji{expressiveness}
  \end{itemize}
  \bigskip
  \pause

  Expressiveness is what makes \daiji{writing complex code possible}
  \medskip
  \pause

  Generally has a cost:
  \begin{itemize}
    \item \daiji{safety}-wise,
    \item \daiji{performance}-wise, and / or
    \item \daiji{brain}-power-wise: expressive constructs can have a steep
      learning curve
  \end{itemize}

\end{frame}



\end{document}
