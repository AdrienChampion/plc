//! Environment demo.

#![allow(dead_code, unused_variables, unused_mut)]

extern crate ansi_term as ansi ;

use std::collections::HashMap ;

use ansi::{ Style, Colour } ;

/// Type information.
pub type Info = String ;

/// A map from strings to strings.
pub type Map = HashMap<String, Info> ;
/// Creates a new `Map`.
pub fn mk_map() -> Map { Map::new() }

/// Environment structure.
pub struct Env {
  /// Built-in bindings.
  pub builtin: Map,
  /// Global bindings.
  pub global: Map,
  /// Stack of (named) local bindings.
  pub local: Vec< (String, Map) >,
}
impl Env {
  /// Creates an empty environment.
  pub fn empty() -> Self {
    Env {
      builtin: mk_map(),
      global: mk_map(),
      local: vec![ ("Top".to_string(), mk_map()) ]
    }
  }
  /// Pops a local map.
  pub fn pop(& mut self) -> (String, Map) {
    match self.local.pop() {
      Some( (scope, map) ) => {
        println!(
          "{:<30} | {} ({} bindings)",
          scope, Style::new().bold().paint("pop"), map.len()
        ) ;
        (scope, map)
      },
      None => panic!("pop on empty local stack"),
    }
  }
  /// Pushes an empty local map.
  pub fn push(& mut self, name: & str) {
    self.local.push( (name.to_string(), mk_map()) ) ;
    println!(
      "{:<30} | {}", name, Style::new().bold().paint("push")
    ) ;
  }
  /// Inserts a local binding.
  pub fn insert(& mut self, name: & str, object: & str) {
    let last = self.local.len() - 1 ;
    let _ = self.local[last].1.insert(name.to_string(), object.to_string()) ;
    println!(
      "{:>30} | {} {} -> {}",
      self.local[last].0, Colour::Red.paint("insert"), name, object
    ) ;
    ()
  }
  /// Retrieves an object from a name.
  pub fn get(& self, name: & str) -> Option<String> {
    for & (ref scope, ref map) in self.local.iter().rev() {
      match map.get(name) {
        None => continue,
        Some(object) => {
          println!(
            "{:>30} | {} \"{}\": \"{}\"",
            scope, Colour::Green.paint("get"), name, object
          ) ;
          return Some(object.clone())
        },
      }
    } ;
    match self.global.get(name) {
      None => (),
      Some(object) => {
        println!(
          "{:>30} | {} \"{}\": \"{}\"",
          "global", Colour::Green.paint("get"), name, object
        ) ;
        return Some(object.clone())
      }
    } ;
    match self.builtin.get(name) {
      None => None,
      Some(object) => {
        println!(
          "{:>30} | {} \"{}\": \"{}\"",
          "builtin", Colour::Green.paint("get"), name, object
        ) ;
        Some(object.clone())
      }
    }
  }
}

fn main() {
  println!("") ;
  println!("|{:=^70}|", " Starting ") ;
  println!("") ;

  let mut env = Env::empty() ;

  let mut x = 3 ;
  env.insert("x", "mut usize") ;

  let y = vec![ "blah" ] ;
  env.insert("y", "Vec<& 'static str>") ;

  for n in 0..4 {
    env.push(& format!("for_loop ({})", n)) ;
    env.insert("n", "usize") ;
    match n {
      odd if n % 2 == 1 => {
        env.push("match (odd)") ;
        env.insert("odd", "usize") ;
        let _ = env.pop() ;
        ()
      },
      even if n % 2 == 0 => {
        env.push("match (even)") ;
        env.insert("even", "usize") ;
        x += n ;
        let _ = env.get("x") ;
        let _ = env.get("n") ;
        let _ = env.get("even") ;
        let _ = env.pop() ;
        ()
      },
      _ => unreachable!(),
    }
    let _ = env.pop() ;
    ()
  }

  println!("") ;
  println!("|{:=^70}|", " Done ") ;
  println!("") ;

  ()

}