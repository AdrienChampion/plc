/*! Example of using crate `piston_meta`.

# RTFM

I **STRONGLY RECOMMEND** you run `cargo doc` on this project and open it
in your browser. To do so just open `target/doc/parser/index.html`.
That way you will have access to all the docs from crates
[`piston_meta`][piston] and [`range`][range] (used by `piston_meta`).

# PEBCAK

The challenge in the assignment is to understand how to use `piston_meta`.
The workflow is as follows:

- write your CFG in a string, `let rules = r#"..."#` here
  (if you're wondering about the `r#` business, it just means [the string is
  a *raw* string][raw strings])
- have `piston_meta` generate a parser from it with function
  [`syntax_errstr`][parse gen fun]
  (can fail and thus produces a [`Result`][res])
- parse some string with [`parse_errstr`][parse fun]
  (can fail and thus produces a [`Result`][res])
- the result is a vector of tokens of type `Range<MetaData>`:

  - [`Range`][range] stores info about the substring the token comes from.
    Field `offset` is the index of the first character of the substring from
    the beginning of the string parsed, and field `length` is the length of the
    substring. Both are used when parsing fails, to be able to point precisely
    at the source of the problem in the input string.

      Field `data` is the token, values of type `MetaData` here.

  - [`MetaData`][metadata] is the actual token. Depending on how you wrote the
    rules it can be several thing: a `bool`, an `f64` or a `String`. It can
    also be a `StartNode` or an `EndNode`, which give the list of tokens some
    structure depending on how you wrote the rules.

To understand precisely how all of this works, there's only one way:

- read the explanations for the library [here][piston repo] and
  [here][self syntax]. The latter gives more technical details, I found it more
  useful to actually understand what's going on.

    But it's probably a good idea to use the first link first, and use the
    second one when you want more precise info (when you start playing with the
    code typically)
- reading the explanations is far from enough. You need to play with the code
  below and try to make it parse a richer language to understand how it works.

    You will not understand anything and fail a lot at first, it's expected :)
    Eventually (hopefully) you will understand enough of it to do the
    assignment.

# Example from `piston_meta`'s documentation

The example I give to you in this project is **slightly** more complex.

```[rust,no_use]
extern crate piston_meta;

use piston_meta::*;

fn main() {
    let text = r#"hi James!"#;
    let rules = r#"
        1 say_hi = ["hi" .w? {"James":"james" "Peter":"peter"} "!"]
        2 document = say_hi
    "#;
    // Parse rules with meta language and convert to rules for parsing text.
    let rules = match syntax_errstr(rules) {
        Err(err) => {
            println!("{}", err);
            return;
        }
        Ok(rules) => rules
    };
    let mut data = vec![];
    match parse_errstr(&rules, text, &mut data) {
        Err(err) => {
            println!("{}", err);
            return;
        }
        Ok(()) => {}
    };
    json::print(&data);
}
```

[piston]: ../piston_meta/index.html (Crate piston_meta)
[range]: ../range/index.html (Crate range)
[parse gen fun]: ../piston_meta/fn.syntax_errstr.html (Function syntax_errstr)
[raw strings]: https://doc.rust-lang.org/reference.html#raw-string-literals (Raw strings)
[res]: https://doc.rust-lang.org/nightly/core/result/enum.Result.html (Type Result)
[parse fun]: ../piston_meta/meta_rules/fn.parse_errstr.html (Function parse_errstr)
[range]: ../range/struct.Range.html (Type Range)
[metadata]: ../piston_meta/enum.MetaData.html (MetaData)
[piston repo]: https://github.com/pistondevelopers/meta#piston_meta (Piston meta repo)
[self syntax]: https://raw.githubusercontent.com/PistonDevelopers/meta/master/assets/self-syntax.txt (Piston meta self syntax)
*/

extern crate piston_meta ;

use piston_meta::* ;

fn main() {
  use std::process::exit ;

  let text = r#"
    hi James!
    hi Peter!
  "# ;

  println!("") ;
  println!("|===| input text:") ;
  let mut line_cnt = 0 ;
  for line in text.lines() {
    println!("| {:>2}|   \"{}\"", line_cnt, line) ;
    line_cnt += 1
  } ;

  let rules = r#"
    1 say_hi = ["hi" .w? {"James":"james" "Peter":"peter"} "!"]
    2 document = .r!([.w? say_hi .w?])
  "# ;

  // Parse rules with meta language and convert to rules for parsing text.
  println!("|===| Generating parser.") ;
  let rules = match syntax_errstr(rules) {
    Err(err) => {
      println!("| could not generate parser:") ;
      for line in format!("{}", err).lines() {
        println!("| {}", line)
      } ;
      println!("|===|") ;
      println!("") ;
      exit(2)
    }
    Ok(rules) => {
      println!("| success") ;
      rules
    }
  } ;

  // Buffer for parsing.
  // You probably should ask about this in class / Piazza if can't figure out
  // what it is.
  let mut data = vec![] ;

  println!("|===| Parsing input string.") ;
  match parse_errstr(& rules, text, & mut data) {
    Err(err) => {
      println!("| could not parse input string:") ;
      for line in format!("{}", err).lines() {
        println!("| {}", line)
      } ;
      println!("|===|") ;
      println!("") ;
      exit(2)
    }
    Ok(()) => {
      println!("| success") ;
      println!("|===|") ;
      println!("")
    }
  } ;

  println!("") ;
  println!("|===| Result of parsing is a vector of `range`s:") ;
  for range in data.iter() {
    println!(
      "| {:?} (offset: {}, length: {})", range.data, range.offset, range.length
    )
  } ;
  println!("|===|") ;
  println!("") ;

  println!("json print:") ;
  // Print function provided by the authors of `piston_meta`.
  //
  // The parser generator was designed to parse json files which have a
  // special, simple syntax. It will be enough for the assignment.
  json::print(& data) ;
  println!("")
}