\documentclass[10pt]{beamer}
\usetheme{Warsaw}

% |===| Package imports.

\usepackage{
  etex, graphicx, amssymb, amsmath, amstext, amsfonts, mathtools,
  multicol, multirow, pgfplots, array, listings, colortbl, ulem, ifthen,
  xcolor, mathrsfs, xspace
}
\usepackage[scaled=1]{beramono}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{zlmtt}

\usetikzlibrary{shapes,arrows}

\renewcommand{\baselinestretch}{1.2}

% Trying to waste less space

% \addtolength{\topmargin}{1pt}
% \addtolength{\headsep}{5pt}
% \addtolength{\oddsidemargin}{-4pt}
% \addtolength{\marginparwidth}{20pt}
% \addtolength{\marginparsep}{-3pt}
% \addtolength{\textwidth}{-5pt}


% |===| Beamer things.

\input{conf/colors}
\input{conf/beamer}

% |===| Tikz things.

\input{conf/tikz}

% |===| Macros.

\input{conf/macros}

% |===| Nice links.

\hypersetup{
  colorlinks=true,
  linkcolor=url,
  urlcolor=url,
  pdftitle={Programming Language Concepts: Foundations}
}


% |===| Title page info.

\title{
  Programming Language Concepts\\[3em]
  Foundations\\[2em]
}

\author[]{
  Adrien Champion\\
  \href{mailto:adrien.champion@email.com}{adrien.champion@email.com}
}


\date{}

\begin{document}
\addtolength{\leftmargin}{-20pt}
\addtolength{\rightmargin}{-20pt}



\begin{frame}{}
  \titlepage
\end{frame}




\section{Algorithms and functions}



\begin{frame}{Algorithms}

  An \daiji{instruction} is an action (operation) that requires no insight,
  guess or randomness.\\
  Understand \daiji{``something a computer can do in one tick''}.
  \smallskip

  \begin{itemize}
    \item write $7$ here
    \item add this number with this one and write the result there
    \item if this number is greater than $0$ then go there
    \item ...
  \end{itemize}
  \bigskip

  An \daiji{algorithm} is a sequence of instructions.\\
  Understand \daiji{``something that runs on a computer: we don't know how long
  it will take or if it will terminate one day''}.

\end{frame}



\begin{frame}[fragile]{Functions}

  A \daiji{function} is a mathematical object that is ``defined''
  \daiji{using some formalism}.
  \bigskip

  For instance: \[
    \forall n \ge 0,\quad \mita{factorial}(n) \;=\;
    \begin{cases}
      1 & \emph{if} \;\; n = 0 \\
      \prod_{i \in [1,n]} i & \emph{otherwise}\\
    \end{cases}
  \]
\end{frame}



\begin{frame}[fragile]{Computable functions}

  A function $f$ over some domain $\mathscr{D}$ is \daiji{computable} if there
  is an \emph{algorithm} \code{f} such that \[
    \forall x \in \mathscr{D},\quad
    \code{f}(x) \text{ \daiji{terminates and produces the value} } f(x)
  \]
  \bigskip

  Factorial is \daiji{computable}:
  \begin{large_rust}
fn factorial(n: usize) -> usize {
  [1..n].into_iter().fold(1, |prod, i| prod * i)
}
  \end{large_rust}
  \bigskip

  \pause
  \emph{Are all functions computable?}
  \pause
  \daiji{No.}

\end{frame}



\begin{frame}{The Halting Problem}

  Let $\lang$ be some language.
  \bigskip

  Is there an algorithm $\haltAlg$ taking \daiji{two strings} as inputs,

  \begin{itemize}
  \item a \daiji{program} \prog in $\lang{}$ (String), and
  \item some \daiji{input data $\inData$} (String),
  \end{itemize}

  always terminates with a correct answer to the \emph{halting question}:
  \begin{center}
    ``\daiji{
      Does $\progApp{\inData}$ terminate or does it loop forever?%
    }''
  \end{center}
  \bigskip

  \pause
  In more technical terms:
  \begin{center}
    ``Does a \daiji{decision procedure} ($\haltAlg$) exist for the halting
    question?''\\
    \uncover<3->{
      ``Is the halting question \daiji{decidable}?''
    }\\
    \uncover<4->{
      ``Is there a \daiji{total computable function} that answers the halting
      question?''
    }
  \end{center}

\end{frame}



\begin{frame}{The Halting Problem}

  \begin{center}
    \daiji{No}: the halting problem is \daiji{undecidable}.\\
    Let's prove it.
  \end{center}

\end{frame}



\begin{frame}[fragile]{The Halting Problem: Pregame}

  Let $f$ be \daiji{any computable function}
  \begin{itemize}
    \item taking two strings as inputs and
    \item returning a Boolean.
  \end{itemize}
  \smallskip
  It is computable so there is an algorithm \code{f} equivalent to it.
  \bigskip

  Let \code{g} be the algorithm taking one string as input, defined as:
  \begin{large_rust}
fn g(s: String) -> bool {
  if f(s,s) { loop {} } else { false }
}
  \end{large_rust}

\end{frame}



\begin{frame}[fragile,t]{The Halting Problem: Proof}

  Say we have an algorithm $\haltAlg$ that solves the halting problem:
  \begin{large_rust}
fn $\haltAlg$(p: String, input: String) -> bool {
  if /* p(input) terminates */ { true } else { false }
}
  \end{large_rust}
  \smallskip
  \pause

  We just defined, \daiji{for any computable function f},
  \begin{large_rust}
fn g(s: String) -> bool {
  if f(s,s) { loop {} } else { false }
}
  \end{large_rust}
  \medskip

  In particular, \code{f} could be $\haltAlg$.

\end{frame}



\begin{frame}[fragile,t]{The Halting Problem: Proof}

  Say we have an algorithm $\haltAlg$ that solves the halting problem:
  \begin{large_rust}
fn $\haltAlg$(p: String, input: String) -> bool {
  if /* p(input) terminates */ { true } else { false }
}
  \end{large_rust}
  \smallskip

  With $\haltAlg$ instead of \code{f}:
  \begin{large_rust}
fn g(s: String) -> bool {
  if $\haltAlg$(s,s) { loop {} } else { false }
}
  \end{large_rust}
  \bigskip
  \pause

  What's \code{g(g)}? (\code{g} applied to the string representation of
  \code{g})
  \begin{itemize}
  \item<3-> say $\haltAlg$\code{(g,g)} is \code{true},
    \uncover<4->{\ita{i.e.} \code{g(g)} \daiji{terminates}}\\
    \uncover<5->{Then \code{g(g)} \daiji{loops forever}: contradiction}
  \item<6-> say $\haltAlg$\code{(g,g)} is \code{false},
    \uncover<7->{\ita{i.e.} \code{g(g)} \daiji{loops forever}}\\
    \uncover<8->{
      Then \code{g(g)} \daiji{terminates} with \code{false}: contradiction
    }
  \end{itemize}

\end{frame}



\begin{frame}{The Halting Problem: Conclusion}

  \begin{itemize}\bigsep
    \item $\haltAlg$ must always answer the halting question \daiji{correctly}
    \item if $\haltAlg$ exists, we can construct \code{g} such that $\haltAlg$
      \daiji{is wrong}
    \item thus $\haltAlg$ \daiji{cannot exist}
  \end{itemize}

\end{frame}



\begin{frame}{The Halting Problem: Epilogue}
  \begin{itemize}\bigsep
    \item Defining the halting problem starts with a \daiji{language $\lang$}
    \item we used Rust in practice
    \item What if we choose a different language? Maybe $\haltAlg$ can exist
  \end{itemize}
\end{frame}



\begin{frame}[fragile]{The Halting Problem: Epilogue}
  $\haltAlg$ cannot exist as long as we can build \code{g}, what did we use?
  \smallskip

  \begin{large_rust}
fn g(s: String) -> bool {
  if $\haltAlg$(s,s) { loop {} } else { false }
}
  \end{large_rust}
  \bigskip
  \pause

  \begin{itemize}\bigsep
    \item \daiji{conditional branching}, to choose what to do
    \item the ability to \daiji{loop}
  \end{itemize}
\end{frame}



\begin{frame}[fragile,t]{The Halting Problem: Epilogue}
  \daiji{No solution} to the halting problem if $\lang$ has
  \begin{itemize}
    \item \daiji{conditional branching}, to choose what to do
    \item the ability to \daiji{loop}
  \end{itemize}
  \bigskip

  \alt<3->{
    A language without conditional branching then?
    \begin{itemize}
      \item<4-> meaning no \code{ite}, \code{while}, \code{for}, \code{switch},
        \code{match}, \ldots
      \item<4-> meaning no decision can be made a runtime
    \end{itemize}
    \bigskip

    \uncover<5->{%
      Halting problem becomes trivial: is there loops in the program?
    }
    \begin{itemize}
      \item<5-> \daiji{yes}: \uncover<6->{%
        program terminates iff there's a \code{break} in every loop
      }
      \item<5-> \daiji{no}: \uncover<7->{%
        program terminates
      }
    \end{itemize}
    \bigskip

    Very poor language too
  }{
    A language without loops maybe?
    \begin{itemize}
      \item<2-> halting problem becomes trivial: \daiji{there's no loops}
      \item very poor language: cannot even write factorial
    \end{itemize}
  }
\end{frame}



\begin{frame}{The Halting Problem: Last Slide}

  Any interesting algorithm uses conditional branching and loops
  \bigskip

  The undecidability of the Halting Problem is an \daiji{absolute limitation}, intrinsic to the notion of algorithm.\\
  It is independent of what the language actually is.
  \bigskip

  As humans we can express \emph{functions and problems} for which no algorithm
  can exist.

\end{frame}


\begin{frame}{Undecidable Problems}

  Determining whether
  \medskip
  \begin{itemize}
    \item a program computes a constant function
    \item two programs compute the same function
    \item a program terminates for every input
    \item a program diverges for every input
    \item a program, give an input, will produce an error during its execution
    \item a program will cause a type error
  \end{itemize}

\end{frame}



\section{Formalisms for Computability}


\begin{frame}{Intro}

  Formalisms for computability
  \begin{itemize}
    \item let us reason about what it means to \daiji{compute stuff}
    \item in an \daiji{abstract} way
    \item without refering to a precise language
  \end{itemize}
  \medskip

  The most well-known formalism is of course \daiji{Turing Machines}

\end{frame}



\begin{frame}{Turing Machines}

  A Turing machine is composed of
  \medskip
  \begin{itemize}
    \item an \daiji{infinite tape} divided into cells each storing a symbol
      from an alphabet
    \item a \daiji{mobile head} that reads and writes cells
    \item a \daiji{finite state controller} for the head
  \end{itemize}
  \bigskip

  Its behavior is
  \medskip
  \begin{itemize}
    \item \daiji{read} symbol from cell
    \item \daiji{write} some symbol to cell,
      depending on the state the controller's in and the symbol read
    \item \daiji{move} left or right and
      \daiji{change the state} of the controller
  \end{itemize}

\end{frame}


\begin{frame}{Turing Machines}

  \begin{itemize}\bigsep
    \item formalism used originally to prove the Halting Problem undecidable
    \item surprisingly \daiji{close to the metal}
    \item not very abstract
  \end{itemize}
  \bigskip

  Important mostly for \emph{historical reasons}
  \begin{itemize}
    \item<2-> slightly obsolete: no notion of parallelism for instance
    \item<3-> not very formal, it's tedious to write proofs for turing machines
  \end{itemize}

\end{frame}


\begin{frame}{More formalisms}

  \begin{itemize}
    \item \daiji{lambda calculus} (and its variants)
    \item general recursive functions
    \item $\mu$-calculus
    \item \daiji{any programming language} is a formalism for computability
    \item \href{https://en.wikipedia.org/wiki/Lambda_calculus\#See_also}{%
      more here
    }
  \end{itemize}
  \bigskip

  But all these formalisms can be \daiji{simulated by each other}, so
  \smallskip
  \begin{itemize}
    \item they can all express \daiji{the same computable functions}
    \item and have the same limitations
  \end{itemize}

  In particular, \daiji{all programming languages} are
  \daiji{Turing-equivalent}, or \daiji{Turing-complete}.

\end{frame}



\begin{frame}{Turing-completeness}

  If a language is Turing-complete, it means it

  \begin{itemize}
    \item has \daiji{loops}
    \item has \daiji{conditional branching}
    \item can \daiji{loop} forever
      but there's no way (in general) to say it will
    \item can represent the same algorithms as other Turing-complete
      languages
  \end{itemize}

\end{frame}


\begin{frame}{All Languages are Equivalent}

  All languages are equivalent \daiji{unless you're a human}.
  \bigskip

  All languages are \daiji{not} equivalent when it comes to
  \smallskip

  \begin{itemize}
    \item flexibility
    \item maintainability
    \item abstraction principles
    \item portability
    \item \emph{safety}
    \item \ldots
  \end{itemize}
  \medskip
  \pause

  These properties are referred to as the \daiji{expressiveness} of the
  language

\end{frame}



\begin{frame}{All Languages are \emph{not} Equivalent}

  \daiji{Brainfuck}:
  \smallskip
  \pause

  Inspired by a few minimalist languages from the 1960s, Brainfuck is
  \daiji{obfuscated by design}. By the
  \href{http://www.muppetlabs.com/~breadbox/bf/standards.html}{original spec},
  Brainfuck programs are dangerously Close To The Metal. The initial compiler
  was 240 bytes which leaves no room for abstraction (or a sane alphabet for
  that matter). As a bonus, \daiji{the machine code the compiler spits out is
  probably more fun to read}.
  \bigskip
  \pause

  Brainfuck is \daiji{Turing-complete}, so
  \begin{itemize}
    \item it is possible to write Grand Theft Auto 5 directly in Brainfuck
    \item although it will take \daiji{several times the age of the Universe}
    \item and would be \daiji{extremely buggy}
  \end{itemize}

\end{frame}


\begin{frame}{All Languages are \emph{not} Equivalent}

  \daiji{Humans} can only handle so much complexity in a program
  \bigskip

  The compiler can provide a lot of help by
  \begin{itemize}
    \item \daiji{statically proving} some bugs cannot occur
    \item \daiji{warning you} when you're doing legal but weird things,
      \ita{e.g.} unused variables
    \item \daiji{handling complex things} for you, \ita{e.g.} memory
      management
  \end{itemize}
  \bigskip
  \pause

  But nothing's free, the cost of this help is
  \begin{itemize}
    \item \daiji{worse performance} than by doing things yourself, and/or
    \item \daiji{understanding how to communicate with the compiler}:
    \begin{itemize}
      \item how to \daiji{write programs using the concepts} of the language,
      \item<3-> how to \daiji{understand why the compiler rejects} your buggy
        code
    \end{itemize}
  \end{itemize}

\end{frame}




\section{Expressiveness: Pattern Matching}



\begin{frame}[t, fragile]{Pattern matching}

  Confronts a \daiji{value} and a list of \daiji{patterns}:
  \begin{large_rust}
match expr {
  pattern_1 => { ... },
  pattern_2 => { ... },
  ...
}
  \end{large_rust}
  \medskip

  \begin{itemize}
    \item at runtime, each pattern is tried \daiji{in order}
    \item when a match is found, the corresponding block is executed
  \end{itemize}
  \bigskip

  What if \daiji{no pattern matches}?
  \smallskip\pause

  Cannot happen, pattern matching \daiji{must be exhaustive}.

  A non-exhaustive pattern matching is a \daiji{static error}.

\end{frame}



\begin{frame}[t, fragile]{Pattern matching}

  Confronts a \daiji{value} and a list of \daiji{patterns}:
  \begin{large_rust}
match expr {
  pattern_1 => { ... },
  pattern_2 => { ... },
  ...
}
  \end{large_rust}

  A \daiji{pattern} can be:
  \begin{itemize}
    \item a \emph{wildcard}: \code{\_}
    \item a \emph{constant}: \code{3}, \code{None}\ldots
    \item an \emph{identifier}: \code{blah}
    \item a \emph{destructuration} containing more patterns:\\
      \code{Ok(...)}\\
      \code{MyStruct \{ field: ... \}}
  \end{itemize}

\end{frame}



\begin{frame}{Pattern matching: examples}

  Examples:
  \begin{itemize}
    \item wildcard: \url{http://is.gd/UQLeCI}
    \item \daiji{to fill in class}
  \end{itemize}

\end{frame}



\begin{frame}[fragile]{Pattern matching: guards}

  Patterns can be guarded:
  \begin{large_rust}
match expr {
  pattern_1 => ...,
  pattern_2 if bool_expr => ...,
  ...
}
  \end{large_rust}

\end{frame}






\end{document}
